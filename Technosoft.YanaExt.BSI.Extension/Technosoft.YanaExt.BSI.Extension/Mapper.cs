﻿namespace Technosoft.YanaExt.BSI.Extension
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;

    #region Helper
    public enum MapMode
    {
        Exact = 1,
        Nearest
    }
    #endregion

    public class Mapper<TE>
        where TE : Entity, new()
    {
        #region Var, Global, Helper
        public static TE Context = new TE();
        static IOrganizationService Service;

        public class Queries
        {
            public object Value;
            public Expression<Func<TE, object>> Target;
        }
        #endregion

        #region Constructor
        public Mapper(IOrganizationService s)
        {
            Service = s;
        }
        #endregion

        /// <summary>
        /// Returns EntityReference. Search by Primary Field.
        /// </summary>
        /// <param name="pf">Primary Field Value. String.</param>
        /// <param name="target">Filter condition target attribute schema name</param>
        /// <returns></returns>
        public static EntityReference Fetch(string pf, string target)
        {
            var fetch = @"<fetch>
                    <entity name='{0}'>
                    <attribute name='{1}' />
                    <filter type='and'>
                    <condition attribute='{2}' operator='eq' value='{3}' />
                    </filter>    
                    </entity>
                    </fetch>";

            var result = Service.RetrieveMultiple(
                new FetchExpression(
                    String.Format(fetch,
                        Context.LogicalName,
                        Context.LogicalName.ToLower() + "id",
                        target,
                        pf)
                )).Entities;

            return !result.Any() ? null : result[0].ToEntity<TE>().ToEntityReference();
        }

        /// <summary>
        /// Returns Object of <typeparamref name="TE"/>. Search by Primary Field.
        /// </summary>
        /// <param name="pf">Primary Field Value. String.</param>
        /// <param name="target">First: filter condition target schema name. Other (optional): ColumnSet column.</param>
        /// <returns></returns>
        public TE Fetch(string pf, params Expression<Func<TE, object>>[] target)
        {
            int ctr = 0;
            string columnSet = "", querySchema = "";
            foreach (var item in target)
            {
                var reff = string.Empty;

                if (item.Body.GetType() == typeof(UnaryExpression))
                {
                    var temp = ((MemberExpression)((UnaryExpression)item.Body).Operand).Member.GetCustomAttributes(true)[0];
                    if (temp.GetType() == typeof(AttributeLogicalNameAttribute))
                        reff = ((AttributeLogicalNameAttribute)temp).LogicalName;
                    else
                    {
                        if (((MemberExpression)((UnaryExpression)item.Body).Operand).Member.Name.ToLower() == "id")
                            reff = Context.LogicalName + "id";
                    }
                }
                else
                {
                    reff = ((AttributeLogicalNameAttribute)((MemberExpression)item.Body).Member.GetCustomAttributes(true)[0]).LogicalName;
                }

                if (string.IsNullOrEmpty(reff)) continue;
                columnSet += String.Format("<attribute name='{0}' />", reff.ToLower());
                if (ctr++ == 0) querySchema = reff.ToLower();
            }

            var fetch = @"<fetch>
                    <entity name='{0}'>
                    <attribute name='{1}' />
                    {4}
                    <filter type='and'>
                    <condition attribute='{2}' operator='eq' value='{3}' />
                    </filter>    
                    </entity>
                    </fetch>";

            var result = Service.RetrieveMultiple(
                new FetchExpression(
                    String.Format(fetch,
                        Context.LogicalName,
                        Context.LogicalName.ToLower() + "id",
                        querySchema,
                        pf,
                        columnSet)
                )).Entities;

            return !result.Any() ? null : result[0].ToEntity<TE>();
        }

        /// <summary>
        /// Returns Object of <typeparamref name="TE"/>. Search by regarding record ID.
        /// </summary>
        /// <param name="id">Regading record Id.</param>
        /// <param name="target">ColumnSet column.</param>
        /// <returns></returns>
        public TE Fetch(Guid id, params Expression<Func<TE, object>>[] target)
        {
            string columnSet = "";
            bool idInColumnSet = false;
            foreach (var item in target)
            {
                var reff = string.Empty;

                if (item.Body.GetType() == typeof(UnaryExpression))
                {
                    var temp = ((MemberExpression)((UnaryExpression)item.Body).Operand).Member.GetCustomAttributes(true)[0];
                    if (temp.GetType() == typeof(AttributeLogicalNameAttribute))
                        reff = ((AttributeLogicalNameAttribute)temp).LogicalName;
                    else
                    {
                        if (((MemberExpression)((UnaryExpression)item.Body).Operand).Member.Name.ToLower() == "id") {
                            idInColumnSet = true;
                            reff = Context.LogicalName + "id";
                        }
                    }
                }
                else
                {
                    reff = ((AttributeLogicalNameAttribute)((MemberExpression)item.Body).Member.GetCustomAttributes(true)[0]).LogicalName;
                }

                if (string.IsNullOrEmpty(reff)) continue;
                columnSet += String.Format("<attribute name='{0}' />", reff.ToLower());
            }

            var fetch = @"<fetch>
                    <entity name='{0}'>
                    <attribute name='{1}' />
                    {4}
                    <filter type='and'>
                    <condition attribute='{2}' operator='eq' value='{3}' />
                    </filter>    
                    </entity>
                    </fetch>";

            var result = Service.RetrieveMultiple(
                new FetchExpression(
                    String.Format(fetch,
                        Context.LogicalName,
                        idInColumnSet ? "" : Context.LogicalName.ToLower() + "id",
                        Context.LogicalName.ToLower() + "id",
                        id,
                        columnSet)
                )).Entities;

            return !result.Any() ? null : result[0].ToEntity<TE>();
        }

        /// <summary>
        /// Returns Object of <typeparamref name="TE"/>. Search by 
        /// </summary>
        /// <param name="q">List of Query</param>
        /// <param name="columns">Column Set</param>
        /// <returns></returns>
        public TE Fetch(List<Queries> q, params Expression<Func<TE, object>>[] columns)

        {
            string columnSet = "";
            string filterConditions = "";

            foreach (var query in q)
            {
                var item = query.Target;
                var reff = string.Empty;
                if (item.Body.GetType() == typeof(UnaryExpression))
                {
                    var temp = ((MemberExpression)((UnaryExpression)item.Body).Operand).Member.GetCustomAttributes(true)[0];
                    if (temp.GetType() == typeof(AttributeLogicalNameAttribute))
                        reff = ((AttributeLogicalNameAttribute)temp).LogicalName;
                    else
                    {
                        if (((MemberExpression)((UnaryExpression)item.Body).Operand).Member.Name.ToLower() == "id")
                            reff = Context.LogicalName + "id";
                    }
                }
                else
                {
                    reff = ((AttributeLogicalNameAttribute)((MemberExpression)item.Body).Member.GetCustomAttributes(true)[0]).LogicalName;
                }

                if (string.IsNullOrEmpty(reff)) continue;
                filterConditions += string.Format("<condition attribute='{0}' operator='eq' value='{1}' />", reff, query.Value);
            }

            foreach (var item in columns)
            {
                var reff = string.Empty;

                if (item.Body.GetType() == typeof(UnaryExpression))
                {
                    var temp = ((MemberExpression)((UnaryExpression)item.Body).Operand).Member.GetCustomAttributes(true)[0];
                    if (temp.GetType() == typeof(AttributeLogicalNameAttribute))
                        reff = ((AttributeLogicalNameAttribute)temp).LogicalName;
                    else
                    {
                        if (((MemberExpression)((UnaryExpression)item.Body).Operand).Member.Name.ToLower() == "id")
                            reff = Context.LogicalName + "id";
                    }
                }
                else
                {
                    reff = ((AttributeLogicalNameAttribute)((MemberExpression)item.Body).Member.GetCustomAttributes(true)[0]).LogicalName;
                }

                if (string.IsNullOrEmpty(reff)) continue;
                columnSet += String.Format("<attribute name='{0}' />", reff.ToLower());
            }

            var fetch = @"<fetch>
                    <entity name='{0}'>
                    <attribute name='{1}' />
                    {2}
                    <filter type='and'>
                    {3}
                    </filter>    
                    </entity>
                    </fetch>";

            var result = Service.RetrieveMultiple(
                new FetchExpression(
                    String.Format(fetch,
                        Context.LogicalName,
                        Context.LogicalName.ToLower() + "id",
                        columnSet,
                        filterConditions)
                )).Entities;

            return !result.Any() ? null : result[0].ToEntity<TE>();
        }
    }
    
    public class MapOptions<T>
            where T : struct, IComparable, IFormattable, IConvertible
    {
        /// <summary>
        /// Get value of Optionset by its name
        /// </summary>
        /// <param name="val">Label</param>
        /// <param name="mode">Default/1:Exact, 2:Nearest</param>
        /// <returns></returns>
        public OptionSetValue FetchByName(string val, MapMode mode = MapMode.Exact)
        {
            var ctr = 0;
            var keys = Enum.GetValues(typeof(T)).Cast<T>();
            var values = Enum.GetValues(typeof(T)).Cast<T>().Cast<int>().ToList();
            var ValList = new Dictionary<string, int>();
            foreach (var key in keys)
            {
                ValList.Add(key.ToString(), values[ctr++]);
            }

            var searchs = //Enum.Parse(typeof(T), val).ToString();
                val.Replace(" ", "").ToLowerInvariant();

            var found = mode == MapMode.Exact
                ? ValList.Where(f => f.Key.ToLowerInvariant().Equals(searchs))?.Select(f => f.Value).First()
                : ValList.Where(f => f.Key.ToLowerInvariant().Contains(searchs))?.Select(f => f.Value).First();

            if (found == null) return null;
            else return new OptionSetValue((int)found);
        }
    }
}
