using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SrikandiListener.Extension.Entities;

namespace SrikandiPush
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Point");
            MainAsync().GetAwaiter().GetResult();
        }

        static async Task MainAsync()
        {
            Console.WriteLine("Sending package attempt start.");
            Console.WriteLine("Connecting Attempt..");
            const string connectionString = "Endpoint=sb://srikandi.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=b0XvwxK2kcS0MzYxel7zGkoqDkzGHSr30ar30MJFM00=";
            const string queueName = "srikandiyanamap";
            var _client = QueueClient.CreateFromConnectionString(connectionString, queueName);
            Console.WriteLine("Connected! Preparing package.");
            var regarding = new Account
            {
                firstname = "Oka",
                lastname = "Prima",
                topic = "ASBQ"
            };
            Console.WriteLine("Package prepared. Serializing..");

            string Message = JsonConvert.SerializeObject(regarding);
            Console.WriteLine("Serialized. Sending..");

            BrokeredMessage message = new BrokeredMessage(Message);
            await _client.SendAsync(message);
            Console.WriteLine("Package sent!");
        }
    }
}
