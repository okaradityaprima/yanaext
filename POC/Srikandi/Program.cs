namespace SrikandiListener
{
    using System;
    using System.Net;
    using System.ServiceModel.Description;
    using Microsoft.Crm.Sdk.Messages;
    using Microsoft.ServiceBus.Messaging;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Client;
    using Newtonsoft.Json;
    using static SrikandiListener.Extension.Entities;

    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Awaiting message..");
            GetMessage();

            Console.WriteLine("Done.");
        }

        static Account AccountReff = null;
        static void GetMessage()
        {
            Console.WriteLine("Receiving message process start.");
            const string connectionString = "Endpoint=sb://srikandi.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=b0XvwxK2kcS0MzYxel7zGkoqDkzGHSr30ar30MJFM00=";
            const string queueName = "srikandiyanamap";
            var queueClient = QueueClient.CreateFromConnectionString(connectionString, queueName);

            Console.WriteLine("Receiving Message..");
            BrokeredMessage message = queueClient.Receive();
            Console.WriteLine("Message received. Deserializing...");
            AccountReff = JsonConvert.DeserializeObject<Account>(message.GetBody<string>());
            Console.WriteLine("Deserialized.");

            Console.WriteLine("Received Packaged as follows.");
            Console.WriteLine("Topic: " + AccountReff.topic);
            Console.WriteLine("firstname: " + AccountReff.firstname);
            Console.WriteLine("Lastname: " + AccountReff.lastname);

            Console.WriteLine("Receiving Message process complete.");
            if (CreateLead())
            {
                message.Complete();
                message.Abandon();
            }

        }

        public static int _operation;
        public static Guid _guid;
        public static IOrganizationService Service;

        static bool CreateLead()
        {
            try
            {
                var envi = "ktb-uat";
                Console.WriteLine("Attempt create Lead to " + envi + ".");
                
                prepareConnection(out Service, envi);
                if (Service == null) return false;

                var reff = new Entity
                {
                    LogicalName = "lead"
                };

                reff.Attributes["firstname"] = AccountReff.firstname;
                reff.Attributes["lastname"] = AccountReff.lastname;
                reff.Attributes["subject"] = AccountReff.topic;

                var id = Service.Create(reff);
                Console.WriteLine("Created successfully with id: " + id.ToString());
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Process Failed");
                Console.WriteLine(ex.Message);
                
                return false;
            }
        }

        #region Utility
        static void prepareConnection(out IOrganizationService organizationService, string envi = null)
        {
            organizationService = null;
            if (envi == null) return;
            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = "connect.user@ktbmmksidms.onmicrosoft.com";
                clientCredentials.UserName.Password = "Mmksi@2018";

                // For Dynamics 365 Customer Engagement V9.X, set Security Protocol as TLS12
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                // Get the URL from CRM, Navigate to Settings -> Customizations -> Developer Resources
                // Copy and Paste Organization Service Endpoint Address URL
                organizationService = (IOrganizationService)new OrganizationServiceProxy(
                    //new Uri("https://<ProvideYourOrgName>.api.<CRMRegion>.dynamics.com/XRMServices/2011/Organization.svc"
                    new Uri("https://" + envi + ".api.crm5.dynamics.com/XRMServices/2011/Organization.svc"),
                    null,
                    clientCredentials,
                    null
                );

                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;



                    if (userid != Guid.Empty)
                    {
                        Console.WriteLine("Connection Established Successfully...");
                    }
                }
                else
                {
                    Console.WriteLine("Failed to Established Connection!!!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught - " + ex.Message);
            }
        }
        #endregion
    }
}