﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class Employee : IBaseEntity
    {
        #region Keys
        public string DealerCode { get; set; }
        public string Context { get; set; }
        public string ExternalCode { get; set; }
        #endregion Keys

        #region Lookup
        //public string Atasan { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string JobPosition { get; set; }
        //public string Country { get; set; }
        //public string ParentEmployee { get; set; }
        #endregion Lookup

        public int WorkingStatus { get; set; }
        public string Address1 { get; set; }
        public DateTime BirthDate { get; set; }
        public string BirthPlace { get; set; }
        public int Category { get; set; }
        public string DnetId { get; set; }
        public int Education { get; set; }
        public string Email { get; set; }
        public string EmployeeName { get; set; }
        //public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        //public bool HasilInterface { get; set; }
        public string IdentificationNo { get; set; }
        public int IdentificationType { get; set; }
        //public int InterfaceStatus { get; set; }
        public DateTime JoinDate { get; set; }
        public int KategoriTeam { get; set; }
        public string KodeAtasan { get; set; }
        public int MaritalStatus { get; set; }
        public string MobilePhone { get; set; }
        public bool Outsource { get; set; }
        public int Permanent { get; set; }
        public int Position { get; set; }
        public string PostalCode { get; set; }
        public int Religion { get; set; }
        public DateTime ResignDate { get; set; }
        public string ResignReason { get; set; }
        public int ResignReasonType { get; set; }
        public int ResignStatus { get; set; }
        public int ResignType { get; set; }
        public int SalesmanArea { get; set; }
        public string SalesmanCodeReff { get; set; }
        public int SalesmanLevel { get; set; }
        public int Type { get; set; }
        public int UkuranBaju { get; set; }
    }
}