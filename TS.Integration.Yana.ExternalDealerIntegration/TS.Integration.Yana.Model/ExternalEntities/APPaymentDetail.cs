﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class APPaymentDetail
    {
        #region Unnecessary
        //public string AccountPayablePaymentDetailNumber { get; set; }
        #endregion

        #region Lookup
        public string AccountPayablePaymentNo { get; set; }
        public string AccountPayableVoucherNo { get; set; }
        public string VendorNo { get; set; }
        public string OrderPurchaseOrderNo { get; set; }

        //Const Candidate
        //public string BusinessunitId { get; set; }
        //public string TransactionCurrencyId { get; set; }
        //public string ParentBusinessunitId { get; set; }
        //public string OwnerId { get; set; }
        #endregion

        #region AmtBase
        //public decimal OutstandingBalance_Base { get; set; }
        //public decimal RemainingBalance_Base { get; set; }
        //public decimal PaymentAmount_Base { get; set; }
        #endregion

        public decimal? DifferenceValue { get; set; }
        public string ExternalDocumentNumber { get; set; }
        public int ExternalDocumentType { get; set; }  
        public decimal OutstandingBalance { get; set; }
        public decimal PaymentAmount { get; set; }
        public Boolean ReceiptFromVendor { get; set; }
        public decimal RemainingBalance { get; set; }
        public int SourceType { get; set; }

        #region Const Candidate
        //public decimal? ExchangeRate { get; set; }
        //public DateTime OrderDate { get; set; } <- taken from PO
        #endregion
    }
}
