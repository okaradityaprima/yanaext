﻿using System;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    [DataContract]
    public class SPK_Old: IBaseEntity
    {
        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string Context { get; set; }

        [DataMember]
        public string ExternalCode { get; set; }

        [DataMember]
        public Guid Id { get; set; }

    }
}
