﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    [DataContract]
    public class Suspect : IBaseEntity
    {
        [DataMember]
        public string DealerCode { get; set; }

        [DataMember]
        public string Context { get; set; }

        [DataMember]
        public string ExternalCode { get; set; }
    }
}
