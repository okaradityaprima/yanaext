﻿namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class PartApprovalCreditLimit : IBaseEntity
    {
        public string DealerCode { get; set; }
        public string Context { get; set; }
        public string ExternalCode { get; set; }
    }
}
