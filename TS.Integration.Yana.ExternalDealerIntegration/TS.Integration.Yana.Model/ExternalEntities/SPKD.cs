﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class SPKD
    {
        #region	Boolean
        public bool HasilInterface { get; set; }
        public bool KonfirmasiCS { get; set; }
        public bool BookingFeeDP { get; set; }
        public bool AlreadyDelivered { get; set; }
        public bool Change { get; set; }
        public bool DownPaymentisPaid { get; set; }
        public bool FixedPrice { get; set; }
        public bool InsuranceFreeofCharge { get; set; }
        public bool InsuranceNeeded { get; set; }
        public bool MatchingRulePassed { get; set; }
        public bool OverMaximumDiscount { get; set; }
        public bool RegistrationFeeisPaid { get; set; }
        public bool RegistrationRequired { get; set; }
        public bool RequestPlateNumber { get; set; }
        #endregion

        #region	DateTime
        public DateTime ContractDate { get; set; }
        public DateTime TanggalKonfirmasi { get; set; }
        public DateTime ConfirmedPaymentDate { get; set; }
        public DateTime ConfirmedRegistrationPaymentDate { get; set; }
        public DateTime DesiredDeliveryDate { get; set; }
        public DateTime DesiredRegistrationDate { get; set; }
        public DateTime ExpectedPaymentDate { get; set; }
        public DateTime ExpectedRegistrationDate { get; set; }
        public DateTime ExpectedRegistrationPaymentDate { get; set; }
        public DateTime InsuranceExpiredDate { get; set; }
        public DateTime PromiseDeliveryDate { get; set; }
        public DateTime RegistrationExpectedDate { get; set; }
        public DateTime RegistrationResultDate { get; set; }
        public DateTime RequestedPlateNumberDate { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime AssignNVSOtoSalesDate { get; set; }
        public DateTime CancelDeliveryDate { get; set; }
        public DateTime CanceledDate { get; set; }
        public DateTime CertificateofTitleNumberReceiptDate { get; set; }
        public DateTime CertificateofTitleNumberSubmitDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime FinancingPODate { get; set; }
        public DateTime InvoiceDueDate { get; set; }
        public DateTime MatchDate { get; set; }
        public DateTime NVSalesCertificateReceiptDate { get; set; }
        public DateTime NVSalesCertificateSubmitDate { get; set; }
        public DateTime PaymentEndDate { get; set; }
        public DateTime PaymentStartDate { get; set; }
        public DateTime PDICancellationDate { get; set; }
        public DateTime PreferredRegistrationDate { get; set; }
        public DateTime PODate { get; set; }
        public DateTime InvoicePostingDate { get; set; }
        public DateTime ScheduledDeliveryDate { get; set; }
        public DateTime? TransactionDate { get; set; }
        public DateTime UnmatchDate { get; set; }
        public DateTime VehicleRegistrationDeliveryDate { get; set; }
        public DateTime VehicleRegistrationReceiptDate { get; set; }
        public DateTime VehicleRegistrationSubmitDate { get; set; }
        #endregion
        
        #region	Double
        public double InterestPercentage { get; set; }
        public double VehicleDiscountPercentage { get; set; }
        #endregion

        #region	Integer
        public int DNetID { get; set; }
        public int DNetSPKCustomerID { get; set; }
        public int IDDNet { get; set; }
        public int BonusPayment1ApplyMonth { get; set; }
        public int BonusPayment2ApplyMonth { get; set; }
        public int InsurancePeriod { get; set; }
        public int RegistrationAgencyLookupType { get; set; }
        public int FinancingPeriod { get; set; }
        public int NumberofCopy { get; set; }
        public int PotentialCustomerLookupType { get; set; }
        public int TotalPaymentMonth { get; set; }
        #endregion

        #region	Lookup
        public string Campaign { get; set; }
        public string KaroseriCompany { get; set; }
        public string Kecamatan { get; set; }
        public string Kelurahan { get; set; }
        public string Leasing { get; set; }
        public string Benefit { get; set; }
        public string OpportunityNo { get; set; }
        public string Program { get; set; }
        public string VehicleCategory { get; set; }
        public string VehicleModel { get; set; }
        public string LastDocumentRegistrationNo { get; set; }
        public string AcquisitionTax { get; set; }
        public string AutomobileTax { get; set; }
        public string DeliveryLocation { get; set; }
        public string InsuranceCategory { get; set; }
        public string PlateNoSegment1 { get; set; }
        public string RegistrationAddress { get; set; }
        public string RegistrationAgencyBU { get; set; }
        public string RegistrationAgency { get; set; }
        public string RegistrationDocumentRefNo { get; set; }
        public string RegistrationProcessBy { get; set; }
        public string RegistrationRequestNo { get; set; }
        public string ServiceBU { get; set; }
        public string NamaFakturBPKPdanSTNK { get; set; }
        public string WeightTax { get; set; }
        public string AddressCode { get; set; }
        public string BillTo { get; set; }
        public string Company { get; set; }
        public string ConsumptionTax1 { get; set; }
        public string ConsumptionTax2 { get; set; }
        public string BillToFinancing { get; set; }
        public string Grade { get; set; }
        public string InsuranceCompany { get; set; }
        public string MethodofPayment { get; set; }
        public string MiscChargesTemplate { get; set; }
        public string NVSalesQuotationNo { get; set; }
        public string Opportunity { get; set; }
        public string OrderType { get; set; }
        public string OrigNVSORefNo { get; set; }
        public string Other { get; set; }
        public string VehicleOwner { get; set; }
        public string PersoninCharge { get; set; }
        public string PotentialCustomerContacts { get; set; }
        public string PotentialCustomer { get; set; }
        public string Configuration { get; set; }
        public string ExteriorColor { get; set; }
        public string Product { get; set; }
        public string InteriorColor { get; set; }
        public string Style { get; set; }
        public string PurchaseOrderNo { get; set; }
        public string RecommendedProduct { get; set; }
        public string SalesOrderNo { get; set; }
        public string SalesPerson { get; set; }
        public string Site { get; set; }
        public string VehiclePriceDetail { get; set; }
        public string StockNumber { get; set; }
        public string TermsofPayment { get; set; }
        public string User { get; set; }
        public string VehiclePriceList { get; set; }
        public string VehicleSpecification { get; set; }
        public string Warehouse { get; set; }
        public string SPKNo { get; set; }
        public string WithholdingTax2 { get; set; }
        public string WithholdingTax1 { get; set; }
        #endregion

        #region	Memo
        public string Accessories_tidakdimantainstock { get; set; }
        #endregion

        #region	Money
        public decimal? AgeYearDiscount { get; set; }
        public decimal? BiayaMediator { get; set; }
        public decimal? CashbackAPM { get; set; }
        public decimal? DPPengurangTax { get; set; }
        public decimal? EstimasiHarga { get; set; }
        public decimal? OngkosKirim { get; set; }
        public decimal? AccessoriesAcquisitionTaxAmount { get; set; }
        public decimal? AccEstProfitAmount { get; set; }
        public decimal? AccEstTotalCost { get; set; }
        public decimal? AdditionalFluorocarbonFee { get; set; }
        public decimal? AdditionalShredderDustFee { get; set; }
        public decimal? AirbagFee { get; set; }
        public decimal? AutomobileTaxAmount { get; set; }
        public decimal? BonusPaymentAmount1 { get; set; }
        public decimal? BonusPaymentAmount2 { get; set; }
        public decimal? FluorocarbonFee { get; set; }
        public decimal? FundManagementFee { get; set; }
        public decimal? InformationManagementFee { get; set; }
        public decimal? MethodofPaymentEstCost { get; set; }
        public decimal? MethodofPaymentEstProfit { get; set; }
        public decimal? OtherTaxAmount { get; set; }
        public decimal? RecyclingAmount { get; set; }
        public decimal? RegistrationPaymentAmount { get; set; }
        public decimal? ShredderDustFee { get; set; }
        public decimal? TotalAcquisitionTaxAmount { get; set; }
        public decimal? TotalEstCostAmount { get; set; }
        public decimal? TotalEstProfitAmount { get; set; }
        public decimal? TotalNetSalesAmount { get; set; }
        public decimal? TotalSalesPrice { get; set; }
        public decimal? VariousCostAmount { get; set; }
        public decimal? VariousEstProfitAmount { get; set; }
        public decimal? VehicleAcquisitionTaxAmount { get; set; }
        public decimal? VehicleEstimatedCost { get; set; }
        public decimal? VehicleEstProfitAmount { get; set; }
        public decimal? VoluntaryInsuranceAmount { get; set; }
        public decimal? WeightTaxAmount { get; set; }
        public decimal? AccessoriesAmount { get; set; }
        public decimal? AccessoriesDiscountAmount { get; set; }
        public decimal? AccessoriesNetSalesPrice { get; set; }
        public decimal? AccessoriesTaxAmount { get; set; }
        public decimal? AdministrationFee { get; set; }
        public decimal? AnnualPaymentAmount { get; set; }
        public decimal? DownPaymentAmountReceived { get; set; }
        public decimal? BankChargesFee { get; set; }
        public decimal? BookingFeeAmount { get; set; }
        public decimal? CalculatedPaymentAmountPayment { get; set; }
        public decimal? CompulsoryInsuranceAmount { get; set; }
        public decimal? ConsumptionTax1Amount { get; set; }
        public decimal? ConsumptionTax2Amount { get; set; }
        public decimal? ConsumptionTaxTotalAmount { get; set; }
        public decimal? DealerDiscount { get; set; }
        public decimal? DiscountAmount { get; set; }
        public decimal? DiscountAmountBeforeTax { get; set; }
        public decimal? DownPaymentAmount { get; set; }
        public decimal? DownPaymentAmountPayment { get; set; }
        public decimal? FinancingAmount { get; set; }
        public decimal? FirstPaymentAmount { get; set; }
        public decimal? InterestAmount { get; set; }
        public decimal? MaximumDiscountAmount { get; set; }
        public decimal? MethodofPaymentEstSalesAmount { get; set; }
        public decimal? MiscChargeBaseAmount { get; set; }
        public decimal? MiscellaneousChargesTaxAmount { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountAfterTax { get; set; }
        public decimal? NonTaxableMiscellaneousChargeAmount { get; set; }
        public decimal? OtherDeductionAmountPayment { get; set; }
        public decimal? OutstandingAmount { get; set; }
        public decimal? PriceAmountBeforeTax { get; set; }
        public decimal? ReferralAmount { get; set; }
        public decimal? RemainingFinanceAmount { get; set; }
        public decimal? SpecialColorPriceAmount { get; set; }
        public decimal? SubsidizedDiscount { get; set; }
        public decimal? SubsidizedLeasing { get; set; }
        public decimal? SubsidizedMainDealer { get; set; }
        public decimal? SubsidizedSoleAgent { get; set; }
        public decimal? TaxableMiscellaneousChargeAmount { get; set; }
        public decimal? TitleRegistrationFee { get; set; }
        public decimal? TotalDiscountAmount { get; set; }
        public decimal? TotalPayment { get; set; }
        public decimal? TotalPriceAmount { get; set; }
        public decimal? TotalReceiptAmount { get; set; }
        public decimal? TradeInDiscount { get; set; }
        public decimal? TradeInNetValue { get; set; }
        public decimal? TradeInNetValuePayment { get; set; }
        public decimal? TradeInRecyclingAmount { get; set; }
        public decimal? TradeInTaxAmount { get; set; }
        public decimal? TradeInValue { get; set; }
        public decimal? VariousAmount { get; set; }
        public decimal? VehicleAmount { get; set; }
        public decimal? VehicleDiscountAmount { get; set; }
        public decimal? VehicleNetSalesPrice { get; set; }
        public decimal? VehicleRelatedProductAmount { get; set; }
        public decimal? VehicleRelatedProductTaxAmount { get; set; }
        public decimal? VehicleTaxAmount { get; set; }
        public decimal? WithholdingTax2Amount { get; set; }
        public decimal? WithholdingTax1Amount { get; set; }
        #endregion
        
        #region	Picklist
        public int Tambahan { get; set; }
        public int BiroJasa { get; set; }
        public int CategoryCode { get; set; }
        public int BentukBodyKendaraanCV { get; set; }
        public int BentukBodyKendaraanLCV { get; set; }
        public int JenisKendaraan { get; set; }
        public int BidangUsahaKonsumen { get; set; }
        public int DaerahUtamaOperasi { get; set; }
        public int ModelKendaraan { get; set; }
        public int KepemilikanKendaraan { get; set; }
        public int KendaraanIniSbgKend { get; set; }
        public int KendaraanSebagai { get; set; }
        public int PenggunaanUtamaKendaraan { get; set; }
        public int KeperluanAngkutan { get; set; }
        public int UsiaPemilikKendaraan { get; set; }
        public int HandlingInvoiceCompany { get; set; }
        public int? Pembayaran { get; set; }
        public int AlasanBatalValue { get; set; }
        public int StatusInvoiceCompany { get; set; }
        public int InsuranceApplyMethod { get; set; }
        public int LocationClassCategory { get; set; }
        public int OwnershipCategory { get; set; }
        public int RegistrationMethod { get; set; }
        public int TaxableBusinessCategory { get; set; }
        public int TaxationForm { get; set; }
        public int UsageCategory { get; set; }
        public int VehicleManagementCategory { get; set; }
        public int Handling { get; set; }
        public int InsuranceType { get; set; }
        public int MatchingType { get; set; }
        public int? PriceOption { get; set; }
        public int POStatus { get; set; }
        public int State { get; set; }
        public int TradeinTaxCategory { get; set; }
        public int VehicleOrderInvoicing { get; set; }
        #endregion

        #region	String
        public string Address4 { get; set; }
        public string BUStock { get; set; }
        public string ContractNumber { get; set; }
        public string DNetSPKNumber { get; set; }
        public string LKPPNumber { get; set; }
        public string Mediator { get; set; }
        public string AlasanBatal { get; set; }
        public string Remarks { get; set; }
        public string VehicleDescription { get; set; }
        public string IdempotentMessage { get; set; }
        public string PlateNo { get; set; }
        public string PlateNoSegment2 { get; set; }
        public string PlateNoSegment3 { get; set; }
        public string PlateNoSegment4 { get; set; }
        public string ProductSubsidyCategory { get; set; }
        public string Remark { get; set; }
        public string VehicleManagementNo { get; set; }
        public string VehiclespecificNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string BillToCompleteAddress { get; set; }
        public string BillToDescription { get; set; }
        public string BillToNo { get; set; }
        public string BillToPhone { get; set; }
        public string CertificateofTitleNo { get; set; }
        public string ClassificationNumber { get; set; }
        public string CompanyCompleteAddress { get; set; }
        public string CompanyDescription { get; set; }
        public string CompanyNo { get; set; }
        public string CompanyPhone { get; set; }
        public string CustomerContactDescription { get; set; }
        public string CustomerNo { get; set; }
        public string EventData { get; set; }
        public string FinancingPONo { get; set; }
        public string GradeDescription { get; set; }
        public string GradeNo { get; set; }
        public string Locking { get; set; }
        public string Modelspecification { get; set; }
        public string NVSalesCertificateNo { get; set; }
        public string SPKDetailNo { get; set; }
        public string OtherCompleteAddress { get; set; }
        public string OtherDescription { get; set; }
        public string OtherNo { get; set; }
        public string OtherPhone { get; set; }
        public string OwnerCompleteAddress { get; set; }
        public string OwnerDescription { get; set; }
        public string OwnerNo { get; set; }
        public string OwnerPhone { get; set; }
        public string PhoneNo { get; set; }
        public string PostalCode { get; set; }
        public string PotentialCustomerDescription { get; set; }
        public string ProductDescription { get; set; }
        public string RegistrationAgencyInvoiceNo { get; set; }
        public string RequestedPlateNumber { get; set; }
        public string UserCompleteAddress { get; set; }
        public string UserDescription { get; set; }
        public string UserNo { get; set; }
        public string UserPhone { get; set; }
        public string VehicleManagementCompleteAddress { get; set; }
        public string VehicleManagementDescription { get; set; }
        public string VehicleManagementPhone { get; set; }
        public string VehicleRegistrationNo { get; set; }
        #endregion

        #region Potential Unnecessary
        //public string ProcessId { get; set; }
        //public string StageId { get; set; }
        //public string NewVehicleSalesOrderID { get; set; }
        //public string Log { get; set; }
        #endregion

        #region Populated
        //public string Owner { get; set; }
        //public string Currency { get; set; }
        //public decimal? ExchangeRate { get; set; }
        //public string PBU { get; set; }
        //public string BU { get; set; }
        //public string BlankoSPKNo { get; set; } //Get from Created DetailBlanko
        #endregion
    }
}
