﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class SPK : IBaseEntity
    {
        #region Keys
        public string DealerCode { get; set; }
        public string Context { get; set; }
        public string ExternalCode { get; set; }
        #endregion

        #region PreRequisite Key
        //Prospect
        public string OpportunityNo { get; set; }
        public Prospect Prospect { get; set; }

        //Contact
        public string ContactCity { get; set; }
        public string ContactPOBox { get; set; }
        public string ContactIdentificationNo { get; set; }
        #endregion

        #region Amount
        public decimal AccessoriesAmount { get; set; }
        public decimal AccessoriesDiscountAmount { get; set; }
        public decimal Balance { get; set; }
        public decimal BookingFeeAmount { get; set; }
        public decimal CompulsoryInsuranceAmount { get; set; }
        public decimal ConsumptionTaxTotalAmount { get; set; }
        public decimal CouponAmount { get; set; }
        public decimal DealerDiscount { get; set; }
        public decimal DownPaymentAmount { get; set; }
        public decimal DownPaymentAmountReceived { get; set; }
        public decimal NetAmount { get; set; }
        public decimal NetAmountAfterTax { get; set; }
        public decimal NonTaxableMiscellaneousChargeAmount { get; set; }
        public decimal OtherTaxAmount { get; set; }
        public decimal OutstandingAmount { get; set; }
        public decimal RecyclingAmount { get; set; }
        public decimal ReferralAmount { get; set; }
        public decimal SpecialColorPriceAmount { get; set; }
        public decimal SubsidizedDiscount { get; set; }
        public decimal TaxableMiscellaneousChargeAmount { get; set; }
        public decimal TitleRegistrationFee { get; set; }
        public decimal TotalPriceAmount { get; set; }
        public decimal TotalReceipt { get; set; }
        public decimal TradeInAmount { get; set; }
        public decimal TradeInRecyclingAmount { get; set; }
        public decimal VehicleAmount { get; set; }
        public decimal VehicleDiscountAmount { get; set; }
        public decimal VehicleRelatedProductAmount { get; set; }
        public decimal VoluntaryInsuranceAmount { get; set; }
        #endregion

        #region LookUp
        public string AddressCode { get; set; }
        public string Benefit { get; set; }
        public string BillTo { get; set; }
        public string Campaign { get; set; }
        public string Karoseri { get; set; }
        public string Kecamatan { get; set; }
        public string Kelurahan { get; set; }
        public string LeasingCompany { get; set; }
        public string NamaKonsumenFaktur { get; set; }
        public string NamaKonsumenSPK { get; set; }
        public string BlankoNo { get; set; } // -> NVSORegistrationDetailNumber

        
        public string PotentialCustomerContacts { get; set; }
        public string EmployeeCode { get; set; } // -> SalesPerson

        public string TermsofPayment { get; set; }
        #endregion

        #region OptionSet
        public int? AlasanBatalOptions { get; set; }
        public int? BentukBodyKendaraanCV { get; set; }
        public int? BentukBodyKendaraanLCV { get; set; }
        public int? BidangUsahaKonsumen { get; set; }
        public int? CaraPembayaran { get; set; }
        public int? CategoryCode { get; set; }
        public int? DaerahUtamaOperasi { get; set; }
        public int? Handling { get; set; }
        //public int Handling { get; set; } //Core Attribute
        public int? JenisKendaraan { get; set; }
        public int? KendaraanIniSbgKend { get; set; }
        public int? KendaraanSebagai { get; set; }
        public int? KepemilikanKendaraan { get; set; }
        public int? KeperluanAngkutan { get; set; }
        public int? ModelKendaraan { get; set; }
        public int? PenggunaanUtamaKendaraan { get; set; }

        #endregion

        #region Date
        public DateTime ContractDate { get; set; }

        #endregion

        #region ASIS
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string AlasanBatal { get; set; }
        public Boolean begbal { get; set; }
        public string BillToNo { get; set; } //Might not need
        public string ContractNumber { get; set; }
        public string CustomerNo { get; set; } //Might not need
        public string DNetSPKNumber { get; set; }
        public Boolean DownPaymentisPaid { get; set; }
        public string InterfaceMessage { get; set; } //Might not need
        public Boolean IsInterfaced { get; set; } //Might not need
        public string LKPPNo { get; set; }
        public string PhoneNo { get; set; }
        public string PostalCode { get; set; }
        public int? Qty { get; set; }
        public int DNetID { get; set; }
        public int DNetSAPCustomerId { get; set; }
        public int DNetSPKCustomerID { get; set; }
        public int DNetSPKDetailID { get; set; }
        #endregion

        public string Remarks { get; set; }
        public DateTime ScheduledDeliveryDate { get; set; }
        
        public string SPKDnetReference { get; set; }
        public int State { get; set; }
        public int Status { get; set; }
        public int StatusInterfaceDNet { get; set; }
        
        public int? Tambahan { get; set; }
        
        public int? TempDocument { get; set; }
        
        public DateTime? TransactionDate { get; set; }
        public int? UsiaPemilikKendaraan { get; set; }
        
        public Double VehicleDiscountPercentage { get; set; }

        public List<SPKD> Details { get; set; }

        public List<Attachment> Attachment { get; set; }

    }

    public class Prospect
    {
        public string Topic { get; set; }
        public string ContactName { get; set; }
        public string ProductCode { get; set; }
        public DateTime LeadDate { get; set; }
        
        public List<Attachment> ContactAttachment { get; set; }
        public List<Attachment> ProspectAttachment { get; set; }
    }

    public class Attachment
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string FileName { get; set; }
        public string FileFormat { get; set; }
        public string FileMimeType { get; set; }
    }
}
