﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class ARReceipt : IBaseEntity
    {
        #region Keys
        public string DealerCode { get; set; }
        public string Context { get; set; }
        public string ExternalCode { get; set; }
        #endregion

        public decimal? AppliedtoDocument { get; set; }
        public decimal? AvailableBalance { get; set; }
        public bool BookingFee { get; set; }
        public bool Cancelled { get; set; }

        //unnecessary public string CustomerDescription { get; set; }
        //unnecessary CustomerNo.
        //unnecessary ExchangeRate
        //ExchangeRateAmount
        //ExchangeRateDate
        //ExchangeRateType

        //unnecessary GeneratedToken
        //unnecessary Handling
        //unnecessary IdempotentMessage
        //unnecessary Say
        //unnecessary State
        //unnecessary Status
        //unnecessary StatusReason

        public decimal? PaymentSettlement { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalOtherExpensesOrIncome { get; set; }
        public decimal? TotalReceiptAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public int Type { get; set; }
        
        #region LookUp

        public string ARReceiptReferenceNo { get; set; }
        public string CustomerNo { get; set; }
        public string MethodofPaymentNo { get; set; }
        public string CashAndBankNo { get; set; }
        public string SPKNo { get; set; }
        #endregion

        #region Populated
        //public string BUId { get; set; }
        //public string PBUId { get; set; }
        //public string CurrencyId { get; set; }
        //public string OwnerId { get; set; }

        //public string CashAndBankDimension1Id { get; set; }
        //public string CashAndBankDimension2Id { get; set; }
        //public string CashAndBankDimension3Id { get; set; }
        //public string CashAndBankDimension4Id { get; set; }
        //public string CashAndBankDimension5Id { get; set; }
        //public string CashAndBankDimension6Id { get; set; }

        //public string ReceivableDimension1Id { get; set; }
        //public string ReceivableDimension2Id { get; set; }
        //public string ReceivableDimension3Id { get; set; }
        //public string ReceivableDimension4Id { get; set; }
        //public string ReceivableDimension5Id { get; set; }
        //public string ReceivableDimension6Id { get; set; }
        #endregion
            
        public List<ARReceiptDetail> Details { get; set; }
    }
}
