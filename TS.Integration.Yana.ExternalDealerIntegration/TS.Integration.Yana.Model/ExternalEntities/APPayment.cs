﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class APPayment : IBaseEntity
    {
        #region Keys
        public string DealerCode { get; set; }
        public string Context { get; set; }
        public string ExternalCode { get; set; }
        #endregion

        public DateTime ActualPaymentDate { get; set; }
        public decimal AppliedToDocument { get; set; }
        public decimal PaymentSettlement { get; set; } 
        public string CancelReason { get; set; }
        public Boolean Cancelled { get; set; }
        public decimal TotalPaymentAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public int Type { get; set; }

        //Const Candidate
        //public int Status { get; set; }  
        //public decimal? ExchangeRate { get; set; }  
        //public Boolean IsInterfaced { get; set; }

        #region Unnecessary - SFL
        //public string IdempotentMessage { get; set; }
        //public string AccountPayablePaymentNumber { get; set; }
        #endregion

        #region AmtBase
        //public decimal AppliedToDocument_Base { get; set; }
        //public decimal PaymentSettlement_Base { get; set; }
        //public decimal TotalPaymentAmount_Base { get; set; } 
        #endregion

        #region Lookup
        public string VendorNo { get; set; }
        public string CashAndBankNo { get; set; }
        public string MethodofPaymentNo { get; set; }
        public string AccountPayableReferenceNo { get; set; }
        public string AccountPayableVoucherReferenceNo { get; set; }

        //Const Candidate
        //public string OwnerId { get; set; }
        //public string BusinessunitId { get; set; }
        //public string ParentBusinessunitId { get; set; } 
        //public string TransactionCurrencyId { get; set; }
        #endregion

        public List<APPaymentDetail> Details { get; set; }
    }
}
