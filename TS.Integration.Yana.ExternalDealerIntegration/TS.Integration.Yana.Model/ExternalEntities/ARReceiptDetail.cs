﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Model.ExternalEntities
{
    public class ARReceiptDetail
    {
        #region LookUp
        public string CustomerNo { get; set; }
        public string InvoiceNo { get; set; }
        public string OrderNoSONo { get; set; }
        public string OrderNoUVSONo { get; set; }
        public string OrderNoWONo { get; set; }
        public string OrderNoWriteOffBalanceNo { get; set; }
        public string OrderTypeNo { get; set; }
        public string SPKDetailNo { get; set; }
        public string OrderNoLookupName { get; set; }
        public string OrderNoLookupType { get; set; }
        #endregion

        public decimal? ChangeAmount { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal? ReceiptAmount { get; set; }
        public decimal? OutstandingBalance { get; set; }
        public decimal? RemainingBalance { get; set; }
        public bool PaidBackToCustomer { get; set; }
        public int SourceType { get; set; }

        #region Unnecessary
        //ExchangeRateAmount
        //ExchangeRateDate
        //ExchangeRateType
        //public string ARReceiptId { get; set; }
        #endregion

        #region Populated
        //public string BUId { get; set; }
        //public string CurrencyId { get; set; }
        //public string OwnerId { get; set; }
        //public string PBUId { get; set; }
        //public int ExchangeRate { get; set; }
        #endregion
    }
}
