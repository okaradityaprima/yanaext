﻿using Microsoft.Xrm.Sdk;
using System.Runtime.Serialization;

namespace TS.Integration.Yana.Model
{
    public interface IBaseEntity
    {
        string DealerCode { get; set; }

        string Context { get; set; }

        string ExternalCode { get; set; }
        
    }

    public class BaseEntity
    {
        public string DealerCode { get; set; }

        public string Context { get; set; }

        public string ExternalCode { get; set; }

    }
}
