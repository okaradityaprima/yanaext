﻿namespace TS.Integration.Yana.Model.YanaEntities
{
    #region AP Payment
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_accountpayablepayment")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_accountpayablepayment : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_accountpayablepayment
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_isinterfaced")]
        public System.Nullable<bool> ktb_isinterfaced
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_isinterfaced");
            }
            set
            {
                this.OnPropertyChanging("ktb_isinterfaced");
                this.SetAttributeValue("ktb_isinterfaced", value);
                this.OnPropertyChanged("ktb_isinterfaced");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cancelreason")]
        public string ktb_cancelreason
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_cancelreason");
            }
            set
            {
                this.OnPropertyChanging("ktb_cancelreason");
                this.SetAttributeValue("ktb_cancelreason", value);
                this.OnPropertyChanged("ktb_cancelreason");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_say")]
        public string ktb_say
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_say");
            }
            set
            {
                this.OnPropertyChanging("ktb_say");
                this.SetAttributeValue("ktb_say", value);
                this.OnPropertyChanged("ktb_say");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_invoiceno")]
        public string ktb_invoiceno
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_invoiceno");
            }
            set
            {
                this.OnPropertyChanging("ktb_invoiceno");
                this.SetAttributeValue("ktb_invoiceno", value);
                this.OnPropertyChanged("ktb_invoiceno");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_salesorderno")]
        public string ktb_salesorderno
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_salesorderno");
            }
            set
            {
                this.OnPropertyChanging("ktb_salesorderno");
                this.SetAttributeValue("ktb_salesorderno", value);
                this.OnPropertyChanged("ktb_salesorderno");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_bankgiro")]
        public string ktb_bankgiro
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_bankgiro");
            }
            set
            {
                this.OnPropertyChanging("ktb_bankgiro");
                this.SetAttributeValue("ktb_bankgiro", value);
                this.OnPropertyChanged("ktb_bankgiro");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_girono")]
        public string ktb_girono
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_girono");
            }
            set
            {
                this.OnPropertyChanging("ktb_girono");
                this.SetAttributeValue("ktb_girono", value);
                this.OnPropertyChanged("ktb_girono");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_giroduedate")]
        public System.Nullable<System.DateTime> ktb_giroduedate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_giroduedate");
            }
            set
            {
                this.OnPropertyChanging("ktb_giroduedate");
                this.SetAttributeValue("ktb_giroduedate", value);
                this.OnPropertyChanged("ktb_giroduedate");
            }
        }
    }
    #endregion

    #region Employee
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_employee")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_employee : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_employee
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_outsource")]
        public System.Nullable<bool> ktb_outsource
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_outsource");
            }
            set
            {
                this.OnPropertyChanging("ktb_outsource");
                this.SetAttributeValue("ktb_outsource", value);
                this.OnPropertyChanged("ktb_outsource");
            }
        }

        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_birthplace")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_birthplace
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_birthplace");
            }
            set
            {
                this.OnPropertyChanging("ktb_birthplace");
                this.SetAttributeValue("ktb_birthplace", value);
                this.OnPropertyChanged("ktb_birthplace");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_jobpositionid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_jobpositionid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_jobpositionid");
            }
            set
            {
                this.OnPropertyChanging("ktb_jobpositionid");
                this.SetAttributeValue("ktb_jobpositionid", value);
                this.OnPropertyChanged("ktb_jobpositionid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_superiors")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_superiors
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_superiors");
            }
            set
            {
                this.OnPropertyChanging("ktb_superiors");
                this.SetAttributeValue("ktb_superiors", value);
                this.OnPropertyChanged("ktb_superiors");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_salespersoncode")]
        public string ktb_salespersoncode
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_salespersoncode");
            }
            set
            {
                this.OnPropertyChanging("ktb_salespersoncode");
                this.SetAttributeValue("ktb_salespersoncode", value);
                this.OnPropertyChanged("ktb_salespersoncode");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_shirtsize")]
        public string ktb_shirtsize
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_shirtsize");
            }
            set
            {
                this.OnPropertyChanging("ktb_shirtsize");
                this.SetAttributeValue("ktb_shirtsize", value);
                this.OnPropertyChanged("ktb_shirtsize");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_interfaceexceptionmessage")]
        public string ktb_interfaceexceptionmessage
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_interfaceexceptionmessage");
            }
            set
            {
                this.OnPropertyChanging("ktb_interfaceexceptionmessage");
                this.SetAttributeValue("ktb_interfaceexceptionmessage", value);
                this.OnPropertyChanged("ktb_interfaceexceptionmessage");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_employeecode")]
        public string ktb_employeecode
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_employeecode");
            }
            set
            {
                this.OnPropertyChanging("ktb_employeecode");
                this.SetAttributeValue("ktb_employeecode", value);
                this.OnPropertyChanged("ktb_employeecode");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_education")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_education
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_education");
            }
            set
            {
                this.OnPropertyChanging("ktb_education");
                this.SetAttributeValue("ktb_education", value);
                this.OnPropertyChanged("ktb_education");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_category")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_category
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_category");
            }
            set
            {
                this.OnPropertyChanging("ktb_category");
                this.SetAttributeValue("ktb_category", value);
                this.OnPropertyChanged("ktb_category");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_identificationno")]
        public string ktb_identificationno
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_identificationno");
            }
            set
            {
                this.OnPropertyChanging("ktb_identificationno");
                this.SetAttributeValue("ktb_identificationno", value);
                this.OnPropertyChanged("ktb_identificationno");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_maritalstatus")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_maritalstatus
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_maritalstatus");
            }
            set
            {
                this.OnPropertyChanging("ktb_maritalstatus");
                this.SetAttributeValue("ktb_maritalstatus", value);
                this.OnPropertyChanged("ktb_maritalstatus");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_position")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_position
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_position");
            }
            set
            {
                this.OnPropertyChanging("ktb_position");
                this.SetAttributeValue("ktb_position", value);
                this.OnPropertyChanged("ktb_position");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_religion")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_religion
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_religion");
            }
            set
            {
                this.OnPropertyChanging("ktb_religion");
                this.SetAttributeValue("ktb_religion", value);
                this.OnPropertyChanged("ktb_religion");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_resignreason")]
        public string ktb_resignreason
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_resignreason");
            }
            set
            {
                this.OnPropertyChanging("ktb_resignreason");
                this.SetAttributeValue("ktb_resignreason", value);
                this.OnPropertyChanged("ktb_resignreason");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_salary")]
        public System.Nullable<int> ktb_salary
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_salary");
            }
            set
            {
                this.OnPropertyChanging("ktb_salary");
                this.SetAttributeValue("ktb_salary", value);
                this.OnPropertyChanged("ktb_salary");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_superiorsname")]
        public string ktb_superiorsName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_superiorsname");
            }
            set
            {
                this.OnPropertyChanging("ktb_superiorsName");
                this.SetAttributeValue("ktb_superiorsname", value);
                this.OnPropertyChanged("ktb_superiorsName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_birthplacename")]
        public string ktb_birthplaceName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_birthplacename");
            }
            set
            {
                this.OnPropertyChanging("ktb_birthplaceName");
                this.SetAttributeValue("ktb_birthplacename", value);
                this.OnPropertyChanged("ktb_birthplaceName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_ukuranbaju")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_ukuranbaju
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_ukuranbaju");
            }
            set
            {
                this.OnPropertyChanging("ktb_ukuranbaju");
                this.SetAttributeValue("ktb_ukuranbaju", value);
                this.OnPropertyChanged("ktb_ukuranbaju");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_salesmanlevel")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_salesmanlevel
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_salesmanlevel");
            }
            set
            {
                this.OnPropertyChanging("ktb_salesmanlevel");
                this.SetAttributeValue("ktb_salesmanlevel", value);
                this.OnPropertyChanged("ktb_salesmanlevel");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_salesmanarea")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_salesmanarea
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_salesmanarea");
            }
            set
            {
                this.OnPropertyChanging("ktb_salesmanarea");
                this.SetAttributeValue("ktb_salesmanarea", value);
                this.OnPropertyChanged("ktb_salesmanarea");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_kategoriteam")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_kategoriteam
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_kategoriteam");
            }
            set
            {
                this.OnPropertyChanging("ktb_kategoriteam");
                this.SetAttributeValue("ktb_kategoriteam", value);
                this.OnPropertyChanged("ktb_kategoriteam");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_salesmancodereff")]
        public string ktb_salesmancodereff
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_salesmancodereff");
            }
            set
            {
                this.OnPropertyChanging("ktb_salesmancodereff");
                this.SetAttributeValue("ktb_salesmancodereff", value);
                this.OnPropertyChanged("ktb_salesmancodereff");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_isinterfaced")]
        public System.Nullable<bool> ktb_isinterfaced
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_isinterfaced");
            }
            set
            {
                this.OnPropertyChanging("ktb_isinterfaced");
                this.SetAttributeValue("ktb_isinterfaced", value);
                this.OnPropertyChanged("ktb_isinterfaced");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_interfacestatus")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_interfacestatus
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_interfacestatus");
            }
            set
            {
                this.OnPropertyChanging("ktb_interfacestatus");
                this.SetAttributeValue("ktb_interfacestatus", value);
                this.OnPropertyChanged("ktb_interfacestatus");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_interfacehandling")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_interfacehandling
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_interfacehandling");
            }
            set
            {
                this.OnPropertyChanging("ktb_interfacehandling");
                this.SetAttributeValue("ktb_interfacehandling", value);
                this.OnPropertyChanged("ktb_interfacehandling");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_id")]
        public string ktb_ID
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_id");
            }
            set
            {
                this.OnPropertyChanging("ktb_ID");
                this.SetAttributeValue("ktb_id", value);
                this.OnPropertyChanged("ktb_ID");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_identificationtype")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_identificationtype
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_identificationtype");
            }
            set
            {
                this.OnPropertyChanging("ktb_identificationtype");
                this.SetAttributeValue("ktb_identificationtype", value);
                this.OnPropertyChanged("ktb_identificationtype");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_resigntype")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_resigntype
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_resigntype");
            }
            set
            {
                this.OnPropertyChanging("ktb_resigntype");
                this.SetAttributeValue("ktb_resigntype", value);
                this.OnPropertyChanged("ktb_resigntype");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_jobpositionidname")]
        public string ktb_jobpositionidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_jobpositionidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_jobpositionidName");
                this.SetAttributeValue("ktb_jobpositionidname", value);
                this.OnPropertyChanged("ktb_jobpositionidName");
            }
        }
    }
    #endregion

    #region SPK
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_newvehiclewholesaleorder")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_newvehiclewholesaleorder : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_newvehiclewholesaleorder
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_contractnumber")]
        public string ktb_contractnumber
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_contractnumber");
            }
            set
            {
                this.OnPropertyChanging("ktb_contractnumber");
                this.SetAttributeValue("ktb_contractnumber", value);
                this.OnPropertyChanged("ktb_contractnumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_contractdate")]
        public System.Nullable<System.DateTime> ktb_contractdate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_contractdate");
            }
            set
            {
                this.OnPropertyChanging("ktb_contractdate");
                this.SetAttributeValue("ktb_contractdate", value);
                this.OnPropertyChanged("ktb_contractdate");
            }
        }


        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_begbal")]
        public System.Nullable<bool> ktb_begbal
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_begbal");
            }
            set
            {
                this.OnPropertyChanging("ktb_begbal");
                this.SetAttributeValue("ktb_begbal", value);
                this.OnPropertyChanged("ktb_begbal");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_leasingcompanyid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_leasingcompanyid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_leasingcompanyid");
            }
            set
            {
                this.OnPropertyChanging("ktb_leasingcompanyid");
                this.SetAttributeValue("ktb_leasingcompanyid", value);
                this.OnPropertyChanged("ktb_leasingcompanyid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_nvsoregistrationdetailnumberidname")]
        public string ktb_nvsoregistrationdetailnumberidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_nvsoregistrationdetailnumberidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_nvsoregistrationdetailnumberidName");
                this.SetAttributeValue("ktb_nvsoregistrationdetailnumberidname", value);
                this.OnPropertyChanged("ktb_nvsoregistrationdetailnumberidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_campaignidname")]
        public string ktb_campaignidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_campaignidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_campaignidName");
                this.SetAttributeValue("ktb_campaignidname", value);
                this.OnPropertyChanged("ktb_campaignidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_qty")]
        public System.Nullable<int> ktb_qty
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_qty");
            }
            set
            {
                this.OnPropertyChanging("ktb_qty");
                this.SetAttributeValue("ktb_qty", value);
                this.OnPropertyChanged("ktb_qty");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetspkcustomerid")]
        public System.Nullable<int> ktb_dnetspkcustomerid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetspkcustomerid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetspkcustomerid");
                this.SetAttributeValue("ktb_dnetspkcustomerid", value);
                this.OnPropertyChanged("ktb_dnetspkcustomerid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetsapcustomerid")]
        public System.Nullable<int> ktb_dnetsapcustomerid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetsapcustomerid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetsapcustomerid");
                this.SetAttributeValue("ktb_dnetsapcustomerid", value);
                this.OnPropertyChanged("ktb_dnetsapcustomerid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_handling")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_handling
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_handling");
            }
            set
            {
                this.OnPropertyChanging("ktb_handling");
                this.SetAttributeValue("ktb_handling", value);
                this.OnPropertyChanged("ktb_handling");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_status")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_status
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_status");
            }
            set
            {
                this.OnPropertyChanging("ktb_status");
                this.SetAttributeValue("ktb_status", value);
                this.OnPropertyChanged("ktb_status");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_scheduleddeliverydate")]
        public System.Nullable<System.DateTime> ktb_scheduleddeliverydate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_scheduleddeliverydate");
            }
            set
            {
                this.OnPropertyChanging("ktb_scheduleddeliverydate");
                this.SetAttributeValue("ktb_scheduleddeliverydate", value);
                this.OnPropertyChanged("ktb_scheduleddeliverydate");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_tempdocument")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_tempdocument
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_tempdocument");
            }
            set
            {
                this.OnPropertyChanging("ktb_tempdocument");
                this.SetAttributeValue("ktb_tempdocument", value);
                this.OnPropertyChanged("ktb_tempdocument");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_categorycode")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_categorycode
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_categorycode");
            }
            set
            {
                this.OnPropertyChanging("ktb_categorycode");
                this.SetAttributeValue("ktb_categorycode", value);
                this.OnPropertyChanged("ktb_categorycode");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetspkdetailid")]
        public System.Nullable<int> ktb_dnetspkdetailid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetspkdetailid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetspkdetailid");
                this.SetAttributeValue("ktb_dnetspkdetailid", value);
                this.OnPropertyChanged("ktb_dnetspkdetailid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetspknumber")]
        public string ktb_dnetspknumber
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_dnetspknumber");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetspknumber");
                this.SetAttributeValue("ktb_dnetspknumber", value);
                this.OnPropertyChanged("ktb_dnetspknumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_interfaceexceptionmessage")]
        public string ktb_interfaceexceptionmessage
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_interfaceexceptionmessage");
            }
            set
            {
                this.OnPropertyChanging("ktb_interfaceexceptionmessage");
                this.SetAttributeValue("ktb_interfaceexceptionmessage", value);
                this.OnPropertyChanged("ktb_interfaceexceptionmessage");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_lkppno")]
        public string ktb_lkppno
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_lkppno");
            }
            set
            {
                this.OnPropertyChanging("ktb_lkppno");
                this.SetAttributeValue("ktb_lkppno", value);
                this.OnPropertyChanged("ktb_lkppno");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_rejectedreason")]
        public string ktb_rejectedreason
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_rejectedreason");
            }
            set
            {
                this.OnPropertyChanging("ktb_rejectedreason");
                this.SetAttributeValue("ktb_rejectedreason", value);
                this.OnPropertyChanged("ktb_rejectedreason");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_waypaid1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_waypaid1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_waypaid1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_waypaid1");
                this.SetAttributeValue("ktb_cbu_waypaid1", value);
                this.OnPropertyChanged("ktb_cbu_waypaid1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_remarks")]
        public string ktb_remarks
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_remarks");
            }
            set
            {
                this.OnPropertyChanging("ktb_remarks");
                this.SetAttributeValue("ktb_remarks", value);
                this.OnPropertyChanged("ktb_remarks");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_jeniskend")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_jeniskend
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_jeniskend");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_jeniskend");
                this.SetAttributeValue("ktb_cbu_jeniskend", value);
                this.OnPropertyChanged("ktb_cbu_jeniskend");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_modelkend")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_modelkend
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_modelkend");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_modelkend");
                this.SetAttributeValue("ktb_cbu_modelkend", value);
                this.OnPropertyChanged("ktb_cbu_modelkend");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_address4")]
        public string ktb_address4
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_address4");
            }
            set
            {
                this.OnPropertyChanging("ktb_address4");
                this.SetAttributeValue("ktb_address4", value);
                this.OnPropertyChanged("ktb_address4");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_purpose2")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_purpose2
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_purpose2");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_purpose2");
                this.SetAttributeValue("ktb_cbu_purpose2", value);
                this.OnPropertyChanged("ktb_cbu_purpose2");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_nvsoregistrationdetailnumberid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_nvsoregistrationdetailnumberid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_nvsoregistrationdetailnumberid");
            }
            set
            {
                this.OnPropertyChanging("ktb_nvsoregistrationdetailnumberid");
                this.SetAttributeValue("ktb_nvsoregistrationdetailnumberid", value);
                this.OnPropertyChanged("ktb_nvsoregistrationdetailnumberid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_karoseriidname")]
        public string ktb_karoseriidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_karoseriidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_karoseriidName");
                this.SetAttributeValue("ktb_karoseriidname", value);
                this.OnPropertyChanged("ktb_karoseriidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_benefitidname")]
        public string ktb_benefitidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_benefitidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_benefitidName");
                this.SetAttributeValue("ktb_benefitidname", value);
                this.OnPropertyChanged("ktb_benefitidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_karoseriid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_karoseriid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_karoseriid");
            }
            set
            {
                this.OnPropertyChanging("ktb_karoseriid");
                this.SetAttributeValue("ktb_karoseriid", value);
                this.OnPropertyChanged("ktb_karoseriid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_campaignid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_campaignid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_campaignid");
            }
            set
            {
                this.OnPropertyChanging("ktb_campaignid");
                this.SetAttributeValue("ktb_campaignid", value);
                this.OnPropertyChanged("ktb_campaignid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_bodytype1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_bodytype1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_bodytype1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_bodytype1");
                this.SetAttributeValue("ktb_cbu_bodytype1", value);
                this.OnPropertyChanged("ktb_cbu_bodytype1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_bodytypelcv1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_bodytypelcv1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_bodytypelcv1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_bodytypelcv1");
                this.SetAttributeValue("ktb_cbu_bodytypelcv1", value);
                this.OnPropertyChanged("ktb_cbu_bodytypelcv1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_loadprofile1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_loadprofile1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_loadprofile1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_loadprofile1");
                this.SetAttributeValue("ktb_cbu_loadprofile1", value);
                this.OnPropertyChanged("ktb_cbu_loadprofile1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_medanoperasi1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_medanoperasi1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_medanoperasi1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_medanoperasi1");
                this.SetAttributeValue("ktb_cbu_medanoperasi1", value);
                this.OnPropertyChanged("ktb_cbu_medanoperasi1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_ownership1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_ownership1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_ownership1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_ownership1");
                this.SetAttributeValue("ktb_cbu_ownership1", value);
                this.OnPropertyChanged("ktb_cbu_ownership1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_purcstat")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_purcstat
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_purcstat");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_purcstat");
                this.SetAttributeValue("ktb_cbu_purcstat", value);
                this.OnPropertyChanged("ktb_cbu_purcstat");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_purpose1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_purpose1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_purpose1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_purpose1");
                this.SetAttributeValue("ktb_cbu_purpose1", value);
                this.OnPropertyChanged("ktb_cbu_purpose1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_userage1")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_userage1
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_userage1");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_userage1");
                this.SetAttributeValue("ktb_cbu_userage1", value);
                this.OnPropertyChanged("ktb_cbu_userage1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_cbu_purcstat2")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_cbu_purcstat2
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_cbu_purcstat2");
            }
            set
            {
                this.OnPropertyChanging("ktb_cbu_purcstat2");
                this.SetAttributeValue("ktb_cbu_purcstat2", value);
                this.OnPropertyChanged("ktb_cbu_purcstat2");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_additional")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_additional
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_additional");
            }
            set
            {
                this.OnPropertyChanging("ktb_additional");
                this.SetAttributeValue("ktb_additional", value);
                this.OnPropertyChanged("ktb_additional");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetid")]
        public System.Nullable<int> ktb_dnetid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetid");
                this.SetAttributeValue("ktb_dnetid", value);
                this.OnPropertyChanged("ktb_dnetid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_statusinterfacednet")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_statusinterfacednet
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_statusinterfacednet");
            }
            set
            {
                this.OnPropertyChanging("ktb_statusinterfacednet");
                this.SetAttributeValue("ktb_statusinterfacednet", value);
                this.OnPropertyChanged("ktb_statusinterfacednet");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_isinterfaced")]
        public System.Nullable<bool> ktb_isinterfaced
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_isinterfaced");
            }
            set
            {
                this.OnPropertyChanging("ktb_isinterfaced");
                this.SetAttributeValue("ktb_isinterfaced", value);
                this.OnPropertyChanged("ktb_isinterfaced");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_rejectedreasonoption")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_rejectedreasonoption
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_rejectedreasonoption");
            }
            set
            {
                this.OnPropertyChanging("ktb_rejectedreasonoption");
                this.SetAttributeValue("ktb_rejectedreasonoption", value);
                this.OnPropertyChanged("ktb_rejectedreasonoption");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_spkdnetreference")]
        public string ktb_spkdnetreference
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_spkdnetreference");
            }
            set
            {
                this.OnPropertyChanging("ktb_spkdnetreference");
                this.SetAttributeValue("ktb_spkdnetreference", value);
                this.OnPropertyChanged("ktb_spkdnetreference");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_leasingcompanyidname")]
        public string ktb_leasingcompanyidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_leasingcompanyidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_leasingcompanyidName");
                this.SetAttributeValue("ktb_leasingcompanyidname", value);
                this.OnPropertyChanged("ktb_leasingcompanyidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_benefitid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_benefitid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_benefitid");
            }
            set
            {
                this.OnPropertyChanging("ktb_benefitid");
                this.SetAttributeValue("ktb_benefitid", value);
                this.OnPropertyChanged("ktb_benefitid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_namakonsumenspk")]
        public string ktb_namakonsumenspk
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("ktb_namakonsumenspk");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("ktb_namakonsumenspk");
                this.SetAttributeValue("ktb_namakonsumenspk", value);
                this.OnPropertyChanged("ktb_namakonsumenspk");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_namakonsumenspkid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_namakonsumenspkid
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_namakonsumenspkid");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("ktb_namakonsumenspkid");
                this.SetAttributeValue("ktb_namakonsumenspkid", value);
                this.OnPropertyChanged("ktb_namakonsumenspkid");
            }
        }
    }
    #endregion

    #region SPK Detail
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_newvehiclesalesorder")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_newvehiclesalesorder : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_newvehiclesalesorder
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_contractnumber")]
        public string ktb_contractnumber
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_contractnumber");
            }
            set
            {
                this.OnPropertyChanging("ktb_contractnumber");
                this.SetAttributeValue("ktb_contractnumber", value);
                this.OnPropertyChanged("ktb_contractnumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_contractdate")]
        public System.Nullable<System.DateTime> ktb_contractdate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_contractdate");
            }
            set
            {
                this.OnPropertyChanging("ktb_contractdate");
                this.SetAttributeValue("ktb_contractdate", value);
                this.OnPropertyChanged("ktb_contractdate");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_opportunitynoid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_OpportunityNoId
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_opportunitynoid");
            }
            set
            {
                this.OnPropertyChanging("ktb_OpportunityNoId");
                this.SetAttributeValue("ktb_opportunitynoid", value);
                this.OnPropertyChanged("ktb_OpportunityNoId");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_programidname")]
        public string ktb_ProgramIdName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_programidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_ProgramIdName");
                this.SetAttributeValue("ktb_programidname", value);
                this.OnPropertyChanged("ktb_ProgramIdName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_karosericompanyidname")]
        public string ktb_karosericompanyidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_karosericompanyidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_karosericompanyidName");
                this.SetAttributeValue("ktb_karosericompanyidname", value);
                this.OnPropertyChanged("ktb_karosericompanyidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_opportunitynoidname")]
        public string ktb_OpportunityNoIdName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_opportunitynoidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_OpportunityNoIdName");
                this.SetAttributeValue("ktb_opportunitynoidname", value);
                this.OnPropertyChanged("ktb_OpportunityNoIdName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_campaignid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_CampaignId
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_campaignid");
            }
            set
            {
                this.OnPropertyChanging("ktb_CampaignId");
                this.SetAttributeValue("ktb_campaignid", value);
                this.OnPropertyChanged("ktb_CampaignId");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_ageyeardiscount_base")]
        public Microsoft.Xrm.Sdk.Money ktb_ageyeardiscount_Base
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("ktb_ageyeardiscount_base");
            }
            set
            {
                this.OnPropertyChanging("ktb_ageyeardiscount_Base");
                this.SetAttributeValue("ktb_ageyeardiscount_base", value);
                this.OnPropertyChanged("ktb_ageyeardiscount_Base");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_benefitidname")]
        public string ktb_BenefitIdName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_benefitidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_BenefitIdName");
                this.SetAttributeValue("ktb_benefitidname", value);
                this.OnPropertyChanged("ktb_BenefitIdName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclecategoryidname")]
        public string ktb_vehiclecategoryidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehiclecategoryidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclecategoryidName");
                this.SetAttributeValue("ktb_vehiclecategoryidname", value);
                this.OnPropertyChanged("ktb_vehiclecategoryidName");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_benefitid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_BenefitId
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_benefitid");
            }
            set
            {
                this.OnPropertyChanging("ktb_BenefitId");
                this.SetAttributeValue("ktb_benefitid", value);
                this.OnPropertyChanged("ktb_BenefitId");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_ageyeardiscount")]
        public Microsoft.Xrm.Sdk.Money ktb_ageyeardiscount
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("ktb_ageyeardiscount");
            }
            set
            {
                this.OnPropertyChanging("ktb_ageyeardiscount");
                this.SetAttributeValue("ktb_ageyeardiscount", value);
                this.OnPropertyChanged("ktb_ageyeardiscount");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_isinterfaced")]
        public System.Nullable<bool> ktb_isinterfaced
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_isinterfaced");
            }
            set
            {
                this.OnPropertyChanging("ktb_isinterfaced");
                this.SetAttributeValue("ktb_isinterfaced", value);
                this.OnPropertyChanged("ktb_isinterfaced");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_nobookingfeedp")]
        public System.Nullable<bool> ktb_NoBookingFeeDP
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_nobookingfeedp");
            }
            set
            {
                this.OnPropertyChanging("ktb_NoBookingFeeDP");
                this.SetAttributeValue("ktb_nobookingfeedp", value);
                this.OnPropertyChanged("ktb_NoBookingFeeDP");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_typecaroseries")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_typecaroseries
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_typecaroseries");
            }
            set
            {
                this.OnPropertyChanging("ktb_typecaroseries");
                this.SetAttributeValue("ktb_typecaroseries", value);
                this.OnPropertyChanged("ktb_typecaroseries");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_rejectedreason")]
        public string ktb_rejectedreason
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_rejectedreason");
            }
            set
            {
                this.OnPropertyChanging("ktb_rejectedreason");
                this.SetAttributeValue("ktb_rejectedreason", value);
                this.OnPropertyChanged("ktb_rejectedreason");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_remarks")]
        public string ktb_remarks
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_remarks");
            }
            set
            {
                this.OnPropertyChanging("ktb_remarks");
                this.SetAttributeValue("ktb_remarks", value);
                this.OnPropertyChanged("ktb_remarks");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclemodelid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_vehiclemodelid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_vehiclemodelid");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclemodelid");
                this.SetAttributeValue("ktb_vehiclemodelid", value);
                this.OnPropertyChanged("ktb_vehiclemodelid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_campaignidname")]
        public string ktb_CampaignIdName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_campaignidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_CampaignIdName");
                this.SetAttributeValue("ktb_campaignidname", value);
                this.OnPropertyChanged("ktb_CampaignIdName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclemodelidname")]
        public string ktb_vehiclemodelidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehiclemodelidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclemodelidName");
                this.SetAttributeValue("ktb_vehiclemodelidname", value);
                this.OnPropertyChanged("ktb_vehiclemodelidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_estimasiharga_base")]
        public Microsoft.Xrm.Sdk.Money ktb_estimasiharga_Base
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("ktb_estimasiharga_base");
            }
            set
            {
                this.OnPropertyChanging("ktb_estimasiharga_Base");
                this.SetAttributeValue("ktb_estimasiharga_base", value);
                this.OnPropertyChanged("ktb_estimasiharga_Base");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclecategoryid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_vehiclecategoryid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_vehiclecategoryid");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclecategoryid");
                this.SetAttributeValue("ktb_vehiclecategoryid", value);
                this.OnPropertyChanged("ktb_vehiclecategoryid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_newbenefitidname")]
        public string ktb_newbenefitidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_newbenefitidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_newbenefitidName");
                this.SetAttributeValue("ktb_newbenefitidname", value);
                this.OnPropertyChanged("ktb_newbenefitidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_iddnet")]
        public System.Nullable<int> ktb_IDDNet
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_iddnet");
            }
            set
            {
                this.OnPropertyChanging("ktb_IDDNet");
                this.SetAttributeValue("ktb_iddnet", value);
                this.OnPropertyChanged("ktb_IDDNet");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_lkppnumber")]
        public string ktb_LKPPNumber
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_lkppnumber");
            }
            set
            {
                this.OnPropertyChanging("ktb_LKPPNumber");
                this.SetAttributeValue("ktb_lkppnumber", value);
                this.OnPropertyChanged("ktb_LKPPNumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_ftaccessories")]
        public string ktb_ftaccessories
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_ftaccessories");
            }
            set
            {
                this.OnPropertyChanging("ktb_ftaccessories");
                this.SetAttributeValue("ktb_ftaccessories", value);
                this.OnPropertyChanged("ktb_ftaccessories");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_pembayaran")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_pembayaran
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_pembayaran");
            }
            set
            {
                this.OnPropertyChanging("ktb_pembayaran");
                this.SetAttributeValue("ktb_pembayaran", value);
                this.OnPropertyChanged("ktb_pembayaran");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetspknumber")]
        public string ktb_dnetspknumber
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_dnetspknumber");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetspknumber");
                this.SetAttributeValue("ktb_dnetspknumber", value);
                this.OnPropertyChanged("ktb_dnetspknumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_additional")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_additional
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_additional");
            }
            set
            {
                this.OnPropertyChanging("ktb_additional");
                this.SetAttributeValue("ktb_additional", value);
                this.OnPropertyChanged("ktb_additional");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dppengurangtax")]
        public Microsoft.Xrm.Sdk.Money ktb_dppengurangtax
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("ktb_dppengurangtax");
            }
            set
            {
                this.OnPropertyChanging("ktb_dppengurangtax");
                this.SetAttributeValue("ktb_dppengurangtax", value);
                this.OnPropertyChanged("ktb_dppengurangtax");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dppengurangtax_base")]
        public Microsoft.Xrm.Sdk.Money ktb_dppengurangtax_Base
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("ktb_dppengurangtax_base");
            }
            set
            {
                this.OnPropertyChanging("ktb_dppengurangtax_Base");
                this.SetAttributeValue("ktb_dppengurangtax_base", value);
                this.OnPropertyChanged("ktb_dppengurangtax_Base");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetid")]
        public System.Nullable<int> ktb_dnetid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetid");
                this.SetAttributeValue("ktb_dnetid", value);
                this.OnPropertyChanged("ktb_dnetid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetspkcustomerid")]
        public System.Nullable<int> ktb_dnetspkcustomerid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetspkcustomerid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetspkcustomerid");
                this.SetAttributeValue("ktb_dnetspkcustomerid", value);
                this.OnPropertyChanged("ktb_dnetspkcustomerid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_konfirmasics")]
        public System.Nullable<bool> ktb_konfirmasics
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_konfirmasics");
            }
            set
            {
                this.OnPropertyChanging("ktb_konfirmasics");
                this.SetAttributeValue("ktb_konfirmasics", value);
                this.OnPropertyChanged("ktb_konfirmasics");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_tanggalkonfirmasi")]
        public System.Nullable<System.DateTime> ktb_tanggalkonfirmasi
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_tanggalkonfirmasi");
            }
            set
            {
                this.OnPropertyChanging("ktb_tanggalkonfirmasi");
                this.SetAttributeValue("ktb_tanggalkonfirmasi", value);
                this.OnPropertyChanged("ktb_tanggalkonfirmasi");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_estimasiharga")]
        public Microsoft.Xrm.Sdk.Money ktb_estimasiharga
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("ktb_estimasiharga");
            }
            set
            {
                this.OnPropertyChanging("ktb_estimasiharga");
                this.SetAttributeValue("ktb_estimasiharga", value);
                this.OnPropertyChanged("ktb_estimasiharga");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehicledescription")]
        public string ktb_vehicledescription
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehicledescription");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehicledescription");
                this.SetAttributeValue("ktb_vehicledescription", value);
                this.OnPropertyChanged("ktb_vehicledescription");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclecolorname")]
        public string ktb_vehiclecolorname
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehiclecolorname");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclecolorname");
                this.SetAttributeValue("ktb_vehiclecolorname", value);
                this.OnPropertyChanged("ktb_vehiclecolorname");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_address4")]
        public string ktb_address4
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_address4");
            }
            set
            {
                this.OnPropertyChanging("ktb_address4");
                this.SetAttributeValue("ktb_address4", value);
                this.OnPropertyChanged("ktb_address4");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_newbenefitid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_newbenefitid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_newbenefitid");
            }
            set
            {
                this.OnPropertyChanging("ktb_newbenefitid");
                this.SetAttributeValue("ktb_newbenefitid", value);
                this.OnPropertyChanged("ktb_newbenefitid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_programid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_ProgramId
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_programid");
            }
            set
            {
                this.OnPropertyChanging("ktb_ProgramId");
                this.SetAttributeValue("ktb_programid", value);
                this.OnPropertyChanged("ktb_ProgramId");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_karosericompanyid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_karosericompanyid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_karosericompanyid");
            }
            set
            {
                this.OnPropertyChanging("ktb_karosericompanyid");
                this.SetAttributeValue("ktb_karosericompanyid", value);
                this.OnPropertyChanged("ktb_karosericompanyid");
            }
        }
    }
    #endregion

    #region AR Invoice
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_accountreceivableinvoice")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_accountreceivableinvoice : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_accountreceivableinvoice
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_origarinvoicenumber")]
        public string ktb_origarinvoicenumber
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_origarinvoicenumber");
            }
            set
            {
                this.OnPropertyChanging("ktb_origarinvoicenumber");
                this.SetAttributeValue("ktb_origarinvoicenumber", value);
                this.OnPropertyChanged("ktb_origarinvoicenumber");
            }
        }

        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_sourcetypenumbering")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_sourcetypenumbering
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_sourcetypenumbering");
            }
            set
            {
                this.OnPropertyChanging("ktb_sourcetypenumbering");
                this.SetAttributeValue("ktb_sourcetypenumbering", value);
                this.OnPropertyChanged("ktb_sourcetypenumbering");
            }
        }

        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_customerdescription")]
        public string ktb_customerdescription
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_customerdescription");
            }
            set
            {
                this.OnPropertyChanging("ktb_customerdescription");
                this.SetAttributeValue("ktb_customerdescription", value);
                this.OnPropertyChanged("ktb_customerdescription");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_say")]
        public string ktb_say
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_say");
            }
            set
            {
                this.OnPropertyChanging("ktb_say");
                this.SetAttributeValue("ktb_say", value);
                this.OnPropertyChanged("ktb_say");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_sayfinancingamount")]
        public string ktb_sayfinancingamount
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("ktb_sayfinancingamount");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("ktb_sayfinancingamount");
                this.SetAttributeValue("ktb_sayfinancingamount", value);
                this.OnPropertyChanged("ktb_sayfinancingamount");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_saytotalamount")]
        public string ktb_saytotalamount
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<string>("ktb_saytotalamount");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("ktb_saytotalamount");
                this.SetAttributeValue("ktb_saytotalamount", value);
                this.OnPropertyChanged("ktb_saytotalamount");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_ordertypeidname")]
        public string ktb_ordertypeidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_ordertypeidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_ordertypeidName");
                this.SetAttributeValue("ktb_ordertypeidname", value);
                this.OnPropertyChanged("ktb_ordertypeidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_interfaceprocess")]
        public System.Nullable<bool> ktb_interfaceprocess
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_interfaceprocess");
            }
            set
            {
                this.OnPropertyChanging("ktb_interfaceprocess");
                this.SetAttributeValue("ktb_interfaceprocess", value);
                this.OnPropertyChanged("ktb_interfaceprocess");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_ordertypeid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_ordertypeid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_ordertypeid");
            }
            set
            {
                this.OnPropertyChanging("ktb_ordertypeid");
                this.SetAttributeValue("ktb_ordertypeid", value);
                this.OnPropertyChanged("ktb_ordertypeid");
            }
        }

        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_parentbusinessunitid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_parentbusinessunitid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_parentbusinessunitid");
            }
            set
            {
                this.OnPropertyChanging("ktb_parentbusinessunitid");
                this.SetAttributeValue("ktb_parentbusinessunitid", value);
                this.OnPropertyChanged("ktb_parentbusinessunitid");
            }
        }

        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_financingcompanyinvoiceamount")]
        public Microsoft.Xrm.Sdk.Money xts_financingcompanyinvoiceamount
        {
            [System.Diagnostics.DebuggerNonUserCode()]
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.Money>("xts_financingcompanyinvoiceamount");
            }
            [System.Diagnostics.DebuggerNonUserCode()]
            set
            {
                this.OnPropertyChanging("xts_financingcompanyinvoiceamount");
                this.SetAttributeValue("xts_financingcompanyinvoiceamount", value);
                this.OnPropertyChanged("xts_financingcompanyinvoiceamount");
            }
        }
    }
    #endregion

    #region AR Receipt
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_accountreceivablereceipt")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_accountreceivablereceipt : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_accountreceivablereceipt
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_createfakturpajak")]
        public System.Nullable<bool> ktb_createfakturpajak
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_createfakturpajak");
            }
            set
            {
                this.OnPropertyChanging("ktb_createfakturpajak");
                this.SetAttributeValue("ktb_createfakturpajak", value);
                this.OnPropertyChanged("ktb_createfakturpajak");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_prospectid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_prospectid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_prospectid");
            }
            set
            {
                this.OnPropertyChanging("ktb_prospectid");
                this.SetAttributeValue("ktb_prospectid", value);
                this.OnPropertyChanged("ktb_prospectid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_customerdescription")]
        public string ktb_customerdescription
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_customerdescription");
            }
            set
            {
                this.OnPropertyChanging("ktb_customerdescription");
                this.SetAttributeValue("ktb_customerdescription", value);
                this.OnPropertyChanged("ktb_customerdescription");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_say")]
        public string ktb_say
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_say");
            }
            set
            {
                this.OnPropertyChanging("ktb_say");
                this.SetAttributeValue("ktb_say", value);
                this.OnPropertyChanged("ktb_say");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_untukpembayaran")]
        public string ktb_untukpembayaran
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_untukpembayaran");
            }
            set
            {
                this.OnPropertyChanging("ktb_untukpembayaran");
                this.SetAttributeValue("ktb_untukpembayaran", value);
                this.OnPropertyChanged("ktb_untukpembayaran");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_description1")]
        public string ktb_description1
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_description1");
            }
            set
            {
                this.OnPropertyChanging("ktb_description1");
                this.SetAttributeValue("ktb_description1", value);
                this.OnPropertyChanged("ktb_description1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_description2")]
        public string ktb_description2
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_description2");
            }
            set
            {
                this.OnPropertyChanging("ktb_description2");
                this.SetAttributeValue("ktb_description2", value);
                this.OnPropertyChanged("ktb_description2");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_description3")]
        public string ktb_description3
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_description3");
            }
            set
            {
                this.OnPropertyChanging("ktb_description3");
                this.SetAttributeValue("ktb_description3", value);
                this.OnPropertyChanged("ktb_description3");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_description4")]
        public string ktb_description4
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_description4");
            }
            set
            {
                this.OnPropertyChanging("ktb_description4");
                this.SetAttributeValue("ktb_description4", value);
                this.OnPropertyChanged("ktb_description4");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_girono")]
        public string ktb_girono
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_girono");
            }
            set
            {
                this.OnPropertyChanging("ktb_girono");
                this.SetAttributeValue("ktb_girono", value);
                this.OnPropertyChanged("ktb_girono");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_bankgiro")]
        public string ktb_bankgiro
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_bankgiro");
            }
            set
            {
                this.OnPropertyChanging("ktb_bankgiro");
                this.SetAttributeValue("ktb_bankgiro", value);
                this.OnPropertyChanged("ktb_bankgiro");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_giroduedate")]
        public System.Nullable<System.DateTime> ktb_giroduedate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_giroduedate");
            }
            set
            {
                this.OnPropertyChanging("ktb_giroduedate");
                this.SetAttributeValue("ktb_giroduedate", value);
                this.OnPropertyChanged("ktb_giroduedate");
            }
        }
    }
    #endregion

    #region AR Receipt Detail
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_accountreceivablereceiptdetail")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_accountreceivablereceiptdetail : Technosoft.DMSX01.XRM.EntityClasses.Entities.xts_accountreceivablereceiptdetail
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_workorderidname")]
        public string ktb_workorderidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_workorderidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_workorderidName");
                this.SetAttributeValue("ktb_workorderidname", value);
                this.OnPropertyChanged("ktb_workorderidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_workorderid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_workorderid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_workorderid");
            }
            set
            {
                this.OnPropertyChanging("ktb_workorderid");
                this.SetAttributeValue("ktb_workorderid", value);
                this.OnPropertyChanged("ktb_workorderid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_say")]
        public string ktb_say
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_say");
            }
            set
            {
                this.OnPropertyChanging("ktb_say");
                this.SetAttributeValue("ktb_say", value);
                this.OnPropertyChanged("ktb_say");
            }
        }
    }
    #endregion

    #region Detail Blanko - Xts_NVSONumberRegistrationDetails
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("xts_nvsonumberregistrationdetails")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class xts_nvsonumberregistrationdetails : Microsoft.Xrm.Sdk.Entity, System.ComponentModel.INotifyPropertyChanging, System.ComponentModel.INotifyPropertyChanged
    {
        public xts_nvsonumberregistrationdetails() : base(EntityLogicalName) { }
        public const string EntityLogicalName = "xts_nvsonumberregistrationdetails";
        public const int EntityTypeCode = 10240;
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;
        protected void OnPropertyChanged(string propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
        protected void OnPropertyChanging(string propertyName)
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdonbehalfbyyominame")]
        public string CreatedOnBehalfByYomiName
        {
            get
            {
                return this.GetAttributeValue<string>("createdonbehalfbyyominame");
            }
            set
            {
                this.OnPropertyChanging("CreatedOnBehalfByYomiName");
                this.SetAttributeValue("createdonbehalfbyyominame", value);
                this.OnPropertyChanged("CreatedOnBehalfByYomiName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owninguser")]
        public Microsoft.Xrm.Sdk.EntityReference OwningUser
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owninguser");
            }
            set
            {
                this.OnPropertyChanging("OwningUser");
                this.SetAttributeValue("owninguser", value);
                this.OnPropertyChanged("OwningUser");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningteam")]
        public Microsoft.Xrm.Sdk.EntityReference OwningTeam
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningteam");
            }
            set
            {
                this.OnPropertyChanging("OwningTeam");
                this.SetAttributeValue("owningteam", value);
                this.OnPropertyChanged("OwningTeam");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_locking")]
        public string xts_locking
        {
            get
            {
                return this.GetAttributeValue<string>("xts_locking");
            }
            set
            {
                this.OnPropertyChanging("xts_locking");
                this.SetAttributeValue("xts_locking", value);
                this.OnPropertyChanged("xts_locking");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statecode")]
        public System.Nullable<Technosoft.DMS.XRM.EntityClasses.Entities.xts_nvsonumberregistrationdetailsState> statecode
        {
            get
            {
                Microsoft.Xrm.Sdk.OptionSetValue optionSet = this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statecode");
                if ((optionSet != null))
                {
                    return ((Technosoft.DMS.XRM.EntityClasses.Entities.xts_nvsonumberregistrationdetailsState)(System.Enum.ToObject(typeof(Technosoft.DMS.XRM.EntityClasses.Entities.xts_nvsonumberregistrationdetailsState), optionSet.Value)));
                }
                else
                {
                    return null;
                }
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_businessunitidname")]
        public string xts_businessunitidName
        {
            get
            {
                return this.GetAttributeValue<string>("xts_businessunitidname");
            }
            set
            {
                this.OnPropertyChanging("xts_businessunitidName");
                this.SetAttributeValue("xts_businessunitidname", value);
                this.OnPropertyChanged("xts_businessunitidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistrationname")]
        public string xts_nvsonumberregistrationName
        {
            get
            {
                return this.GetAttributeValue<string>("xts_nvsonumberregistrationname");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsonumberregistrationName");
                this.SetAttributeValue("xts_nvsonumberregistrationname", value);
                this.OnPropertyChanged("xts_nvsonumberregistrationName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owneridname")]
        public string OwnerIdName
        {
            get
            {
                return this.GetAttributeValue<string>("owneridname");
            }
            set
            {
                this.OnPropertyChanging("OwnerIdName");
                this.SetAttributeValue("owneridname", value);
                this.OnPropertyChanged("OwnerIdName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdonbehalfby")]
        public Microsoft.Xrm.Sdk.EntityReference CreatedOnBehalfBy
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdonbehalfby");
            }
            set
            {
                this.OnPropertyChanging("CreatedOnBehalfBy");
                this.SetAttributeValue("createdonbehalfby", value);
                this.OnPropertyChanged("CreatedOnBehalfBy");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdby")]
        public Microsoft.Xrm.Sdk.EntityReference CreatedBy
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("createdby");
            }
            set
            {
                this.OnPropertyChanging("CreatedBy");
                this.SetAttributeValue("createdby", value);
                this.OnPropertyChanged("CreatedBy");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_pkcombinationkey")]
        public string xts_pkcombinationkey
        {
            get
            {
                return this.GetAttributeValue<string>("xts_pkcombinationkey");
            }
            set
            {
                this.OnPropertyChanging("xts_pkcombinationkey");
                this.SetAttributeValue("xts_pkcombinationkey", value);
                this.OnPropertyChanged("xts_pkcombinationkey");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistrationdetails")]
        public string xts_nvsonumberregistrationdetails1
        {
            get
            {
                return this.GetAttributeValue<string>("xts_nvsonumberregistrationdetails");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsonumberregistrationdetails1");
                this.SetAttributeValue("xts_nvsonumberregistrationdetails", value);
                this.OnPropertyChanged("xts_nvsonumberregistrationdetails1");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistrationdetailsid")]
        public System.Nullable<System.Guid> xts_nvsonumberregistrationdetailsId
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.Guid>>("xts_nvsonumberregistrationdetailsid");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsonumberregistrationdetailsId");
                this.SetAttributeValue("xts_nvsonumberregistrationdetailsid", value);
                if (value.HasValue)
                {
                    base.Id = value.Value;
                }
                else
                {
                    base.Id = System.Guid.Empty;
                }
                this.OnPropertyChanged("xts_nvsonumberregistrationdetailsId");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistrationdetailsid")]
        public override System.Guid Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                this.xts_nvsonumberregistrationdetailsId = value;
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("importsequencenumber")]
        public System.Nullable<int> ImportSequenceNumber
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("importsequencenumber");
            }
            set
            {
                this.OnPropertyChanging("ImportSequenceNumber");
                this.SetAttributeValue("importsequencenumber", value);
                this.OnPropertyChanged("ImportSequenceNumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owneridyominame")]
        public string OwnerIdYomiName
        {
            get
            {
                return this.GetAttributeValue<string>("owneridyominame");
            }
            set
            {
                this.OnPropertyChanging("OwnerIdYomiName");
                this.SetAttributeValue("owneridyominame", value);
                this.OnPropertyChanged("OwnerIdYomiName");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistration")]
        public Microsoft.Xrm.Sdk.EntityReference xts_nvsonumberregistration
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("xts_nvsonumberregistration");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsonumberregistration");
                this.SetAttributeValue("xts_nvsonumberregistration", value);
                this.OnPropertyChanged("xts_nvsonumberregistration");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("utcconversiontimezonecode")]
        public System.Nullable<int> UTCConversionTimeZoneCode
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("utcconversiontimezonecode");
            }
            set
            {
                this.OnPropertyChanging("UTCConversionTimeZoneCode");
                this.SetAttributeValue("utcconversiontimezonecode", value);
                this.OnPropertyChanged("UTCConversionTimeZoneCode");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistrationid")]
        public Microsoft.Xrm.Sdk.EntityReference xts_nvsonumberregistrationid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("xts_nvsonumberregistrationid");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsonumberregistrationid");
                this.SetAttributeValue("xts_nvsonumberregistrationid", value);
                this.OnPropertyChanged("xts_nvsonumberregistrationid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdbyyominame")]
        public string CreatedByYomiName
        {
            get
            {
                return this.GetAttributeValue<string>("createdbyyominame");
            }
            set
            {
                this.OnPropertyChanging("CreatedByYomiName");
                this.SetAttributeValue("createdbyyominame", value);
                this.OnPropertyChanged("CreatedByYomiName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedbyname")]
        public string ModifiedByName
        {
            get
            {
                return this.GetAttributeValue<string>("modifiedbyname");
            }
            set
            {
                this.OnPropertyChanging("ModifiedByName");
                this.SetAttributeValue("modifiedbyname", value);
                this.OnPropertyChanged("ModifiedByName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("versionnumber")]
        public System.Nullable<int> VersionNumber
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("versionnumber");
            }
            set
            {
                this.OnPropertyChanging("VersionNumber");
                this.SetAttributeValue("versionnumber", value);
                this.OnPropertyChanged("VersionNumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedby")]
        public Microsoft.Xrm.Sdk.EntityReference ModifiedBy
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedby");
            }
            set
            {
                this.OnPropertyChanging("ModifiedBy");
                this.SetAttributeValue("modifiedby", value);
                this.OnPropertyChanged("ModifiedBy");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedbyyominame")]
        public string ModifiedByYomiName
        {
            get
            {
                return this.GetAttributeValue<string>("modifiedbyyominame");
            }
            set
            {
                this.OnPropertyChanging("ModifiedByYomiName");
                this.SetAttributeValue("modifiedbyyominame", value);
                this.OnPropertyChanged("ModifiedByYomiName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_status")]
        public Microsoft.Xrm.Sdk.OptionSetValue xts_status
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("xts_status");
            }
            set
            {
                this.OnPropertyChanging("xts_status");
                this.SetAttributeValue("xts_status", value);
                this.OnPropertyChanged("xts_status");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("timezoneruleversionnumber")]
        public System.Nullable<int> TimeZoneRuleVersionNumber
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("timezoneruleversionnumber");
            }
            set
            {
                this.OnPropertyChanging("TimeZoneRuleVersionNumber");
                this.SetAttributeValue("timezoneruleversionnumber", value);
                this.OnPropertyChanged("TimeZoneRuleVersionNumber");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_assigntosalesdate")]
        public System.Nullable<System.DateTime> xts_assigntosalesdate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("xts_assigntosalesdate");
            }
            set
            {
                this.OnPropertyChanging("xts_assigntosalesdate");
                this.SetAttributeValue("xts_assigntosalesdate", value);
                this.OnPropertyChanged("xts_assigntosalesdate");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_salespersonid")]
        public Microsoft.Xrm.Sdk.EntityReference xts_salespersonid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("xts_salespersonid");
            }
            set
            {
                this.OnPropertyChanging("xts_salespersonid");
                this.SetAttributeValue("xts_salespersonid", value);
                this.OnPropertyChanged("xts_salespersonid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_lastnumber")]
        public System.Nullable<int> xts_lastnumber
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("xts_lastnumber");
            }
            set
            {
                this.OnPropertyChanging("xts_lastnumber");
                this.SetAttributeValue("xts_lastnumber", value);
                this.OnPropertyChanged("xts_lastnumber");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedon")]
        public System.Nullable<System.DateTime> ModifiedOn
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("modifiedon");
            }
            set
            {
                this.OnPropertyChanging("ModifiedOn");
                this.SetAttributeValue("modifiedon", value);
                this.OnPropertyChanged("ModifiedOn");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsoserial")]
        public System.Nullable<int> xts_nvsoserial
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("xts_nvsoserial");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsoserial");
                this.SetAttributeValue("xts_nvsoserial", value);
                this.OnPropertyChanged("xts_nvsoserial");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedonbehalfbyyominame")]
        public string ModifiedOnBehalfByYomiName
        {
            get
            {
                return this.GetAttributeValue<string>("modifiedonbehalfbyyominame");
            }
            set
            {
                this.OnPropertyChanging("ModifiedOnBehalfByYomiName");
                this.SetAttributeValue("modifiedonbehalfbyyominame", value);
                this.OnPropertyChanged("ModifiedOnBehalfByYomiName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("statuscode")]
        public Microsoft.Xrm.Sdk.OptionSetValue statuscode
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("statuscode");
            }
            set
            {
                this.OnPropertyChanging("statuscode");
                this.SetAttributeValue("statuscode", value);
                this.OnPropertyChanged("statuscode");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdbyname")]
        public string CreatedByName
        {
            get
            {
                return this.GetAttributeValue<string>("createdbyname");
            }
            set
            {
                this.OnPropertyChanging("CreatedByName");
                this.SetAttributeValue("createdbyname", value);
                this.OnPropertyChanged("CreatedByName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdon")]
        public System.Nullable<System.DateTime> CreatedOn
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("createdon");
            }
            set
            {
                this.OnPropertyChanging("CreatedOn");
                this.SetAttributeValue("createdon", value);
                this.OnPropertyChanged("CreatedOn");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_businessunitid")]
        public Microsoft.Xrm.Sdk.EntityReference xts_businessunitid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("xts_businessunitid");
            }
            set
            {
                this.OnPropertyChanging("xts_businessunitid");
                this.SetAttributeValue("xts_businessunitid", value);
                this.OnPropertyChanged("xts_businessunitid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("createdonbehalfbyname")]
        public string CreatedOnBehalfByName
        {
            get
            {
                return this.GetAttributeValue<string>("createdonbehalfbyname");
            }
            set
            {
                this.OnPropertyChanging("CreatedOnBehalfByName");
                this.SetAttributeValue("createdonbehalfbyname", value);
                this.OnPropertyChanged("CreatedOnBehalfByName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("owningbusinessunit")]
        public Microsoft.Xrm.Sdk.EntityReference OwningBusinessUnit
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("owningbusinessunit");
            }
            set
            {
                this.OnPropertyChanging("OwningBusinessUnit");
                this.SetAttributeValue("owningbusinessunit", value);
                this.OnPropertyChanged("OwningBusinessUnit");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_assigndate")]
        public System.Nullable<System.DateTime> xts_assigndate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("xts_assigndate");
            }
            set
            {
                this.OnPropertyChanging("xts_assigndate");
                this.SetAttributeValue("xts_assigndate", value);
                this.OnPropertyChanged("xts_assigndate");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedonbehalfbyname")]
        public string ModifiedOnBehalfByName
        {
            get
            {
                return this.GetAttributeValue<string>("modifiedonbehalfbyname");
            }
            set
            {
                this.OnPropertyChanging("ModifiedOnBehalfByName");
                this.SetAttributeValue("modifiedonbehalfbyname", value);
                this.OnPropertyChanged("ModifiedOnBehalfByName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_nvsonumberregistrationidname")]
        public string xts_nvsonumberregistrationidName
        {
            get
            {
                return this.GetAttributeValue<string>("xts_nvsonumberregistrationidname");
            }
            set
            {
                this.OnPropertyChanging("xts_nvsonumberregistrationidName");
                this.SetAttributeValue("xts_nvsonumberregistrationidname", value);
                this.OnPropertyChanged("xts_nvsonumberregistrationidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("modifiedonbehalfby")]
        public Microsoft.Xrm.Sdk.EntityReference ModifiedOnBehalfBy
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("modifiedonbehalfby");
            }
            set
            {
                this.OnPropertyChanging("ModifiedOnBehalfBy");
                this.SetAttributeValue("modifiedonbehalfby", value);
                this.OnPropertyChanged("ModifiedOnBehalfBy");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ownerid")]
        public Microsoft.Xrm.Sdk.EntityReference OwnerId
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ownerid");
            }
            set
            {
                this.OnPropertyChanging("OwnerId");
                this.SetAttributeValue("ownerid", value);
                this.OnPropertyChanged("OwnerId");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("overriddencreatedon")]
        public System.Nullable<System.DateTime> OverriddenCreatedOn
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("overriddencreatedon");
            }
            set
            {
                this.OnPropertyChanging("OverriddenCreatedOn");
                this.SetAttributeValue("overriddencreatedon", value);
                this.OnPropertyChanged("OverriddenCreatedOn");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("xts_salespersonidname")]
        public string xts_salespersonidName
        {
            get
            {
                return this.GetAttributeValue<string>("xts_salespersonidname");
            }
            set
            {
                this.OnPropertyChanging("xts_salespersonidName");
                this.SetAttributeValue("xts_salespersonidname", value);
                this.OnPropertyChanged("xts_salespersonidName");
            }
        }
    }
    #endregion
}
