﻿namespace TS.Integration.Yana.Model.YanaEntities
{
    [System.Runtime.Serialization.DataContractAttribute()]
    [Microsoft.Xrm.Sdk.Client.EntityLogicalNameAttribute("lead")]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("CrmSvcUtil", "5.0.9690.3448")]
    public partial class Lead : Technosoft.DMSX01.XRM.EntityClasses.Entities.Lead
    {
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_statusreason")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_statusreason
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_statusreason");
            }
            set
            {
                this.OnPropertyChanging("ktb_statusreason");
                this.SetAttributeValue("ktb_statusreason", value);
                this.OnPropertyChanged("ktb_statusreason");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetid")]
        public System.Nullable<int> ktb_dnetid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetid");
                this.SetAttributeValue("ktb_dnetid", value);
                this.OnPropertyChanged("ktb_dnetid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclemodelid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_vehiclemodelid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_vehiclemodelid");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclemodelid");
                this.SetAttributeValue("ktb_vehiclemodelid", value);
                this.OnPropertyChanged("ktb_vehiclemodelid");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_customerpurpose")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_customerpurpose
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_customerpurpose");
            }
            set
            {
                this.OnPropertyChanging("ktb_customerpurpose");
                this.SetAttributeValue("ktb_customerpurpose", value);
                this.OnPropertyChanged("ktb_customerpurpose");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_interfaceexceptionmessage")]
        public string ktb_interfaceexceptionmessage
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_interfaceexceptionmessage");
            }
            set
            {
                this.OnPropertyChanging("ktb_interfaceexceptionmessage");
                this.SetAttributeValue("ktb_interfaceexceptionmessage", value);
                this.OnPropertyChanged("ktb_interfaceexceptionmessage");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_eventdata")]
        public string ktb_eventdata
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_eventdata");
            }
            set
            {
                this.OnPropertyChanging("ktb_eventdata");
                this.SetAttributeValue("ktb_eventdata", value);
                this.OnPropertyChanged("ktb_eventdata");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclecategoryid")]
        public Microsoft.Xrm.Sdk.EntityReference ktb_vehiclecategoryid
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.EntityReference>("ktb_vehiclecategoryid");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclecategoryid");
                this.SetAttributeValue("ktb_vehiclecategoryid", value);
                this.OnPropertyChanged("ktb_vehiclecategoryid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclecategoryidname")]
        public string ktb_vehiclecategoryidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehiclecategoryidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclecategoryidName");
                this.SetAttributeValue("ktb_vehiclecategoryidname", value);
                this.OnPropertyChanged("ktb_vehiclecategoryidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclemodelidname")]
        public string ktb_vehiclemodelidName
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehiclemodelidname");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclemodelidName");
                this.SetAttributeValue("ktb_vehiclemodelidname", value);
                this.OnPropertyChanged("ktb_vehiclemodelidName");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_statusinterfacednet")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_statusinterfacednet
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_statusinterfacednet");
            }
            set
            {
                this.OnPropertyChanging("ktb_statusinterfacednet");
                this.SetAttributeValue("ktb_statusinterfacednet", value);
                this.OnPropertyChanged("ktb_statusinterfacednet");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehicletypecode")]
        public string ktb_vehicletypecode
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehicletypecode");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehicletypecode");
                this.SetAttributeValue("ktb_vehicletypecode", value);
                this.OnPropertyChanged("ktb_vehicletypecode");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehicledescription")]
        public string ktb_vehicledescription
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehicledescription");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehicledescription");
                this.SetAttributeValue("ktb_vehicledescription", value);
                this.OnPropertyChanged("ktb_vehicledescription");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_isinterfaced")]
        public System.Nullable<bool> ktb_isinterfaced
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<bool>>("ktb_isinterfaced");
            }
            set
            {
                this.OnPropertyChanging("ktb_isinterfaced");
                this.SetAttributeValue("ktb_isinterfaced", value);
                this.OnPropertyChanged("ktb_isinterfaced");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_informationsource")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_informationsource
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_informationsource");
            }
            set
            {
                this.OnPropertyChanging("ktb_informationsource");
                this.SetAttributeValue("ktb_informationsource", value);
                this.OnPropertyChanged("ktb_informationsource");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_qty")]
        public System.Nullable<int> ktb_qty
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_qty");
            }
            set
            {
                this.OnPropertyChanging("ktb_qty");
                this.SetAttributeValue("ktb_qty", value);
                this.OnPropertyChanged("ktb_qty");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_vehiclecolorname")]
        public string ktb_vehiclecolorname
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_vehiclecolorname");
            }
            set
            {
                this.OnPropertyChanging("ktb_vehiclecolorname");
                this.SetAttributeValue("ktb_vehiclecolorname", value);
                this.OnPropertyChanged("ktb_vehiclecolorname");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_prospectdate")]
        public System.Nullable<System.DateTime> ktb_prospectdate
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<System.DateTime>>("ktb_prospectdate");
            }
            set
            {
                this.OnPropertyChanging("ktb_prospectdate");
                this.SetAttributeValue("ktb_prospectdate", value);
                this.OnPropertyChanged("ktb_prospectdate");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_webid")]
        public string ktb_webid
        {
            get
            {
                return this.GetAttributeValue<string>("ktb_webid");
            }
            set
            {
                this.OnPropertyChanging("ktb_webid");
                this.SetAttributeValue("ktb_webid", value);
                this.OnPropertyChanged("ktb_webid");
            }
        }
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_dnetspkcustomerid")]
        public System.Nullable<int> ktb_dnetspkcustomerid
        {
            get
            {
                return this.GetAttributeValue<System.Nullable<int>>("ktb_dnetspkcustomerid");
            }
            set
            {
                this.OnPropertyChanging("ktb_dnetspkcustomerid");
                this.SetAttributeValue("ktb_dnetspkcustomerid", value);
                this.OnPropertyChanged("ktb_dnetspkcustomerid");
            }
        }
        [System.Obsolete("Please do not use this property, because it is UNUSED attribute.")]
        [Microsoft.Xrm.Sdk.AttributeLogicalNameAttribute("ktb_informationtype")]
        public Microsoft.Xrm.Sdk.OptionSetValue ktb_informationtype
        {
            get
            {
                return this.GetAttributeValue<Microsoft.Xrm.Sdk.OptionSetValue>("ktb_informationtype");
            }
            set
            {
                this.OnPropertyChanging("ktb_informationtype");
                this.SetAttributeValue("ktb_informationtype", value);
                this.OnPropertyChanged("ktb_informationtype");
            }
        }
    }

}
