﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Modules.Integrate
{
    using Microsoft.Crm.Sdk.Messages;
    using Microsoft.Xrm.Sdk;
    using OP.MSCRM.Wrapper;
    using Technosoft.BSI.EntityClasses;
    using Technosoft.BSI.EntityClasses.Entities;
    using TS.Integration.Yana.Model.ExternalEntities;
    using TS.Integration.Yana.Utility.Custom;
    using Utility;

    public class SPK : BaseOperation<Model.ExternalEntities.SPK>
    {
        #region Var, Const, Enum, Init, internal Classes
        private Account Customer;
        private BusinessUnit BU;
        private Contact Contact;
        private Opportunity Prospect;
        private TransactionCurrency CurrencyReff;
        private xts_newvehiclegeneralsetup NVGS;
        private xts_nvsonumberregistrationdetails DetailBlanko;
        private xts_nvsomiscellaneouscharge NVSOMiscCharge;
        private xts_nvsoaccessories NVSOAccessories;
        private xts_employee Salesperson;

        #region SPKD enum
        private enum SPKD_ktb_additional
        {
            Aksesoris = 0,
            KomponenLainnya,
            TidakAda
        }

        private enum SPKD_ktb_birojasa
        {
            Internal = 1,
            Vendor
        }

        private enum SPKD_ktb_categorycode
        {
            PC = 1,
            LCV,
            CV
        }

        private enum SPKD_ktb_cbu_bodytype1
        {
            TK_TANGKI = 1,
            DT_DUMPTRUCK,
            BX_BOX,
            BS_BUS,
            BK_BAK,
            AMB_AMBULANCE,
            LL_LAIN_LAIN,
            MIX_MIXER,
            TH_TRACTORHEAD
        }
        #endregion
        #endregion

        public SPK(BaseExecute e, Model.ExternalEntities.SPK te) : base(e, te) { }

        private void Itinerary()
        {
            Operation = OperationTask.Configuration;
            "To Do List:".ConsoleWriteLine();
            "- Create Detail Blanko".ConsoleWriteLine();
            "- Won Prospect".ConsoleWriteLine();
            "- Fill Contact information".ConsoleWriteLine();
            "- Convert Contact".ConsoleWriteLine();
            "- Create SPK".ConsoleWriteLine();
            "- Create SPK Detail".ConsoleWriteLine();
            "- Create SPKD Accessories (if any)".ConsoleWriteLine();
            "- Create SPKD Misc Charges (if any)".ConsoleWriteLine();
        }

        public override OperationTask OperationIdentifier()
        {
            var opt = new Mapper<xts_newvehiclewholesaleorder>(Context.Service)
            .Fetch(DataContext.ExternalCode, e => ExternalCodeScheme, e => e.Id);

            if (!opt.Any()) return OperationTask.Create;
            Reference.Id = opt.First().Id;

            return OperationTask.Update;
        }

        public override void Execute()
        {
            try
            {
                "SPK".ConsoleWriteLine();
                if (Context.DebugMode) Itinerary();
                RequiredDataCheckAndDataPrep();

                PrintExecuteLog = false;
                CreateDetailBlanko();
                AttachBlankoToProspect();
                WonProspect();
                UpdateContactPreliminary();
                ConvertContactToCustomer();
                
                base.Execute();
                if (Reference != null)
                {
                    if (Reference.Id != null && Reference.Id != Guid.Empty)
                    {
                        AttachDokumenSPK();

                        //"Currently bypassing Detail Creation".ConsoleWriteLine();
                        CreateDetails();
                        //SetHeaderHandlingAsAwal();

                        new ExternalDealerLog(Context, new EDILog<Entity>
                        {
                            DealerIdentifier = DataContext.DealerCode,
                            Context = DataContext.Context,
                            ExternalCode = DataContext.ExternalCode,
                            ReceivedMessage = Context.Messages,
                            Operation = (int)Operation,
                            Lookup = new Entity { LogicalName = xts_newvehiclewholesaleorder.EntityLogicalName, Id = (Guid)Reference.Id }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                new ExternalDealerLog(Context, new EDILog<Entity>
                {
                    DealerIdentifier = DataContext.DealerCode,
                    Context = DataContext.Context,
                    ExternalCode = DataContext.ExternalCode,
                    ReceivedMessage = Checkpoint + "\n" + Context.Messages.Minify(),
                    ExceptionMessage = ex.GetErrorMessage(),
                    Operation = (int)Operation,
                    Lookup = Reference?.Id == null ? null : new Entity { LogicalName = xts_newvehiclewholesaleorder.EntityLogicalName, Id = Reference.Id }
                });
            }
            finally
            {
                "Processing SPK message finished.".ConsoleWriteLine();
            }
        }

        private void AttachBlankoToProspect()
        {
            Checkpoint = "Attachment Blanko - Prospect";
            if (DataContext.Prospect.ProspectAttachment == null) return;
            if (!DataContext.Prospect.ProspectAttachment.Any()) return;

            foreach (var attachment in DataContext.Prospect?.ProspectAttachment)
            {
                "Checking Prospect Attachment".ConsoleWriteLine();
                var ProspectAttachmentSubject = attachment?.Subject;
                var ProspectAttachmentFileName = attachment?.FileName ?? "Blanko";
                var qProspectAttachment = new List<Mapper<Annotation>.Queries>
                {
                    new Mapper<Annotation>.Queries {
                        Target = e => e.ObjectId,
                        Value = Prospect.Id
                    },
                    new Mapper<Annotation>.Queries {
                        Target = e => e.FileName,
                        Value = ProspectAttachmentFileName
                    },
                    new Mapper<Annotation>.Queries
                    {
                        Target = e => e.Subject,
                        Value = ProspectAttachmentSubject
                    }
                };

                var Attachment = new Mapper<Annotation>(Context.Service).FetchIgnoreStateCode(qProspectAttachment);
                if (!Attachment.Any())
                {
                    "Attaching Document Prospect.".ConsoleWriteLine();
                    var Notes = new Entity { LogicalName = Annotation.EntityLogicalName };
                    Notes["subject"] = ProspectAttachmentSubject;
                    Notes["objectid"] = Prospect.ToEntityReference();
                    Notes["objecttypecode"] = Opportunity.EntityTypeCode;
                    Notes["filename"] = ProspectAttachmentFileName;
                    Notes["documentbody"] = attachment?.Body;
                    Notes["mimetype"] = attachment?.FileMimeType ?? GetMimeType(attachment?.FileFormat);
                    Context.Service.Create(Notes);
                }
            }
        }

        private void AttachDokumenSPK()
        {
            Checkpoint = "Attachment Document SPK";
            if (DataContext.Attachment == null) return;
            if (!DataContext.Attachment.Any()) return;

            foreach (var attachment in DataContext.Attachment)
            {
                "Checking SPK Attachment".ConsoleWriteLine();
                var SPKAttachmentSubject = attachment?.Subject;
                var SPKAttachmentFileName = attachment?.FileName ?? "Document SPK";
                var qSPKAttachment = new List<Mapper<Annotation>.Queries>
                {
                    new Mapper<Annotation>.Queries {
                        Target = e => e.ObjectId,
                        Value = Reference.Id
                    },
                    new Mapper<Annotation>.Queries {
                        Target = e => e.FileName,
                        Value = SPKAttachmentFileName
                    },
                    new Mapper<Annotation>.Queries
                    {
                        Target = e => e.Subject,
                        Value = SPKAttachmentSubject
                    }
                };

                var Attachment = new Mapper<Annotation>(Context.Service).FetchIgnoreStateCode(qSPKAttachment);
                if (!Attachment.Any())
                {
                    "Attaching Document SPK.".ConsoleWriteLine();
                    var Notes = new Entity { LogicalName = Annotation.EntityLogicalName };
                    Notes["subject"] = SPKAttachmentSubject;
                    Notes["objectid"] = Reference.ToEntityReference();
                    Notes["objecttypecode"] = xts_newvehiclewholesaleorder.EntityTypeCode;
                    Notes["filename"] = SPKAttachmentFileName;
                    Notes["documentbody"] = attachment?.Body;
                    Notes["mimetype"] = attachment?.FileMimeType ?? GetMimeType(attachment?.FileFormat);
                    Context.Service.Create(Notes);
                }
            }
        }

        private bool RequiredDataCheckAndDataPrep()
        {
            Operation = OperationTask.Configuration;
            Checkpoint = "Prerequisite Data Checking";

            try
            {
                #region Data Supplied
                "Data Check".ConsoleWriteLine();
                if (string.IsNullOrWhiteSpace(DataContext.BlankoNo)) throw new Exception("BlankoNo was not supplied.");
                if (string.IsNullOrWhiteSpace(DataContext.Prospect?.Topic)) throw new Exception("Prospect criteria is not supplied: ProspectTopic.");
                if (string.IsNullOrWhiteSpace(DataContext.Prospect?.ContactName)) throw new Exception("Prospect criteria is not supplied: ProspectContactName.");
                if (string.IsNullOrWhiteSpace(DataContext.Prospect?.ProductCode)) throw new Exception("Prospect criteria is not supplied: ProspectProductCode.");
                if (string.IsNullOrWhiteSpace(DataContext.Prospect?.ContactAttachment?.First()?.Subject)) throw new Exception("Prospect criteria is not supplied: ProspectAttachmentSubject.");
                if (string.IsNullOrWhiteSpace(DataContext.Prospect?.ContactAttachment?.First()?.Body)) throw new Exception("Prospect criteria is not supplied: ProspectAttachmentDocument.");
                if (DataContext.Prospect?.LeadDate == null) throw new Exception("Prospect criteria is not supplied: ProspectLeadDate.");
                if (DataContext.Qty == null) throw new Exception("Prospect criteria is not supplied: Qty");
                #endregion

                #region GetReferenced
                "Get BU Reff".ConsoleWriteLine();
                var BUReff = new Mapper<BusinessUnit>(Context.Service)
                    .FetchIgnoreStateCode(DataContext.DealerCode, e => e.Name, e => e.xts_onbehalfteamid, e => e.ParentBusinessUnitId, e => e.xts_newvehiclesodefaultmatchingsiteid);
                if (!BUReff.Any()) throw new Exception(string.Format("BU:{0} is not found.", DataContext.DealerCode));
                else BU = BUReff.FirstOrDefault();

                "Get Currency Reff".ConsoleWriteLine();
                CurrencyReff = new Mapper<TransactionCurrency>(Context.Service)
                    .FetchIgnoreStateCode("IDR", e => e.ISOCurrencyCode, e => e.Id, e => e.ExchangeRate)?.First();

                "Get NVGS Reff".ConsoleWriteLine();
                var NVGSReff = new Mapper<xts_newvehiclegeneralsetup>(Context.Service).Fetch(BU.Name, e => e.xts_businessunitidName, e => e.xts_defaultsalesordertypeid, e => e.xts_defaultconsumptiontax1id);
                if (NVGSReff.Any()) NVGS = NVGSReff.FirstOrDefault();
                else NVGSReff = new Mapper<xts_newvehiclegeneralsetup>(Context.Service).Fetch(BU.ParentBusinessUnitIdName, e => e.xts_businessunitidName, e => e.xts_defaultsalesordertypeid, e => e.xts_defaultconsumptiontax1id);
                if (NVGSReff.Any()) NVGS = NVGSReff.FirstOrDefault();
                else throw new Exception(string.Format("NV General Setup for current Dealer ({0}) is not found.", DataContext.DealerCode));

                "Get Contact Reff".ConsoleWriteLine();
                var ContactReff = new Mapper<Contact>(Context.Service).Fetch(DataContext.Prospect?.ContactName, e => e.FullName);
                if (!ContactReff.Any()) throw new Exception(string.Format("Contact with Name:{0} is not found.", DataContext.Prospect?.ContactName));
                else Contact = ContactReff.FirstOrDefault();
                
                "Get Prospect Reff".ConsoleWriteLine();
                var q = new List<Mapper<Opportunity>.Queries>() {
                    //new Mapper<Opportunity>.Queries() {
                    //    Target = e => e.xts_leaddate,
                    //    Value = DataContext.Prospect?.LeadDate.ConvertFromUTC(Context.Service)
                    //},
                    new Mapper<Opportunity>.Queries {
                        Target = e => e.ParentContactIdName,
                        Value = DataContext.Prospect?.ContactName
                    },
                    new Mapper<Opportunity>.Queries {
                        Target = e => e.xts_productidName,
                        Value = DataContext.Prospect?.ProductCode
                    },
                    new Mapper<Opportunity>.Queries {
                        Target = e => e.xts_businessunitid,
                        Value = BU.Id
                    },
                    new Mapper<Opportunity>.Queries {
                        Target = e => "xto_quantity",
                        Value = DataContext.Qty
                    }
                };

                "Originating lead reff".ConsoleWriteLine();
                //Why ignore state code: because it might already qualified
                var leadid = new Mapper<Lead>(Context.Service).FetchIgnoreStateCode(DataContext.Prospect.Topic, e => e.Subject, e => e.Id)?.First().Id;
                if (leadid != null) {
                    q.Add(new Mapper<Opportunity>.Queries() {
                        Target = e => e.OriginatingLeadId,
                        Value = leadid
                    });
                }

                //Why ignore state code: because it might already Won
                var ProspectReff = new Mapper<Opportunity>(Context.Service).FetchIgnoreStateCode(q, e => e.StatusCode, e => e.ContactId, e => e.ParentContactId, e => e.xts_productid, e => e.xts_exteriorcolorid);
                if (!ProspectReff.Any()) {
                    var criteriaMsg = "";
                    foreach (var qc in q) criteriaMsg += qc.Target + " -> " + qc.Value + "\n";
                    throw new Exception(string.Format("Prospect with given criteria is not found:\n{0}", criteriaMsg));
                }
                else Prospect = ProspectReff.FirstOrDefault();

                "Get Employee Reff".ConsoleWriteLine();
                var SPReff = new Mapper<xts_employee>(Context.Service).FetchIgnoreStateCode(DataContext.EmployeeCode, e => e.ktb_employeecode);
                if (!SPReff.Any()) throw new Exception(string.Format("Employee with Code:{0} is not found.", DataContext.EmployeeCode));
                else Salesperson = SPReff.FirstOrDefault();
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Prerequisite
        private void WonProspect()
        {
            Operation = OperationTask.Update;
            Checkpoint = "Attempt to Won Prospect".ConsoleWriteLine();

            try
            {
                if (Prospect.StatusCode.Value == 3)
                {
                    "Prospect already Won.".ConsoleWriteLine();
                    return;
                }

                "Attempt to Renew Topic".ConsoleWriteLine();
                var target = new Entity { LogicalName = Opportunity.EntityLogicalName };
                target.Id = Prospect.Id;
                target["name"] = Prospect.Name + DetailBlanko?.xts_nvsonumberregistrationdetails1;
                Context.Service.Update(target);

                "Attempt to Won Prospect".ConsoleWriteLine();
                var CloseProspect = new Entity { LogicalName = OpportunityClose.EntityLogicalName };
                CloseProspect["opportunityid"] = Prospect.ToEntityReference();
                CloseProspect["actualrevenue"] = new Money(0);
                CloseProspect["actualend"] = DateTime.Now.ConvertFromUTC(Context.Service);
                CloseProspect["description"] = string.Empty;

                var WonRequest = new WinOpportunityRequest
                {
                    OpportunityClose = CloseProspect,
                    Status = new OptionSetValue((int)Opportunity_StatusCode.Won)
                };
                Context.Service.Execute(WonRequest);
                "Prospect closed as Won, Congratulation.".ConsoleWriteLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdateContactPreliminary()
        {
            try
            {
                Operation = OperationTask.Update;
                "Updating Contact Preliminary Data.".ConsoleWriteLine();
                var target = new Entity { LogicalName = Contact.EntityLogicalName };
                var CityReff = new Mapper<xts_city>(Context.Service).Fetch(DataContext.ContactCity, e => e.xts_city1, e => e.xts_provinceid).FirstOrDefault();
                var ProvinceReff = new Mapper<xts_province>(Context.Service).Fetch(CityReff.xts_provinceid.Name, e => e.xts_province1, e => e.xts_countryid)?.FirstOrDefault();

                target.Id = Prospect.ParentContactId.Id;
                target["xts_cityid"] = CityReff.ToEntityReference();
                target["xts_provinceid"] = CityReff.xts_provinceid;
                target["xts_countryid"] = ProvinceReff.xts_countryid;

                //target["ktb_pobox"] = DataContext.ContactPOBox;
                target["address1_line1"] = DataContext.Address1;
                target["xts_identificationtype"] = new OptionSetValue(1); //Identity Card
                target["xts_identificationnumber"] = DataContext.ContactIdentificationNo;

                "Attempt to update Contact.".ConsoleWriteLine();
                Context.Service.Update(target);
                "Contact Updated.".ConsoleWriteLine();

                foreach (var attachment in DataContext.Prospect?.ContactAttachment)
                {
                    "Checking contact attachment".ConsoleWriteLine();
                    var contactAttachmentSubject = attachment?.Subject;
                    var contactAttachmentFileName = attachment?.FileName ?? "Contact Attachment";
                    var qContactAttachment = new List<Mapper<Annotation>.Queries>
                    {
                        new Mapper<Annotation>.Queries {
                            Target = e => e.ObjectId,
                            Value = Contact.Id
                        },
                        new Mapper<Annotation>.Queries {
                            Target = e => e.FileName,
                            Value = contactAttachmentFileName
                        },
                        new Mapper<Annotation>.Queries
                        {
                            Target = e => e.Subject,
                            Value = contactAttachmentSubject
                        }
                    };
                    var contactAttachment = new Mapper<Annotation>(Context.Service)
                        .FetchIgnoreStateCode(qContactAttachment);

                    if (!contactAttachment.Any())
                    {
                        "Attaching Contact Attachment.".ConsoleWriteLine();
                        var Notes = new Entity { LogicalName = Annotation.EntityLogicalName };
                        Notes["subject"] = contactAttachmentSubject;
                        Notes["objectid"] = Contact.ToEntityReference();
                        Notes["objecttypecode"] = Contact.EntityTypeCode;
                        Notes["filename"] = contactAttachmentFileName;
                        Notes["documentbody"] = attachment?.Body;
                        Notes["mimetype"] = attachment?.FileMimeType ?? GetMimeType(attachment?.FileFormat);
                        Context.Service.Create(Notes);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ConvertContactToCustomer()
        {
            Operation = OperationTask.Update;
            Checkpoint = "Attempt to Convert Contact To Customer".ConsoleWriteLine();

            try
            {
                //Check whether contact already converted.
                var qCustomer = new List<Mapper<Account>.Queries> {
                    new Mapper<Account>.Queries {
                        Target = e => e.xts_originatingcontactid,
                        Value = Prospect.ParentContactId.Id
                    }
                };
                var alreadyExistCustomer = new Mapper<Account>(Context.Service).Fetch(qCustomer, e => e.Id, e => e.AccountNumber, e => e.Name);
                if (alreadyExistCustomer.Any())
                {
                    "Customer already exists. continuing with customer instead of converting.".ConsoleWriteLine();
                    Customer = alreadyExistCustomer.FirstOrDefault();
                }
                else
                {
                    "Converting contact to customer".ConsoleWriteLine();
                    var contactReff = new Mapper<Contact>(Context.Service)
                        .Fetch(Prospect.ParentContactId.Id, e => e.xts_customertype, e => e.ParentCustomerId);

                    var target = new Entity { LogicalName = Contact.EntityLogicalName };
                    target.Id = contactReff.Id;
                    target["xts_isdialog"] = true;
                    
                    Context.Service.Update(target);
                    qCustomer = new List<Mapper<Account>.Queries> {
                        new Mapper<Account>.Queries {
                            Target = e => e.xts_originatingcontactid,
                            Value = Prospect.ParentContactId.Id
                        }
                    };
                    Customer = new Mapper<Account>(Context.Service).Fetch(qCustomer, e => e.Id, e => e.AccountNumber, e => e.Name).First();
                    ("Contact converted. New Customer: " + Customer.AccountNumber).ConsoleWriteLine();
                }
            }
            catch (Exception ex)
            {
                //if (!ex.Message.ContainsAny("SYS", "JPN", "EXT", "KTB")) throw ex;

                if (ex.Message.Contains("KTB000004"))
                {
                    ex.Message.ConsoleWriteLine();
                    "Continue with existing customer.".ConsoleWriteLine();
                }
                else throw ex;
            }
        }

        private void CreateDetailBlanko()
        {
            Operation = OperationTask.Create;
            Checkpoint = "Attempt to Create Detail Blanko".ConsoleWriteLine();

            try
            {
                //Check Whether Detail with same blanko no Already Exists
                var reference = new Mapper<xts_nvsonumberregistrationdetails>(Context.Service).Fetch(DataContext.BlankoNo, e => e.xts_nvsonumberregistrationdetails1);
                if (reference.Any())
                {
                    String.Format("Record Detail Blanko with No:{0} already exists.", DataContext.BlankoNo).ConsoleWriteLine();
                    DetailBlanko = reference.First();
                    return;
                }

                var target = new Entity { LogicalName = xts_nvsonumberregistrationdetails.EntityLogicalName };
                target["xts_nvsonumberregistrationdetails"] = DataContext.BlankoNo;
                target["xts_status"] = new OptionSetValue(2); //Assign
                target["xts_assigndate"] = DateTime.UtcNow;
                target["xts_salespersonid"] = Salesperson.ToEntityReference();
                target["xts_businessunitid"] = BU.ToEntityReference();
                target["ownerid"] = BU.xts_onbehalfteamid;

                var id = Context.Service.Create(target);
                DetailBlanko = new Mapper<xts_nvsonumberregistrationdetails>(Context.Service).Fetch(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// SPK
        /// </summary>
        /// <param name="Ent"></param>
        /// <returns></returns>
        public override Entity PrepareData(Model.ExternalEntities.SPK Ent)
        {
            "SPK Preparation".ConsoleWriteLine();
            var reference = new xts_newvehiclewholesaleorder();
            var target = new Entity { LogicalName = xts_newvehiclewholesaleorder.EntityLogicalName };
            target["ktb_externalcode"] 
                //= target["xts_newvehiclewholesaleordernumber"] 
                = Ent.ExternalCode;

            #region SFL
            /*
            #region Money
            target["xts_accessoriesamount"] = Ent.AccessoriesAmount.ToMoney();
            target["xts_accessoriesdiscountamount"] = Ent.AccessoriesDiscountAmount.ToMoney();
            target["xts_balance"] = Ent.Balance.ToMoney();
            target["xts_bookingfeeamount"] = Ent.BookingFeeAmount.ToMoney();
            target["xts_compulsoryinsuranceamount"] = Ent.CompulsoryInsuranceAmount.ToMoney();
            target["xts_consumptiontaxtotalamount"] = Ent.ConsumptionTaxTotalAmount.ToMoney();
            target["xts_couponamount"] = Ent.CouponAmount.ToMoney();
            target["xts_dealerdiscount"] = Ent.DealerDiscount.ToMoney();
            target["xts_downpaymentamount"] = Ent.DownPaymentAmount.ToMoney();
            target["xts_downpaymentamountreceived"] = Ent.DownPaymentAmountReceived.ToMoney();
            target["xts_netamount"] = Ent.NetAmount.ToMoney();
            target["xts_netamountaftertax"] = Ent.NetAmountAfterTax.ToMoney();
            target["xts_nontaxablemiscellaneouschargeamount"] = Ent.NonTaxableMiscellaneousChargeAmount.ToMoney();
            target["xts_othertaxamount"] = Ent.OtherTaxAmount.ToMoney();
            target["xts_outstandingamount"] = Ent.OutstandingAmount.ToMoney();
            target["xts_taxablemiscellaneouschargeamount"] = Ent.TaxableMiscellaneousChargeAmount.ToMoney();
            target["xts_titleregistrationfee"] = Ent.TitleRegistrationFee.ToMoney();
            target["xts_totalpriceamount"] = Ent.TotalPriceAmount.ToMoney();
            target["xts_totalreceipt"] = Ent.TotalReceipt.ToMoney();
            target["xts_tradeinamount"] = Ent.TradeInAmount.ToMoney();
            target["xjp_tradeinrecyclingamount"] = Ent.TradeInRecyclingAmount.ToMoney();
            target["xts_vehicleamount"] = Ent.VehicleAmount.ToMoney();
            target["xts_vehiclediscountamount"] = Ent.VehicleDiscountAmount.ToMoney();
            target["xts_vehiclediscountpercentage"] = Ent.VehicleDiscountPercentage;
            target["xts_vehiclerelatedproductamount"] = Ent.VehicleRelatedProductAmount.ToMoney();
            target["xts_voluntaryinsuranceamount"] = Ent.VoluntaryInsuranceAmount.ToMoney();
            target["xjp_recyclingamount"] = Ent.RecyclingAmount.ToMoney();
            target["xts_referralamount"] = Ent.ReferralAmount.ToMoney();
            target["xts_specialcolorpriceamount"] = Ent.SpecialColorPriceAmount.ToMoney();
            target["xts_subsidizeddiscount"] = Ent.SubsidizedDiscount.ToMoney();
            #endregion

            #region OptionSet
            target["ktb_rejectedreasonoption"] = Ent.AlasanBatalOptions.ToOptionsetValue();
            target["ktb_cbu_bodytype1"] = Ent.BentukBodyKendaraanCV.ToOptionsetValue();
            target["ktb_cbu_bodytypelcv1"] = Ent.BentukBodyKendaraanLCV.ToOptionsetValue();
            target["ktb_cbu_loadprofile1"] = Ent.BidangUsahaKonsumen.ToOptionsetValue();
            target["ktb_cbu_waypaid1"] = Ent.CaraPembayaran.ToOptionsetValue();
            target["ktb_categorycode"] = Ent.CategoryCode.ToOptionsetValue();
            target["ktb_cbu_medanoperasi1"] = Ent.DaerahUtamaOperasi.ToOptionsetValue();
            target["ktb_cbu_jeniskend"] = Ent.JenisKendaraan.ToOptionsetValue();
            target["ktb_cbu_purcstat"] = Ent.KendaraanIniSbgKend.ToOptionsetValue();
            target["ktb_cbu_purcstat2"] = Ent.KendaraanSebagai.ToOptionsetValue();
            target["ktb_cbu_ownership1"] = Ent.KepemilikanKendaraan.ToOptionsetValue();
            target["ktb_cbu_purpose2"] = Ent.KeperluanAngkutan.ToOptionsetValue();
            target["ktb_cbu_modelkend"] = Ent.ModelKendaraan.ToOptionsetValue();
            target["ktb_cbu_purpose1"] = Ent.PenggunaanUtamaKendaraan.ToOptionsetValue();
            target["ktb_additional"] = Ent.Tambahan.ToOptionsetValue();
            target["ktb_tempdocument"] = Ent.TempDocument.ToOptionsetValue();
            target["ktb_cbu_userage1"] = Ent.UsiaPemilikKendaraan.ToOptionsetValue();
            #endregion

            #region DateTime
            if (!Ent.ContractDate.IsMinValue()) target["ktb_contractdate"] = Ent.ContractDate;
            if (!Ent.ScheduledDeliveryDate.IsMinValue()) target["ktb_scheduleddeliverydate"] = Ent.ScheduledDeliveryDate;
            if (!Ent.TransactionDate.IsMinValue()) target["xts_transactiondate"] = Ent.TransactionDate;
            #endregion

            #region LookUp
            target["xts_addressid"] = Ent.AddressCode;
            target["ktb_benefitid"] = Ent.Benefit;
            target["xjp_billtoid"] = Ent.BillTo;
            target["ktb_campaignid"] = Ent.Campaign;
            target["ktb_karoseriid"] = Ent.Karoseri;
            target["ktb_kecamatanid"] = Ent.Kecamatan;
            target["ktb_kelurahanid"] = Ent.Kelurahan;
            target["ktb_leasingcompanyid"] = Ent.LeasingCompany;
            target["xts_potentialcustomerid"] = Ent.NamaKonsumenFaktur;
            target["ktb_nvsoregistrationdetailnumberid"] = Ent.BlankoNo;
            target["ktb_namakonsumenspkid"] = Ent.NamaKonsumenSPK;
            target["xts_opportunityid"] = Ent.OpportunityNo;
            target["xts_potentialcontactid"] = Ent.PotentialCustomerContacts;
            target["xts_salespersonid"] = Ent.EmployeeCode;
            target["xts_termofpaymentid"] = Ent.TermsofPayment;
            #endregion

            #region ASIS
            target["xts_address1"] = Ent.Address1;
            target["xts_address2"] = Ent.Address2;
            target["xts_address3"] = Ent.Address3;
            target["ktb_address4"] = Ent.Address4;
            target["ktb_rejectedreason"] = Ent.AlasanBatal;
            target["ktb_begbal"] = Ent.begbal;
            target["xjp_billtonumber"] = Ent.BillToNo;
            target["ktb_contractnumber"] = Ent.ContractNumber;
            target["xts_customernumber"] = Ent.CustomerNo;
            target["ktb_dnetid"] = Ent.DNetID;
            target["ktb_dnetsapcustomerid"] = Ent.DNetSAPCustomerId;
            target["ktb_dnetspkcustomerid"] = Ent.DNetSPKCustomerID;
            target["ktb_dnetspkdetailid"] = Ent.DNetSPKDetailID;
            target["ktb_dnetspknumber"] = Ent.DNetSPKNumber;
            target["xts_downpaymentamountispaid"] = Ent.DownPaymentisPaid;
            target["ktb_lkppno"] = Ent.LKPPNo;
            target["xts_phonenumber"] = Ent.PhoneNo;
            target["xts_zipcode"] = Ent.PostalCode;
            target["ktb_qty"] = Ent.Qty;
            target["ktb_remarks"] = Ent.Remarks;
            target["ktb_spkdnetreference"] = Ent.SPKDnetReference;
            #endregion
            
            #region Unnecessary - SFL
            //target["ktb_handling"] = Ent.Handling;
            //target["xts_handling"] = Ent.Handling;
            //target["ktb_interfaceexceptionmessage"] = Ent.InterfaceMessage;
            //target["ktb_isinterfaced"] = Ent.IsInterfaced;
            target["ktb_statusinterfacednet"] = Ent.StatusInterfaceDNet;
            #endregion
            */
            #endregion

            "SP001".ConsoleWriteLine();
            target["xts_businessunitid"] = BU.ToEntityReference();
            target["xts_parentbusinessunitid"] = BU.ParentBusinessUnitId;
            target["ownerid"] = BU.xts_onbehalfteamid;
            target["transactioncurrencyid"] = CurrencyReff.ToEntityReference();
            
            "SP002".ConsoleWriteLine();
            target["xts_transactiondate"] = DataContext.TransactionDate ?? DateTime.Today.ConvertFromUTC(Context.Service);
            target["xts_opportunityid"] = Prospect.ToEntityReference();
            target["xts_salespersonid"] = Salesperson.ToEntityReference();
            target["xts_address1"] = DataContext.Address1;
            target["ktb_qty"] = DataContext.Qty;
            target["ktb_nvsoregistrationdetailnumberid"] = DetailBlanko.ToEntityReference();
            target["ktb_namakonsumenspkid"] = Customer.ToEntityReference();
            target["xts_potentialcustomerid"] = Customer.ToEntityReference();
            target["xts_customernumber"] = Customer.AccountNumber;
            target["xts_phonenumber"] = Customer.Telephone3;
            target["xjp_billtoid"] = Customer.ToEntityReference();
            target["xts_potentiallookuptype"] = 1;
            target["xts_potentiallookupname"] = Customer.Name;
            target["xts_outstandingamount"] = DataContext.OutstandingAmount.ToMoney();
            target["ktb_scheduleddeliverydate"] 
                = DataContext.ScheduledDeliveryDate.ConvertFromUTC(Context.Service);

            "SP003".ConsoleWriteLine();
            target["xts_status"] = new OptionSetValue(1);
            target["ktb_status"] = new OptionSetValue(1);
            target["xts_handling"] = new OptionSetValue(1);
            target["ktb_handling"] = new OptionSetValue(1);
            target["ktb_statusinterfacednet"] = new OptionSetValue(1);

            "SP004".ConsoleWriteLine();
            if (DataContext.KepemilikanKendaraan != null) target["ktb_cbu_ownership1"] = new OptionSetValue((int)DataContext.KepemilikanKendaraan);
            if (DataContext.UsiaPemilikKendaraan != null) target["ktb_cbu_userage1"] = new OptionSetValue((int)DataContext.UsiaPemilikKendaraan);
            if (DataContext.BidangUsahaKonsumen != null) target["ktb_cbu_loadprofile1"] = new OptionSetValue((int)DataContext.BidangUsahaKonsumen);
            if (DataContext.PenggunaanUtamaKendaraan != null) target["ktb_cbu_purpose1"] = new OptionSetValue((int)DataContext.PenggunaanUtamaKendaraan);
            if (DataContext.KendaraanIniSbgKend != null) target["ktb_cbu_purcstat"] = new OptionSetValue((int)DataContext.KendaraanIniSbgKend);
            if (DataContext.KeperluanAngkutan != null) target["ktb_cbu_purpose2"] = new OptionSetValue((int)DataContext.KeperluanAngkutan);
            if (DataContext.KendaraanSebagai != null) target["ktb_cbu_purcstat2"] = new OptionSetValue((int)DataContext.KendaraanSebagai);
            if (DataContext.DaerahUtamaOperasi != null) target["ktb_cbu_medanoperasi1"] = new OptionSetValue((int)DataContext.DaerahUtamaOperasi);

            "SP004.1".ConsoleWriteLine();
            if (DataContext.Tambahan != null) target["ktb_additional"] = new OptionSetValue((int)DataContext.Tambahan);
            target["ktb_remarks"] = DataContext.Remarks;
            if (DataContext.CaraPembayaran != null) target["ktb_cbu_waypaid1"] = new OptionSetValue((int)DataContext.CaraPembayaran);
            if (DataContext.AlasanBatalOptions != null) target["ktb_rejectedreasonoption"] = new OptionSetValue((int)DataContext.AlasanBatalOptions);
            target["ktb_rejectedreason"] = DataContext.AlasanBatal;

            "SP005".ConsoleWriteLine();
            //target["ktb_karoseriid"] = DataContext.Karoseri;
            if (DataContext.CategoryCode != null) target["ktb_categorycode"] = new OptionSetValue((int)DataContext.CategoryCode);
            if (DataContext.BentukBodyKendaraanCV != null) target["ktb_cbu_bodytype1"] = new OptionSetValue((int)DataContext.BentukBodyKendaraanCV);
            if (DataContext.BentukBodyKendaraanLCV != null) target["ktb_cbu_bodytypelcv1"] = new OptionSetValue((int)DataContext.BentukBodyKendaraanLCV);
            if (DataContext.JenisKendaraan != null) target["ktb_cbu_jeniskend"] = new OptionSetValue((int)DataContext.JenisKendaraan);
            if (DataContext.ModelKendaraan != null) target["ktb_cbu_modelkend"] = new OptionSetValue((int)DataContext.ModelKendaraan);

            "SP006".ConsoleWriteLine();
            if (!string.IsNullOrWhiteSpace(DataContext.LeasingCompany)) {
                var leasingCompany = new Mapper<ktb_leasingcompany>(Context.Service)
                    .Fetch(DataContext.LeasingCompany, e => e.ktb_leasingcompany1);
                "SP007".ConsoleWriteLine();

                if (leasingCompany.Any())
                    target["ktb_leasingcompanyid"] = leasingCompany.First().ToEntityReference();
                else
                    "SP009".ConsoleWriteLine();
            }

            "SP010".ConsoleWriteLine();
            return target;
        } 

        /// <summary>
        /// SPK Details
        /// </summary>
        private void CreateDetails()
        {
            Checkpoint = "Attempt to Create SPK Details".ConsoleWriteLine();

            try
            {
                foreach (var SPKD in DataContext.Details)
                {
                    var target = new Entity { LogicalName = xts_newvehiclesalesorder.EntityLogicalName };

                    "P1:General".ConsoleWriteLine();
                    #region Page1:General
                    target["xts_transactiondate"] = SPKD.TransactionDate ?? DataContext.TransactionDate ?? DateTime.Today.ConvertFromUTC(Context.Service); //(Reference as xts_newvehiclesalesorder).xts_transactiondate;
                    target["ktb_konfirmasics"] = false; //SPKD.KonfirmasiCS;
                    target["xts_wholesaleorderid"] = Reference.ToEntityReference();
                    target["xts_ordertypeid"] = NVGS.xts_defaultsalesordertypeid;
                    target["xts_vehicleorderinvoicing"] = new OptionSetValue(3); //Manual // SPKD.VehicleOrderInvoicing;
                    target["xjp_usagecategory"] = new OptionSetValue(1); //Private // SPKD.UsageCategory;
                    target["xts_nvsonumberregistrationdetailid"] = DetailBlanko.ToEntityReference(); // SPKD.nvso BlankoSPKNo;
                    target["xts_matchingtype"] = new OptionSetValue(1); //Manual // SPKD.MatchingType;
                    target["xts_salespersonid"] = Salesperson.ToEntityReference(); // SPKD.SalesPerson;
                    target["ktb_opportunitynoid"] = Prospect.ToEntityReference();
                    target["ktb_nobookingfeedp"] = false; // SPKD.BookingFeeDP;
                    target["xts_change"] = false; // SPKD.Change;
                    target["xts_potentialcustomerid"] = Customer.ToEntityReference(); //SPKD.PotentialCustomer;
                    target["xjp_vehiclemanagementid"] = Customer.ToEntityReference(); //SPKD.NamaFakturBPKPdanSTNK;
                    target["xts_customernumber"] = Customer.AccountNumber; //SPKD.CustomerNo;
                    target["xts_address1"] = DataContext.Address1;
                    target["xts_billtoid"] = Customer.ToEntityReference(); //SPKD.BillTo;
                    target["xts_scheduleddeliverydate"] = DataContext.ScheduledDeliveryDate.ConvertFromUTC(Context.Service);
                    #endregion

                    "P2:VecInfo".ConsoleWriteLine();
                    #region Page2: Vehicle Information
                    var ProductRef = new Mapper<xts_product>(Context.Service)
                        .Fetch(Prospect.xts_productid.Id, e => e.xts_productsegment2id, e => e.xts_productsegment3id);

                    target["xts_productid"] = ProductRef.ToEntityReference(); //SPKD.Product;
                    target["ktb_vehiclecategoryid"] = ProductRef.xts_productsegment3id; //SPKD.VehicleCategory;
                    target["ktb_vehiclemodelid"] = ProductRef.xts_productsegment2id; //SPKD.VehicleModel;
                    target["ktb_vehicledescription"] = ProductRef.xts_description; //SPKD.VehicleDescription;
                    target["xts_siteid"] = BU.xts_newvehiclesodefaultmatchingsiteid; //SPKD.Site;
                    target["xts_productexteriorcolorid"] = Prospect.xts_exteriorcolorid; //SPKD.ExteriorColor;
                    //target["ktb_vehiclecolorname"] = SPKD.Vehicle ColorName;
                    target["ktb_additional"] = new OptionSetValue((int)DataContext.Tambahan);
                    target["xts_registrationrequired"] = SPKD.RegistrationRequired;
                    target["xts_requestplatenumber"] = SPKD.RequestPlateNumber;
                    target["xjp_vehiclemanagementcategory"] = new OptionSetValue(41); //Potential Customer //SPKD.VehicleManagementCategory;
                    target["xjp_vehiclemanagementnumber"] = Customer.Name; // SPKD.VehicleManagementNo;
                    target["xts_vehiclemanagementcompleteaddress"] = DataContext.Address1; //SPKD.VehicleManagementCompleteAddress; 
                    #endregion

                    "P3:PriceInfo".ConsoleWriteLine();
                    #region Page3: Price Information
                    target["xts_fixedprice"] = true; //SPKD.FixedPrice;
                    //target["xts_specialcolorpriceid"] = SPKD.VehiclePriceDetail;
                    //target["xts_vehiclepricelistid"] = SPKD.VehiclePriceList;
                    target["ktb_estimasiharga"] = SPKD.EstimasiHarga.ToMoney();
                    target["ktb_biayamediator"] = SPKD.BiayaMediator.ToMoney();
                    target["ktb_ongkoskirim"] = SPKD.OngkosKirim.ToMoney();
                    target["ktb_cashbackapm"] = SPKD.CashbackAPM.ToMoney();
                    if (SPKD.Pembayaran != null) target["ktb_pembayaran"] = new OptionSetValue((int)SPKD.Pembayaran);
                    target["ktb_leasingid"]
                        = target["xts_financingcompanyid"]
                        = new Mapper<Account>(Context.Service).Fetch(DataContext.LeasingCompany, e => e.AccountNumber);
                    target["xts_bookingfeeamount"] = SPKD.BookingFeeAmount.ToMoney();
                    target["xts_outstandingamount"] = SPKD.OutstandingAmount.ToMoney();
                    target["ktb_dppengurangtax"] = SPKD.DPPengurangTax.ToMoney();
                    target["xts_downpaymentamount"] = SPKD.DownPaymentAmount.ToMoney();
                    target["xts_downpaymentispaid"] = SPKD.DownPaymentisPaid;
                    target["xts_registrationfeeispaid"] = SPKD.RegistrationFeeisPaid;
                    if (SPKD.PriceOption != null) target["xts_priceoption"] = new OptionSetValue((int)SPKD.PriceOption);
                    target["xts_specialcolorpriceamount"] = SPKD.SpecialColorPriceAmount.ToMoney();
                    target["xts_titleregistrationfee"] = SPKD.TitleRegistrationFee.ToMoney(); //SPKD.bbn;
                    target["xts_accessoriesamount"] = SPKD.AccessoriesAmount.ToMoney();
                    target["xts_vehiclerelatedproductamount"] = SPKD.VehicleRelatedProductAmount.ToMoney();
                    target["xts_miscchargebaseamount"] = SPKD.MiscChargeBaseAmount.ToMoney();
                    target["xts_totalpriceamount"] = SPKD.TotalPriceAmount.ToMoney();
                    ////target["xts_vehiclediscountpercentage"] = SPKD.fleet discount percentage;
                    //target["xts_vehiclediscountamount"] = SPKD.fleet discount;
                    target["ktb_ageyeardiscount"] = SPKD.AgeYearDiscount.ToMoney();
                    target["xts_subsidizeddiscount"] = SPKD.SubsidizedDiscount.ToMoney();
                    target["xts_subsidizedleasing"] = SPKD.SubsidizedLeasing.ToMoney();
                    target["xts_subsidizedsoleagent"] = SPKD.SubsidizedSoleAgent.ToMoney();
                    target["xts_subsidizedmaindealer"] = SPKD.SubsidizedMainDealer.ToMoney();
                    target["xts_dealerdiscount"] = SPKD.DealerDiscount.ToMoney();
                    target["xts_netamount"] = SPKD.NetAmount.ToMoney();
                    target["xts_overmaximumdiscount"] = SPKD.OverMaximumDiscount;
                    target["xts_totaldiscountamount"] = SPKD.TotalDiscountAmount.ToMoney();
                    target["xts_maximumdiscountamount"] = SPKD.MaximumDiscountAmount.ToMoney();
                    target["xts_totalreceiptamount"] = SPKD.TotalReceiptAmount.ToMoney();
                    target["xts_balance"] = SPKD.DownPaymentAmountReceived.ToMoney();
                    target["xts_referralamount"] = SPKD.ReferralAmount.ToMoney();
                    target["xjp_voluntaryinsuranceamount"] = SPKD.VoluntaryInsuranceAmount.ToMoney();
                    #endregion

                    "P4:TaxInfo".ConsoleWriteLine();
                    #region Page4: Tax Information
                    target["xts_consumptiontax1id"] = NVGS.xts_defaultconsumptiontax1id;
                    target["xts_consumptiontax1amount"] = SPKD.ConsumptionTax1Amount.ToMoney();
                    //should be automatically calculated 
                    //target["xts_vehicletaxamount"] = new Money(0); // SPKD.VehicleTaxAmount;
                    //target["xjp_othertaxamount"] = new Money(0); // SPKD.OtherTaxAmount;
                    //target["xts_accessoriestaxamount"] = new Money(0); // SPKD.AccessoriesTaxAmount;
                    //target["xts_miscellaneouschargestaxamount"] = new Money(0); // SPKD.MiscellaneousChargesTaxAmount;
                    //target["xts_vehiclerelatedproducttaxamount"] = new Money(0); // SPKD.VehicleRelatedProductTaxAmount;
                    //target["xts_consumptiontaxtotalamount"] = new Money(0); // SPKD.ConsumptionTaxTotalAmount;
                    //target["xts_netamountaftertax"] = new Money(0); // SPKD.NetAmountAfterTax;
                    target["xjp_taxationform"] = new OptionSetValue(1); //Taxable //SPKD.TaxationForm;
                    target["xjp_taxablebusinesscategory"] = new OptionSetValue(2); //Taxable //SPKD.TaxableBusinessCategory;
                    target["xjp_locationclasscategory"] = new OptionSetValue(1); //1st Class //SPKD.LocationClassCategory;
                    #endregion

                    "P5:".ConsoleWriteLine();
                    #region Page5: Other Information
                    target["xts_alreadydelivered"] = false; //SPKD.AlreadyDelivered;
                    #endregion

                    "P6:Interface".ConsoleWriteLine();
                    #region Page6: Interface
                    if (DataContext.KepemilikanKendaraan != null) target["ktb_cbu_ownership1"] = new OptionSetValue((int)DataContext.KepemilikanKendaraan);
                    if (DataContext.UsiaPemilikKendaraan != null) target["ktb_cbu_userage1"] = new OptionSetValue((int)DataContext.UsiaPemilikKendaraan);
                    if (DataContext.BidangUsahaKonsumen != null) target["ktb_cbu_loadprofile1"] = new OptionSetValue((int)DataContext.BidangUsahaKonsumen);
                    if (DataContext.PenggunaanUtamaKendaraan != null) target["ktb_cbu_purpose1"] = new OptionSetValue((int)DataContext.PenggunaanUtamaKendaraan);
                    if (DataContext.KendaraanIniSbgKend != null) target["ktb_cbu_purcstat"] = new OptionSetValue((int)DataContext.KendaraanIniSbgKend);
                    if (DataContext.KeperluanAngkutan != null) target["ktb_cbu_purpose2"] = new OptionSetValue((int)DataContext.KeperluanAngkutan);
                    if (DataContext.KendaraanSebagai != null) target["ktb_cbu_purcstat2"] = new OptionSetValue((int)DataContext.KendaraanSebagai);
                    if (DataContext.DaerahUtamaOperasi != null) target["ktb_cbu_medanoperasi1"] = new OptionSetValue((int)DataContext.DaerahUtamaOperasi);
                    if (DataContext.CategoryCode != null) target["ktb_categorycode"] = new OptionSetValue((int)DataContext.CategoryCode);
                    if (DataContext.BentukBodyKendaraanCV != null) target["ktb_cbu_bodytype1"] = new OptionSetValue((int)DataContext.BentukBodyKendaraanCV);
                    if (DataContext.BentukBodyKendaraanLCV != null) target["ktb_cbu_bodytypelcv1"] = new OptionSetValue((int)DataContext.BentukBodyKendaraanLCV);
                    if (DataContext.JenisKendaraan != null) target["ktb_cbu_jeniskend"] = new OptionSetValue((int)DataContext.JenisKendaraan);
                    if (DataContext.ModelKendaraan != null) target["ktb_cbu_modelkend"] = new OptionSetValue((int)DataContext.ModelKendaraan);
                    target["ktb_rejectedreason"] = DataContext.AlasanBatal;
                    if (DataContext.AlasanBatalOptions != null) target["ktb_rejectedreasonoption"] = new OptionSetValue((int)DataContext.AlasanBatalOptions);
                    #endregion

                    "P7:Hidden".ConsoleWriteLine();
                    #region P7: Hidden
                    target["xts_status"] = new OptionSetValue(1); //Open
                    target["xts_handling"] = new OptionSetValue(1); //No Action
                    target["xts_parentbusinessunitid"] = BU.ParentBusinessUnitId;
                    target["xts_businessunitid"] = BU.ToEntityReference();
                    target["ownerid"] = BU.xts_onbehalfteamid;
                    target["transactioncurrencyid"] = CurrencyReff.ToEntityReference();
                    target["xts_userid"] = Customer.ToEntityReference();
                    #endregion

                    "PP:Execute".ConsoleWriteLine();
                    Context.Service.Create(target);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Obsolete("SFL")]
        private void CreateDetail_BackUp()
        {
            Checkpoint = "Attempt to Create SPK Details".ConsoleWriteLine();

            try
            {
                foreach (var spkd in DataContext.Details)
                {
                    var target = new Entity { LogicalName = xts_newvehiclesalesorder.EntityLogicalName };

                    #region SFL - Potential unused
                    //target["ktb_typecaroseries"] = spkd.[UNUSED]JenisKaroseri;
                    //target["xts_addressid"] = spkd.[UNUSED]AddressCode;
                    //target["ktb_BenefitId"] = spkd.[UNUSED]Benefit;
                    //target["xts_log"] = spkd.Log;
                    #endregion

                    #region Money
                    target["ktb_ageyeardiscount"] = spkd.AgeYearDiscount.ToMoney();
                    target["ktb_biayamediator"] = spkd.BiayaMediator.ToMoney();
                    target["ktb_cashbackapm"] = spkd.CashbackAPM.ToMoney();
                    target["ktb_dppengurangtax"] = spkd.DPPengurangTax.ToMoney();
                    target["ktb_estimasiharga"] = spkd.EstimasiHarga.ToMoney();
                    target["ktb_ongkoskirim"] = spkd.OngkosKirim.ToMoney();
                    target["xjp_accessoriesacquisitiontaxamount"] = spkd.AccessoriesAcquisitionTaxAmount.ToMoney();
                    target["xjp_accessoriesestimatedprofitamount"] = spkd.AccEstProfitAmount.ToMoney();
                    target["xjp_accessoriesestimatedtotalcost"] = spkd.AccEstTotalCost.ToMoney();
                    target["xjp_additionalfluorocarbonfee"] = spkd.AdditionalFluorocarbonFee.ToMoney();
                    target["xjp_additionalshredderdustfee"] = spkd.AdditionalShredderDustFee.ToMoney();
                    target["xjp_airbagfee"] = spkd.AirbagFee.ToMoney();
                    target["xjp_automobiletaxamount"] = spkd.AutomobileTaxAmount.ToMoney();
                    target["xjp_bonuspaymentamount1"] = spkd.BonusPaymentAmount1.ToMoney();
                    target["xjp_bonuspaymentamount2"] = spkd.BonusPaymentAmount2.ToMoney();
                    target["xjp_fluorocarbonfee"] = spkd.FluorocarbonFee.ToMoney();
                    target["xjp_fundmanagementfee"] = spkd.FundManagementFee.ToMoney();
                    target["xjp_informationmanagementfee"] = spkd.InformationManagementFee.ToMoney();
                    target["xjp_methodofpaymentestimatedcost"] = spkd.MethodofPaymentEstCost.ToMoney();
                    target["xjp_methodofpaymentestimatedprofit"] = spkd.MethodofPaymentEstProfit.ToMoney();
                    target["xjp_othertaxamount"] = spkd.OtherTaxAmount.ToMoney();
                    target["xjp_recyclingamount"] = spkd.RecyclingAmount.ToMoney();
                    target["xjp_registrationpaymentamount"] = spkd.RegistrationPaymentAmount.ToMoney();
                    target["xjp_shredderdustfee"] = spkd.ShredderDustFee.ToMoney();
                    target["xjp_totalacquisitiontaxamount"] = spkd.TotalAcquisitionTaxAmount.ToMoney();
                    target["xjp_totalestimatedcostamount"] = spkd.TotalEstCostAmount.ToMoney();
                    target["xjp_totalestimatedprofitamount"] = spkd.TotalEstProfitAmount.ToMoney();
                    target["xjp_totalnetsalesamount"] = spkd.TotalNetSalesAmount.ToMoney();
                    target["xjp_totalsalesprice"] = spkd.TotalSalesPrice.ToMoney();
                    target["xjp_variouscostamount"] = spkd.VariousCostAmount.ToMoney();
                    target["xjp_variousestimatedprofitamount"] = spkd.VariousEstProfitAmount.ToMoney();
                    target["xjp_vehicleacquisitiontaxamount"] = spkd.VehicleAcquisitionTaxAmount.ToMoney();
                    target["xjp_vehicleestimatedcost"] = spkd.VehicleEstimatedCost.ToMoney();
                    target["xjp_vehicleestimatedprofitamount"] = spkd.VehicleEstProfitAmount.ToMoney();
                    target["xjp_voluntaryinsuranceamount"] = spkd.VoluntaryInsuranceAmount.ToMoney();
                    target["xjp_weighttaxamount"] = spkd.WeightTaxAmount.ToMoney();
                    target["xts_accessoriesamount"] = spkd.AccessoriesAmount.ToMoney();
                    target["xts_accessoriesdiscountamount"] = spkd.AccessoriesDiscountAmount.ToMoney();
                    target["xts_accessoriesnetsalesprice"] = spkd.AccessoriesNetSalesPrice.ToMoney();
                    target["xts_accessoriestaxamount"] = spkd.AccessoriesTaxAmount.ToMoney();
                    target["xts_administrationfee"] = spkd.AdministrationFee.ToMoney();
                    target["xts_annualpaymentamount"] = spkd.AnnualPaymentAmount.ToMoney();
                    target["xts_balance"] = spkd.DownPaymentAmountReceived.ToMoney();
                    target["xts_bankchargesfee"] = spkd.BankChargesFee.ToMoney();
                    target["xts_bookingfeeamount"] = spkd.BookingFeeAmount.ToMoney();
                    target["xts_calculatedpaymentamountpayment"] = spkd.CalculatedPaymentAmountPayment.ToMoney();
                    target["xts_compulsoryinsuranceamount"] = spkd.CompulsoryInsuranceAmount.ToMoney();
                    target["xts_consumptiontax1amount"] = spkd.ConsumptionTax1Amount.ToMoney();
                    target["xts_consumptiontax2amount"] = spkd.ConsumptionTax2Amount.ToMoney();
                    target["xts_consumptiontaxtotalamount"] = spkd.ConsumptionTaxTotalAmount.ToMoney();
                    target["xts_dealerdiscount"] = spkd.DealerDiscount.ToMoney();
                    target["xts_discountamount"] = spkd.DiscountAmount.ToMoney();
                    target["xts_discountamountbeforetax"] = spkd.DiscountAmountBeforeTax.ToMoney();
                    target["xts_downpaymentamount"] = spkd.DownPaymentAmount.ToMoney();
                    target["xts_downpaymentamountpayment"] = spkd.DownPaymentAmountPayment.ToMoney();
                    target["xts_financingamount"] = spkd.FinancingAmount.ToMoney();
                    target["xts_firstpaymentamount"] = spkd.FirstPaymentAmount.ToMoney();
                    target["xts_interestamount"] = spkd.InterestAmount.ToMoney();
                    target["xts_maximumdiscountamount"] = spkd.MaximumDiscountAmount.ToMoney();
                    target["xts_methodofpaymentestimatedsalesamount"] = spkd.MethodofPaymentEstSalesAmount.ToMoney();
                    target["xts_miscchargebaseamount"] = spkd.MiscChargeBaseAmount.ToMoney();
                    target["xts_miscellaneouschargestaxamount"] = spkd.MiscellaneousChargesTaxAmount.ToMoney();
                    target["xts_netamount"] = spkd.NetAmount.ToMoney();
                    target["xts_netamountaftertax"] = spkd.NetAmountAfterTax.ToMoney();
                    target["xts_nontaxablemiscellaneouschargeamount"] = spkd.NonTaxableMiscellaneousChargeAmount.ToMoney();
                    target["xts_otherdeductionamountpayment"] = spkd.OtherDeductionAmountPayment.ToMoney();
                    target["xts_outstandingamount"] = spkd.OutstandingAmount.ToMoney();
                    target["xts_priceamountbeforetax"] = spkd.PriceAmountBeforeTax.ToMoney();
                    target["xts_referralamount"] = spkd.ReferralAmount.ToMoney();
                    target["xts_remainingfinancingamount"] = spkd.RemainingFinanceAmount.ToMoney();
                    target["xts_specialcolorpriceamount"] = spkd.SpecialColorPriceAmount.ToMoney();
                    target["xts_subsidizeddiscount"] = spkd.SubsidizedDiscount.ToMoney();
                    target["xts_subsidizedleasing"] = spkd.SubsidizedLeasing.ToMoney();
                    target["xts_subsidizedmaindealer"] = spkd.SubsidizedMainDealer.ToMoney();
                    target["xts_subsidizedsoleagent"] = spkd.SubsidizedSoleAgent.ToMoney();
                    target["xts_taxablemiscellaneouschargeamount"] = spkd.TaxableMiscellaneousChargeAmount.ToMoney();
                    target["xts_titleregistrationfee"] = spkd.TitleRegistrationFee.ToMoney();
                    target["xts_totaldiscountamount"] = spkd.TotalDiscountAmount.ToMoney();
                    target["xts_totalpayment"] = spkd.TotalPayment.ToMoney();
                    target["xts_totalpriceamount"] = spkd.TotalPriceAmount.ToMoney();
                    target["xts_totalreceiptamount"] = spkd.TotalReceiptAmount.ToMoney();
                    target["xts_tradeindiscount"] = spkd.TradeInDiscount.ToMoney();
                    target["xts_tradeinnetvalue"] = spkd.TradeInNetValue.ToMoney();
                    target["xts_tradeinnetvaluepayment"] = spkd.TradeInNetValuePayment.ToMoney();
                    target["xts_tradeinrecyclingamount"] = spkd.TradeInRecyclingAmount.ToMoney();
                    target["xts_tradeintaxamount"] = spkd.TradeInTaxAmount.ToMoney();
                    target["xts_tradeinvalue"] = spkd.TradeInValue.ToMoney();
                    target["xts_variousamount"] = spkd.VariousAmount.ToMoney();
                    target["xts_vehicleamount"] = spkd.VehicleAmount.ToMoney();
                    target["xts_vehiclediscountamount"] = spkd.VehicleDiscountAmount.ToMoney();
                    target["xts_vehiclenetsalesprice"] = spkd.VehicleNetSalesPrice.ToMoney();
                    target["xts_vehiclerelatedproductamount"] = spkd.VehicleRelatedProductAmount.ToMoney();
                    target["xts_vehiclerelatedproducttaxamount"] = spkd.VehicleRelatedProductTaxAmount.ToMoney();
                    target["xts_vehicletaxamount"] = spkd.VehicleTaxAmount.ToMoney();
                    target["xts_withholdingtax2amount"] = spkd.WithholdingTax2Amount.ToMoney();
                    target["xts_withholdingtaxamount"] = spkd.WithholdingTax1Amount.ToMoney();
                    #endregion

                    #region OptionSet
                    target["ktb_additional"] = spkd.Tambahan;
                    target["ktb_birojasa"] = spkd.BiroJasa;


                    #endregion

                    #region DateTime
                    target["ktb_contractdate"] = spkd.ContractDate.ConvertFromUTC(Context.Service);
                    target["xjp_confirmedpaymentdate"] = spkd.ConfirmedPaymentDate.ConvertFromUTC(Context.Service);
                    target["xjp_confirmedregistrationpaymentdate"] = spkd.ConfirmedRegistrationPaymentDate.ConvertFromUTC(Context.Service);
                    target["xjp_desireddeliverydate"] = spkd.DesiredDeliveryDate.ConvertFromUTC(Context.Service);
                    target["xjp_desiredregistrationdate"] = spkd.DesiredRegistrationDate.ConvertFromUTC(Context.Service);
                    target["xjp_expectedpaymentdate"] = spkd.ExpectedPaymentDate.ConvertFromUTC(Context.Service);
                    target["xjp_expectedregistrationdate"] = spkd.ExpectedRegistrationDate.ConvertFromUTC(Context.Service);
                    target["xjp_expectedregistrationpaymentdate"] = spkd.ExpectedRegistrationPaymentDate.ConvertFromUTC(Context.Service);
                    target["xjp_insuranceexpireddate"] = spkd.InsuranceExpiredDate.ConvertFromUTC(Context.Service);
                    target["xjp_promisedeliverydate"] = spkd.PromiseDeliveryDate.ConvertFromUTC(Context.Service);
                    target["xjp_registrationexpecteddate"] = spkd.RegistrationExpectedDate.ConvertFromUTC(Context.Service);
                    target["xjp_registrationresultdate"] = spkd.RegistrationResultDate.ConvertFromUTC(Context.Service);
                    target["xjp_requestedplatenumberdate"] = spkd.RequestedPlateNumberDate.ConvertFromUTC(Context.Service);
                    target["xts_approvaldate"] = spkd.ApprovalDate.ConvertFromUTC(Context.Service);
                    target["xts_assignvsotosalesdate"] = spkd.AssignNVSOtoSalesDate.ConvertFromUTC(Context.Service);
                    target["xts_canceldeliverydate"] = spkd.CancelDeliveryDate.ConvertFromUTC(Context.Service);
                    target["xts_cancelleddate"] = spkd.CanceledDate.ConvertFromUTC(Context.Service);
                    target["xts_certificateoftitlenumberreceiptdate"] = spkd.CertificateofTitleNumberReceiptDate.ConvertFromUTC(Context.Service);
                    target["xts_certificateoftitlenumbersubmitdate"] = spkd.CertificateofTitleNumberSubmitDate.ConvertFromUTC(Context.Service);
                    target["xts_deliverydate"] = spkd.DeliveryDate.ConvertFromUTC(Context.Service);
                    target["xts_financingpurchaseorderdate"] = spkd.FinancingPODate.ConvertFromUTC(Context.Service);
                    target["xts_invoiceduedate"] = spkd.InvoiceDueDate.ConvertFromUTC(Context.Service);
                    target["xts_matchdate"] = spkd.MatchDate.ConvertFromUTC(Context.Service);
                    target["xts_newvehiclesalescertificatereceiptdate"] = spkd.NVSalesCertificateReceiptDate.ConvertFromUTC(Context.Service);
                    target["xts_newvehiclesalescertificatesubmitDate"] = spkd.NVSalesCertificateSubmitDate.ConvertFromUTC(Context.Service);
                    target["xts_paymentenddate"] = spkd.PaymentEndDate.ConvertFromUTC(Context.Service);
                    target["xts_paymentstartdate"] = spkd.PaymentStartDate.ConvertFromUTC(Context.Service);
                    target["xts_predeliveryinspectioncancellationdate"] = spkd.PDICancellationDate.ConvertFromUTC(Context.Service);
                    target["xts_preferredregistrationdate"] = spkd.PreferredRegistrationDate.ConvertFromUTC(Context.Service);
                    target["xts_purchaseorderdate"] = spkd.PODate.ConvertFromUTC(Context.Service);
                    target["xts_salesdate"] = spkd.InvoicePostingDate.ConvertFromUTC(Context.Service);
                    target["xts_scheduleddeliverydate"] = spkd.ScheduledDeliveryDate.ConvertFromUTC(Context.Service);
                    target["xts_transactiondate"] = spkd.TransactionDate?.ConvertFromUTC(Context.Service);
                    target["xts_unmatchdate"] = spkd.UnmatchDate.ConvertFromUTC(Context.Service);
                    target["xts_vehicleregistrationdeliverydate"] = spkd.VehicleRegistrationDeliveryDate.ConvertFromUTC(Context.Service);
                    target["xts_vehicleregistrationreceiptdate"] = spkd.VehicleRegistrationReceiptDate.ConvertFromUTC(Context.Service);
                    target["xts_vehicleregistrationsubmitdate"] = spkd.VehicleRegistrationSubmitDate.ConvertFromUTC(Context.Service);
                    #endregion

                    #region LookUp
                    if (!string.IsNullOrWhiteSpace(spkd.Campaign)) target["ktb_CampaignId"] = new Mapper<Campaign>(Context.Service).Fetch(spkd.Campaign, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.KaroseriCompany)) target["ktb_karosericompanyid"] = new Mapper<xts_vendor>(Context.Service).Fetch(spkd.KaroseriCompany, e => e.xts_vendor1)?.FirstOrDefault()?.ToEntityReference();
                    /* Entity Ref Pack missing */ //if (!string.IsNullOrWhiteSpace(spkd.Kecamatan)) target["ktb_kecamatanid"] = new Mapper<ktb_kecamatan>(Context.Service).Fetch(spkd.Kecamatan, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    /* Entity Ref Pack missing */ //if (!string.IsNullOrWhiteSpace(spkd.Kelurahan)) target["ktb_kelurahanid"] = new Mapper<ktb_kelurahan>(Context.Service).Fetch(spkd.Kelurahan, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Leasing)) target["ktb_leasingid"] = new Mapper<Account>(Context.Service).Fetch(spkd.Leasing, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Benefit)) target["ktb_newbenefitid"] = new Mapper<ktb_benefit>(Context.Service).Fetch(spkd.Benefit, e => e.ktb_benefitregno)?.FirstOrDefault()?.ToEntityReference();

                    /* Need confirm of PFQuery */ //if (!string.IsNullOrWhiteSpace(spkd.OpportunityNo)) target["ktb_OpportunityNoId"] = new Mapper<opportunity>(Context.Service).Fetch(spkd.OpportunityNo, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                                                  /* Potential Duplicate */ //if (!string.IsNullOrWhiteSpace(spkd.Opportunity)) target["xts_opportunityid"] = new Mapper<opportunity>(Context.Service).Fetch(spkd.Opportunity, e => e.Name)?.FirstOrDefault()?.ToEntityReference();

                    if (!string.IsNullOrWhiteSpace(spkd.Program)) target["ktb_ProgramId"] = new Mapper<xts_subsidyanddiscount>(Context.Service).Fetch(spkd.Program, e => e.xts_subsidyanddiscount1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.VehicleCategory)) target["ktb_vehiclecategoryid"] = new Mapper<xts_productsegment3>(Context.Service).Fetch(spkd.VehicleCategory, e => e.xts_productsegment31)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.VehicleModel)) target["ktb_vehiclemodelid"] = new Mapper<xts_productsegment2>(Context.Service).Fetch(spkd.VehicleModel, e => e.xts_productsegment21)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.LastDocumentRegistrationNo)) target["xid_lastdocumentregistrationid"] = new Mapper<xid_documentregistration>(Context.Service).Fetch(spkd.LastDocumentRegistrationNo, e => e.xid_documentregistrationnumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.AcquisitionTax)) target["xjp_acquisitiontaxid"] = new Mapper<xjp_newvehicleacquisitiontax>(Context.Service).Fetch(spkd.AcquisitionTax, e => e.xjp_newvehicleacquisitiontax1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.AutomobileTax)) target["xjp_automobiletaxid"] = new Mapper<xjp_automobiletax>(Context.Service).Fetch(spkd.AutomobileTax, e => e.xjp_automobiletax1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.DeliveryLocation)) target["xjp_deliverylocationid"] = new Mapper<xts_common>(Context.Service).Fetch(spkd.DeliveryLocation, e => e.xts_common1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.InsuranceCategory)) target["xjp_insurancecategoryid"] = new Mapper<xts_common>(Context.Service).Fetch(spkd.InsuranceCategory, e => e.xts_common1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.PlateNoSegment1)) target["xjp_platenumbersegment1id"] = new Mapper<xjp_standardplate>(Context.Service).Fetch(spkd.PlateNoSegment1, e => e.xjp_standardplate1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.RegistrationAddress)) target["xjp_registrationaddressid"] = new Mapper<xts_moreaddress>(Context.Service).Fetch(spkd.RegistrationAddress, e => e.xts_moreaddress1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.RegistrationAgencyBU)) target["xjp_registrationagencybusinessunitid"] = new Mapper<BusinessUnit>(Context.Service).Fetch(spkd.RegistrationAgencyBU, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.RegistrationAgency)) target["xjp_registrationagencyid"] = new Mapper<xts_vendor>(Context.Service).Fetch(spkd.RegistrationAgency, e => e.xts_vendor1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.RegistrationProcessBy)) target["xjp_registrationprocessbyid"] = new Mapper<xts_common>(Context.Service).Fetch(spkd.RegistrationProcessBy, e => e.xts_common1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.RegistrationRequestNo)) target["xjp_registrationrequestnumberid"] = new Mapper<xjp_registrationrequest>(Context.Service).Fetch(spkd.RegistrationRequestNo, e => e.xjp_registrationrequestnumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.ServiceBU)) target["xjp_servicebusinessunitid"] = new Mapper<BusinessUnit>(Context.Service).Fetch(spkd.ServiceBU, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.NamaFakturBPKPdanSTNK)) target["xjp_vehiclemanagementid"] = new Mapper<Account>(Context.Service).Fetch(spkd.NamaFakturBPKPdanSTNK, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.WeightTax)) target["xjp_weighttaxid"] = new Mapper<xjp_weighttax>(Context.Service).Fetch(spkd.WeightTax, e => e.xjp_weighttax1)?.FirstOrDefault()?.ToEntityReference();
                    /* Entity Ref Pack Missing */ //if (!string.IsNullOrWhiteSpace(spkd.AddressCode)) target["xts_addressnewid"] = new Mapper<xts_villageandstreet>(Context.Service).Fetch(spkd.AddressCode, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.BillTo)) target["xts_billtoid"] = new Mapper<Account>(Context.Service).Fetch(spkd.BillTo, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Company)) target["xts_companyid"] = new Mapper<Account>(Context.Service).Fetch(spkd.Company, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.ConsumptionTax1)) target["xts_consumptiontax1id"] = new Mapper<xts_consumptiontax>(Context.Service).Fetch(spkd.ConsumptionTax1, e => e.xts_consumptiontax1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.ConsumptionTax2)) target["xts_consumptiontax2id"] = new Mapper<xts_consumptiontax>(Context.Service).Fetch(spkd.ConsumptionTax2, e => e.xts_consumptiontax1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.BillToFinancing)) target["xts_financingcompanyid"] = new Mapper<Account>(Context.Service).Fetch(spkd.BillToFinancing, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Grade)) target["xts_gradeid"] = new Mapper<xts_grade>(Context.Service).Fetch(spkd.Grade, e => e.xts_grade1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.InsuranceCompany)) target["xts_insurancecompanyid"] = new Mapper<xts_vendor>(Context.Service).Fetch(spkd.InsuranceCompany, e => e.xts_vendor1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.MethodofPayment)) target["xts_methodofpaymentid"] = new Mapper<xts_common>(Context.Service).Fetch(spkd.MethodofPayment, e => e.xts_common1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.MiscChargesTemplate)) target["xts_miscchargestemplateid"] = new Mapper<xts_miscellaneouschargetemplate>(Context.Service).Fetch(spkd.MiscChargesTemplate, e => e.xts_miscellaneouschargetemplate1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.NVSalesQuotationNo)) target["xts_newvehiclesalesquoteid"] = new Mapper<xts_newvehiclesalesquote>(Context.Service).Fetch(spkd.NVSalesQuotationNo, e => e.xts_salesquotenumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.OrderType)) target["xts_ordertypeid"] = new Mapper<xts_ordertype>(Context.Service).Fetch(spkd.OrderType, e => e.xts_ordertype1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.OrigNVSORefNo)) target["xts_originalnewvehiclesalesorderreferenceid"] = new Mapper<xts_newvehiclesalesorder>(Context.Service).Fetch(spkd.OrigNVSORefNo, e => e.xts_newvehiclesalesordernumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Other)) target["xts_otherid"] = new Mapper<Account>(Context.Service).Fetch(spkd.Other, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.VehicleOwner)) target["xts_ownerid"] = new Mapper<Account>(Context.Service).Fetch(spkd.VehicleOwner, e => e.Name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.PersoninCharge)) target["xts_personinchargeid"] = new Mapper<xts_employee>(Context.Service).Fetch(spkd.PersoninCharge, e => e.xts_employee1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.PotentialCustomerContacts)) target["xts_potentialcontactid"] = new Mapper<Contact>(Context.Service).Fetch(spkd.PotentialCustomerContacts, e => e.FullName)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.PotentialCustomer)) target["xts_potentialcustomerid"] = new Mapper<Account>(Context.Service).Fetch(spkd.PotentialCustomer, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Configuration)) target["xts_productconfigurationid"] = new Mapper<xts_productconfiguration>(Context.Service).Fetch(spkd.Configuration, e => e.xts_productconfiguration1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.ExteriorColor)) target["xts_productexteriorcolorid"] = new Mapper<xts_productexteriorcolor>(Context.Service).Fetch(spkd.ExteriorColor, e => e.xts_productexteriorcolor1)?.FirstOrDefault()?.ToEntityReference();
                    /* Need to Query By Company also */ //if (!string.IsNullOrWhiteSpace(spkd.Product)) target["xts_productid"] = new Mapper<xts_product>(Context.Service).Fetch(spkd.Product, e => e.xts_product1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.InteriorColor)) target["xts_productinteriorcolorid"] = new Mapper<xts_productinteriorcolor>(Context.Service).Fetch(spkd.InteriorColor, e => e.xts_productinteriorcolor1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Style)) target["xts_productstyleid"] = new Mapper<xts_productstyle>(Context.Service).Fetch(spkd.Style, e => e.xts_productstyle1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.PurchaseOrderNo)) target["xts_purchaseorderid"] = new Mapper<xts_purchaseorder>(Context.Service).Fetch(spkd.PurchaseOrderNo, e => e.xts_purchaseordernumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.RecommendedProduct)) target["xts_recommendedproductid"] = new Mapper<xts_recommendedproduct>(Context.Service).Fetch(spkd.RecommendedProduct, e => e.xts_recommendedproduct1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.SalesOrderNo)) target["xts_salesorderid"] = new Mapper<xts_salesorder>(Context.Service).Fetch(spkd.SalesOrderNo, e => e.xts_salesordernumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.SalesPerson)) target["xts_salespersonid"] = new Mapper<xts_employee>(Context.Service).Fetch(spkd.SalesPerson, e => e.xts_employee1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Site)) target["xts_siteid"] = new Mapper<xts_site>(Context.Service).Fetch(spkd.Site, e => e.xts_name)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.VehiclePriceDetail)) target["xts_specialcolorpriceid"] = new Mapper<xts_vehiclepricedetail>(Context.Service).Fetch(spkd.VehiclePriceDetail, e => e.xts_vehiclepricedetail1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.StockNumber)) target["xts_stockid"] = new Mapper<xts_inventorynewvehicle>(Context.Service).Fetch(spkd.StockNumber, e => e.xts_stocknumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.TermsofPayment)) target["xts_termsofpaymentid"] = new Mapper<xts_termofpayment>(Context.Service).Fetch(spkd.TermsofPayment, e => e.xts_termofpayment1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.User)) target["xts_userid"] = new Mapper<Account>(Context.Service).Fetch(spkd.User, e => e.AccountNumber)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.VehiclePriceList)) target["xts_vehiclepricelistid"] = new Mapper<xts_vehicleprice>(Context.Service).Fetch(spkd.VehiclePriceList, e => e.xts_vehicleprice1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.VehicleSpecification)) target["xts_vehiclespecificationid"] = new Mapper<xts_vehiclespecification>(Context.Service).Fetch(spkd.VehicleSpecification, e => e.xts_vehiclespecification1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.Warehouse)) target["xts_warehouseid"] = new Mapper<xts_warehouse>(Context.Service).Fetch(spkd.Warehouse, e => e.xts_warehouse1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.WithholdingTax1)) target["xts_withholdingtaxid"] = new Mapper<xts_consumptiontax>(Context.Service).Fetch(spkd.WithholdingTax1, e => e.xts_consumptiontax1)?.FirstOrDefault()?.ToEntityReference();
                    if (!string.IsNullOrWhiteSpace(spkd.WithholdingTax2)) target["xts_withholdingtax2id"] = new Mapper<xts_consumptiontax>(Context.Service).Fetch(spkd.WithholdingTax2, e => e.xts_consumptiontax1)?.FirstOrDefault()?.ToEntityReference();

                    #endregion

                    target["xjp_registrationagencylookuptype"] = spkd.RegistrationAgencyLookupType;
                    target["xts_potentiallookuptype"] = spkd.PotentialCustomerLookupType;

                    #region Asis
                    //int
                    target["ktb_dnetid"] = spkd.DNetID;
                    target["ktb_dnetspkcustomerid"] = spkd.DNetSPKCustomerID;
                    target["ktb_IDDNet"] = spkd.IDDNet;
                    target["xjp_bonuspayment1applymonth"] = spkd.BonusPayment1ApplyMonth;
                    target["xjp_bonuspayment2applymonth"] = spkd.BonusPayment2ApplyMonth;
                    target["xjp_insuranceperiod"] = spkd.InsurancePeriod;
                    target["xts_financingperiod"] = spkd.FinancingPeriod;
                    target["xts_totalpaymentmonth"] = spkd.TotalPaymentMonth;

                    //double
                    target["xts_interestpercentage"] = spkd.InterestPercentage;
                    target["xts_vehiclediscountpercentage"] = spkd.VehicleDiscountPercentage;

                    //bool
                    target["ktb_isinterfaced"] = spkd.HasilInterface;
                    target["ktb_konfirmasics"] = spkd.KonfirmasiCS;
                    target["ktb_NoBookingFeeDP"] = spkd.BookingFeeDP;
                    target["xts_alreadydelivered"] = spkd.AlreadyDelivered;
                    target["xts_change"] = spkd.Change;
                    target["xts_downpaymentispaid"] = spkd.DownPaymentisPaid;
                    target["xts_fixedprice"] = spkd.FixedPrice;
                    target["xts_insurancefreeofcharge"] = spkd.InsuranceFreeofCharge;
                    target["xts_insuranceneeded"] = spkd.InsuranceNeeded;
                    target["xts_matchingrulepassed"] = spkd.MatchingRulePassed;
                    target["xts_overmaximumdiscount"] = spkd.OverMaximumDiscount;
                    target["xts_registrationfeeispaid"] = spkd.RegistrationFeeisPaid;
                    target["xts_registrationrequired"] = spkd.RegistrationRequired;
                    target["xts_requestedplatenumber"] = spkd.RequestedPlateNumber;

                    //etc unsorted
                    target["ktb_categorycode"] = DataContext.CategoryCode;
                    target["ktb_cbu_bodytype1"] = DataContext.BentukBodyKendaraanCV;
                    target["ktb_cbu_bodytypelcv1"] = DataContext.BentukBodyKendaraanLCV;
                    target["ktb_cbu_jeniskend"] = DataContext.JenisKendaraan;
                    target["ktb_cbu_loadprofile1"] = DataContext.BidangUsahaKonsumen;
                    target["ktb_cbu_medanoperasi1"] = DataContext.DaerahUtamaOperasi;
                    target["ktb_cbu_modelkend"] = DataContext.ModelKendaraan;
                    target["ktb_cbu_ownership1"] = DataContext.KepemilikanKendaraan;
                    target["ktb_cbu_purcstat"] = DataContext.KendaraanIniSbgKend;
                    target["ktb_cbu_purcstat2"] = DataContext.KendaraanSebagai;
                    target["ktb_cbu_purpose1"] = DataContext.PenggunaanUtamaKendaraan;
                    target["ktb_cbu_purpose2"] = DataContext.KeperluanAngkutan;
                    target["ktb_cbu_userage1"] = DataContext.UsiaPemilikKendaraan;
                    #endregion

                    //Asis candidate
                    target["ktb_address4"] = spkd.Address4;
                    target["ktb_bustock"] = spkd.BUStock;


                    target["ktb_contractnumber"] = spkd.ContractNumber;
                    target["ktb_dnetspknumber"] = spkd.DNetSPKNumber;
                    target["ktb_ftaccessories"] = spkd.Accessories_tidakdimantainstock;
                    target["ktb_handlinginvoicecompany"] = spkd.HandlingInvoiceCompany;
                    target["ktb_LKPPNumber"] = spkd.LKPPNumber;
                    target["ktb_mediator"] = spkd.Mediator;
                    target["ktb_pembayaran"] = spkd.Pembayaran;
                    target["ktb_rejectedreason"] = spkd.AlasanBatal;
                    target["ktb_rejectedreasonoption"] = spkd.AlasanBatal;
                    target["ktb_remarks"] = spkd.Remarks;
                    target["ktb_statusinvoicecompany"] = spkd.StatusInvoiceCompany;
                    target["ktb_tanggalkonfirmasi"] = spkd.TanggalKonfirmasi;
                    target["ktb_vehicledescription"] = spkd.VehicleDescription;
                    target["xjp_idempotentmessage"] = spkd.IdempotentMessage;
                    target["xjp_insuranceapplymethod"] = spkd.InsuranceApplyMethod;
                    target["xjp_locationclasscategory"] = spkd.LocationClassCategory;
                    target["xjp_ownershipcategory"] = spkd.OwnershipCategory;
                    target["xjp_platenumber"] = spkd.PlateNo;
                    target["xjp_platenumbersegment2"] = spkd.PlateNoSegment2;
                    target["xjp_platenumbersegment3"] = spkd.PlateNoSegment3;
                    target["xjp_platenumbersegment4"] = spkd.PlateNoSegment4;
                    target["xjp_productsubsidycategory"] = spkd.ProductSubsidyCategory;
                    target["xjp_registrationdocumentrefnumberid"] = spkd.RegistrationDocumentRefNo;
                    target["xjp_registrationmethod"] = spkd.RegistrationMethod;
                    target["xjp_remark"] = spkd.Remark;
                    target["xjp_taxablebusinesscategory"] = spkd.TaxableBusinessCategory;
                    target["xjp_taxationform"] = spkd.TaxationForm;
                    target["xjp_usagecategory"] = spkd.UsageCategory;
                    target["xjp_vehiclemanagementcategory"] = spkd.VehicleManagementCategory;
                    target["xjp_vehiclemanagementnumber"] = spkd.VehicleManagementNo;
                    target["xjp_vehiclespecificnumber"] = spkd.VehiclespecificNumber;
                    target["xts_address1"] = spkd.Address1;
                    target["xts_address2"] = spkd.Address2;
                    target["xts_address3"] = spkd.Address3;
                    target["xts_billtocompleteaddress"] = spkd.BillToCompleteAddress;
                    target["xts_billtodescription"] = spkd.BillToDescription;
                    target["xts_billtonumber"] = spkd.BillToNo;
                    target["xts_billtophone"] = spkd.BillToPhone;
                    target["xts_certificateoftitlenumber"] = spkd.CertificateofTitleNo;
                    target["xts_classificationnumber"] = spkd.ClassificationNumber;
                    target["xts_companycompleteaddress"] = spkd.CompanyCompleteAddress;
                    target["xts_companydescription"] = spkd.CompanyDescription;
                    target["xts_companynumber"] = spkd.CompanyNo;
                    target["xts_companyphone"] = spkd.CompanyPhone;
                    target["xts_customercontactdescription"] = spkd.CustomerContactDescription;
                    target["xts_customernumber"] = spkd.CustomerNo;
                    target["xts_eventdata"] = spkd.EventData;
                    target["xts_financingpurchaseordernumber"] = spkd.FinancingPONo;
                    target["xts_gradedescription"] = spkd.GradeDescription;
                    target["xts_gradenumber"] = spkd.GradeNo;
                    target["xts_handling"] = spkd.Handling;
                    target["xts_insurancetype"] = spkd.InsuranceType;
                    target["xts_matchingtype"] = spkd.MatchingType;
                    target["xts_modelspecification"] = spkd.Modelspecification;
                    target["xts_newvehiclesalescertificatenumber"] = spkd.NVSalesCertificateNo;
                    target["xts_newvehiclesalesordernumber"] = spkd.SPKDetailNo;
                    target["xts_othercompleteaddress"] = spkd.OtherCompleteAddress;
                    target["xts_otherdescription"] = spkd.OtherDescription;
                    target["xts_othernumber"] = spkd.OtherNo;
                    target["xts_otherphone"] = spkd.OtherPhone;
                    target["xts_ownercompleteaddress"] = spkd.OwnerCompleteAddress;
                    target["xts_ownerdescription"] = spkd.OwnerDescription;
                    target["xts_ownernumber"] = spkd.OwnerNo;
                    target["xts_ownerphone"] = spkd.OwnerPhone;
                    target["xts_phonenumber"] = spkd.PhoneNo;
                    target["xts_postalcode"] = spkd.PostalCode;
                    target["xts_potentialcustomerdescription"] = spkd.PotentialCustomerDescription;
                    target["xts_priceoption"] = spkd.PriceOption;
                    target["xts_productdescription"] = spkd.ProductDescription;
                    target["xts_purchaseorderstatus"] = spkd.POStatus;
                    target["xts_registrationagencyinvoicenumber"] = spkd.RegistrationAgencyInvoiceNo;
                    target["xts_requestplatenumber"] = spkd.RequestPlateNumber;
                    target["xts_status"] = spkd.State;
                    target["xts_tradeintaxcategory"] = spkd.TradeinTaxCategory;
                    target["xts_usercompleteaddress"] = spkd.UserCompleteAddress;
                    target["xts_userdescription"] = spkd.UserDescription;
                    target["xts_usernumber"] = spkd.UserNo;
                    target["xts_userphone"] = spkd.UserPhone;
                    target["xts_vehiclemanagementcompleteaddress"] = spkd.VehicleManagementCompleteAddress;
                    target["xts_vehiclemanagementdescription"] = spkd.VehicleManagementDescription;
                    target["xts_vehiclemanagementphone"] = spkd.VehicleManagementPhone;
                    target["xts_vehicleorderinvoicing"] = spkd.VehicleOrderInvoicing;
                    target["xts_vehicleregistrationnumber"] = spkd.VehicleRegistrationNo;




                    #region Prev-Reference
                    target["xts_nvsonumberregistrationdetailid"] = DetailBlanko.ToEntityReference();
                    #endregion

                    #region Populated
                    var BUReff = new Mapper<BusinessUnit>(Context.Service)
                    .FetchIgnoreStateCode(DataContext.DealerCode, e => e.Name, e => e.Id, e => e.ParentBusinessUnitId, e => e.xts_onbehalfteamid)
                    ?.First();

                    var PBUReff = BUReff?.ParentBusinessUnitId;
                    var OwnerReff = BUReff?.xts_onbehalfteamid;
                    
                    target["xts_businessunitid"] = BUReff?.ToEntityReference();
                    target["xts_parentbusinessunitid"] = PBUReff;
                    target["ownerid"] = OwnerReff;
                    target["transactioncurrencyid"] = CurrencyReff?.ToEntityReference();
                    target["exchangerate"] = CurrencyReff?.ExchangeRate;
                    #endregion

                    //Parent Reference
                    target["xts_wholesaleorderid"] = Reference.ToEntityReference();
                    Context.Service.Create(target);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Release SPK
        /// </summary>
        private void SetHeaderHandlingAsAwal()
        {
            Checkpoint = "Attempt to Set Handling As Awal".ConsoleWriteLine();

            try
            {
                var target = new Entity { LogicalName = xts_newvehiclewholesaleorder.EntityLogicalName };
                target.Id = Reference.Id;
                target["xts_handling"] = new OptionSetValue(2); //Release
                target["ktb_handling"] = new OptionSetValue(101); //Awal

                Context.Service.Update(target);
                "SPK Set As Awal & Released".ConsoleWriteLine();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Utility
        private string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (regKey != null && regKey.GetValue("Content Type") != null)
                mimeType = regKey.GetValue("Content Type").ToString();
            return mimeType;
        }
        #endregion
    }
}
