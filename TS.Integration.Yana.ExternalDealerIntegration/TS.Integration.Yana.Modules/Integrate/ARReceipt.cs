﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using OP.MSCRM.Wrapper;
using Technosoft.BSI.EntityClasses.Entities;
using TS.Integration.Yana.Model.ExternalEntities;
using TS.Integration.Yana.Utility;
using TS.Integration.Yana.Utility.Custom;
using CoreEntities = Technosoft.DMS.XRM.EntityClasses.Entities;

namespace TS.Integration.Yana.Modules.Integrate
{
    public class ARReceipt : BaseOperation<Model.ExternalEntities.ARReceipt>
    {
        public ARReceipt(BaseExecute e, Model.ExternalEntities.ARReceipt te) : base(e, te) { }

        public override OperationTask OperationIdentifier()
        {
            var opt = new Mapper<xts_accountreceivablereceipt>(Context.Service)
                .Fetch(DataContext.ExternalCode, e => ExternalCodeScheme, e => e.Id);

            if (!opt.Any()) return OperationTask.Create;
            Reference.Id = opt.First().Id;

            return OperationTask.Update;
        }

        public override void Execute()
        {
            try
            {
                PrintExecuteLog = false;
                base.Execute();
                if (Reference != null)
                { 
                    if (Reference.Id != null && Reference.Id != Guid.Empty)
                    {
                        CreateDetail();
                        //"Release is disabled for debugging Purposes".ConsoleWriteLine();
                        ReleaseHeader();
                        
                        new ExternalDealerLog(Context, new EDILog<Entity>
                        {
                            DealerIdentifier = DataContext.DealerCode,
                            Context = DataContext.Context,
                            ExternalCode = DataContext.ExternalCode,
                            ReceivedMessage = Context.Messages,
                            Operation = (int)Operation,
                            Lookup = new Entity { LogicalName = xts_accountreceivablereceipt.EntityLogicalName, Id = (Guid)Reference.Id }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                new ExternalDealerLog(Context, new EDILog<Entity>
                {
                    DealerIdentifier = DataContext.DealerCode,
                    Context = DataContext.Context,
                    ExternalCode = DataContext.ExternalCode,
                    ReceivedMessage = Checkpoint + "\n" + Context.Messages,
                    ExceptionMessage = ex.GetErrorMessage(),
                    Operation = (int)Operation,
                    Lookup = Reference?.Id == null ? null : new Entity { LogicalName = xts_accountreceivablereceipt.EntityLogicalName, Id = Reference.Id }
                });
            }
        }

        public override Entity PrepareData(Model.ExternalEntities.ARReceipt Ent)
        {
            var reff = new xts_accountreceivablereceipt();
            var target = new Entity { LogicalName = xts_accountreceivablereceipt.EntityLogicalName };

            try
            {
                Checkpoint = "01";
                #region ASIS
                target["ktb_externalcode"] = Ent.ExternalCode;
                target["xts_cancelled"] = Ent.Cancelled;
                target["xts_transactiondate"] = Ent.TransactionDate;
                target["xts_bookingfee"] = Ent.BookingFee;
                target["xts_arreceipttype"] = new OptionSetValue(Ent.Type);
                #endregion

                Checkpoint = "02";
                #region Amt
                target["xts_totalamount"] = Ent.AppliedtoDocument.ToMoney();
                target["xts_paymentsettlement"] = Ent.AvailableBalance.ToMoney();
                target["xts_totalotherexpenses"] = Ent.TotalOtherExpensesOrIncome.ToMoney();
                target["xts_totalreceiptamount"] = Ent.TotalReceiptAmount.ToMoney();
                #endregion

                Checkpoint = "03";
                #region Lookup
                target["xts_newvehiclewholesaleorderid"] = string.IsNullOrWhiteSpace(Ent.SPKNo) ? null
                    : new Mapper<xts_newvehiclewholesaleorder>(Context.Service)
                        .Fetch(Ent.SPKNo, e => e.xts_newvehiclewholesaleordernumber)?.First().ToEntityReference();
                
                target["xts_accountreceivablereceiptreferenceid"] = string.IsNullOrWhiteSpace(Ent.ARReceiptReferenceNo) ? null
                    : new Mapper<xts_accountreceivablereceipt>(Context.Service)
                        .Fetch(Ent.ARReceiptReferenceNo, e => e.xts_accountreceivablereceiptnumber)?.First().ToEntityReference();
                
                target["xts_methodofpaymentid"] = string.IsNullOrWhiteSpace(Ent.MethodofPaymentNo) ? null
                    : new Mapper<xts_common>(Context.Service)
                        .Fetch(Ent.MethodofPaymentNo, e => e.xts_common1)?.First().ToEntityReference();

                #region Customer x Receive Dimension
                Checkpoint = "03:CRD";
                var CustomerReff = string.IsNullOrWhiteSpace(Ent.CustomerNo) ? null
                    : new Mapper<Account>(Context.Service)
                        .Fetch(Ent.CustomerNo, e => e.AccountNumber, e => e.xts_customerclassid, e => e.Name)?.First();

                if (CustomerReff != null)
                {
                    target["xts_customernumber"] = Ent.CustomerNo;
                    target["xts_customerid"] = CustomerReff.ToEntityReference();
                    target["ktb_customerdescription"] = CustomerReff.Name;

                    var ClassReff =
                        new Mapper<xts_customerclass>(Context.Service).Fetch(CustomerReff.xts_customerclassid.Id,
                        e => e.xts_dimension1id, e => e.xts_dimension2id, e => e.xts_dimension3id,
                        e => e.xts_dimension4id, e => e.xts_dimension5id, e => e.xts_dimension6id
                    );

                    if (ClassReff != null)
                    {
                        target["xts_receivabledimension1id"] = ClassReff.xts_dimension1id;
                        target["xts_receivabledimension2id"] = ClassReff.xts_dimension2id;
                        target["xts_receivabledimension3id"] = ClassReff.xts_dimension3id;
                        target["xts_receivabledimension4id"] = ClassReff.xts_dimension4id;
                        target["xts_receivabledimension5id"] = ClassReff.xts_dimension5id;
                        target["xts_receivabledimension6id"] = ClassReff.xts_dimension6id;
                    }
                }

                #endregion

                #region Cash And Bank
                Checkpoint = "03:CAB";
                var CAB = string.IsNullOrWhiteSpace(Ent.CashAndBankNo) ? null
                    : new Mapper<xts_cashandbank>(Context.Service)
                        .Fetch(Ent.CashAndBankNo, e => e.xts_accountnumber, 
                            e => e.xts_cashandbankdimension1id, e => e.xts_cashandbankdimension2id, e => e.xts_cashandbankdimension3id,
                            e => e.xts_cashandbankdimension4id, e => e.xts_cashandbankdimension5id, e => e.xts_cashandbankdimension6id
                        )?.First();

                if (CAB != null)
                {
                    target["xts_cashandbankid"] = CAB.ToEntityReference();
                    target["xts_cashandbankdimension1id"] = CAB.xts_cashandbankdimension1id;
                    target["xts_cashandbankdimension2id"] = CAB.xts_cashandbankdimension2id;
                    target["xts_cashandbankdimension3id"] = CAB.xts_cashandbankdimension3id;
                    target["xts_cashandbankdimension4id"] = CAB.xts_cashandbankdimension4id;
                    target["xts_cashandbankdimension5id"] = CAB.xts_cashandbankdimension5id;
                    target["xts_cashandbankdimension6id"] = CAB.xts_cashandbankdimension6id;
                }
                #endregion

                #endregion

                Checkpoint = "04";
                #region Populated
                var BUReff = new Mapper<BusinessUnit>(Context.Service)
                    .FetchIgnoreStateCode(Ent.DealerCode, e => e.Name,
                        e => e.ParentBusinessUnitId, e => e.xts_onbehalfteamid)?.First();

                target["xts_businessunitid"] = BUReff?.ToEntityReference();
                target["xts_parentbusinessunitid"] = BUReff?.ParentBusinessUnitId;
                target["ownerid"] = BUReff?.xts_onbehalfteamid;

                var cursReff = new Mapper<TransactionCurrency>(Context.Service)
                    .Fetch("IDR", e => e.ISOCurrencyCode, e => e.ExchangeRate)?.First();

                target["transactioncurrencyid"] = cursReff?.ToEntityReference();
                target["exchangerate"] = cursReff?.ExchangeRate;
                #endregion

                Checkpoint = "05";
                return target;
            }
            catch (Exception ex)
            {
                "Issue occurs. Details as follow\n".ConsoleWriteLine();
                ex.GetErrorMessage().ConsoleWriteLine();
                ("ErrorCode: {0x8170A" + Checkpoint + "}").ConsoleWriteLine();
                throw ex;
            }
        }
        
        private void CreateDetail()
        {
            ("Process: Attempt to create detail " + DataContext.Context).ConsoleWriteLine();

            #region FastExit
            if (Reference.Attributes.Contains("xts_arreceipttype")) if (Reference["xts_arreceipttype"] != null)
                if (((OptionSetValue)Reference["xts_arreceipttype"]).Value == 2) { //Cancellation
                "Bypass process detail creation.\nARReceipt for type cancellation will generated automatically.".ConsoleWriteLine();
                return;
            }
            
            #endregion
            
            try
            {
                Checkpoint = "On Detail Start";
                var ctr = 0;
                var reff = new xts_accountreceivablereceiptdetail();
                foreach (ARReceiptDetail detail in DataContext.Details)
                {
                    var dEnt = new Entity { LogicalName = xts_accountreceivablereceiptdetail.EntityLogicalName };
                    dEnt["xts_accountreceivablereceiptid"] = Reference.ToEntityReference();

                    #region LookUp
                    Checkpoint = "On Detail Lookup";

                    dEnt["xts_customerid"] = string.IsNullOrWhiteSpace(detail.CustomerNo) ? null
                        : new Mapper<Account>(Context.Service)
                            .Fetch(detail.CustomerNo, e => e.AccountNumber)?.First().ToEntityReference();

                    dEnt["xts_invoiceid"] = string.IsNullOrWhiteSpace(detail.InvoiceNo)? null
                            : new Mapper<xts_accountreceivableinvoice>(Context.Service)
                                .Fetch(detail.InvoiceNo, e => e.xts_accountreceivableinvoice1)?.First().ToEntityReference();

                    #region Orders
                    Checkpoint = "On Detail Orders";
                    var OrderObject =
                        !string.IsNullOrWhiteSpace(detail.SPKDetailNo) ? xts_newvehiclesalesorder.EntityLogicalName
                        : !string.IsNullOrWhiteSpace(detail.OrderNoSONo) ? xts_salesorder.EntityLogicalName
                        : !string.IsNullOrWhiteSpace(detail.OrderNoWONo) ? xts_workorder.EntityLogicalName
                        : !string.IsNullOrWhiteSpace(detail.OrderNoWriteOffBalanceNo) ? CoreEntities.xts_writeoffbalance.EntityLogicalName
                        : String.Empty;

                    if (!string.IsNullOrWhiteSpace(OrderObject))
                    {
                        Checkpoint = "OD Order:VSO";
                        var VSOReff = string.IsNullOrWhiteSpace(detail.SPKDetailNo) ? null
                            : new Mapper<xts_newvehiclesalesorder>(Context.Service).Fetch(detail.SPKDetailNo, e => e.xts_newvehiclesalesordernumber, e => e.xts_transactiondate)?.First();
                        dEnt["xts_ordernvsoid"] = VSOReff?.ToEntityReference();

                        Checkpoint = "OD Order:SO";
                        var SOReff = string.IsNullOrWhiteSpace(detail.OrderNoSONo) ? null
                            : new Mapper<xts_salesorder>(Context.Service).Fetch(detail.OrderNoSONo, e => e.xts_salesordernumber, e => e.xts_transactiondate)?.First();
                        dEnt["xts_ordersalesorderid"] = SOReff?.ToEntityReference();

                        Checkpoint = "OD Order:WO";
                        var WOReff = string.IsNullOrWhiteSpace(detail.OrderNoWONo) ? null
                            : new Mapper<xts_workorder>(Context.Service).Fetch(detail.OrderNoWONo, e => e.xts_workorder1, e => e.xts_transactiondate)?.First();
                        dEnt["xts_orderworkorderid"] = WOReff?.ToEntityReference();

                        Checkpoint = "OD Order:WOB";
                        var WOBReff = string.IsNullOrWhiteSpace(detail.OrderNoWriteOffBalanceNo) ? null
                            : new Mapper<CoreEntities.xts_writeoffbalance>(Context.Service).Fetch(detail.OrderNoWriteOffBalanceNo, e => e.xts_writeoffbalancenumber, e => e.xts_transactiondate)?.First();
                        dEnt["xts_orderwriteoffbalanceid"] = WOBReff?.ToEntityReference();

                        Checkpoint = "OD Order:Oty";
                        dEnt["xts_ordertypeid"] = string.IsNullOrWhiteSpace(detail.OrderTypeNo) ? null
                            : new Mapper<xts_ordertype>(Context.Service).Fetch(detail.OrderTypeNo, e => e.xts_ordertype1)?.First()?.ToEntityReference();

                        //SFL - might unused: UV scenario
                        //dEnt["xts_orderuvsoid"] = detail.OrderNoUVSONo?.Id().ToEntityReference(xts_usedvehiclesalesorder.EntityLogicalName);

                        Checkpoint = "OD Order:OLN";
                        dEnt["xts_orderlookupname"] = detail.SPKDetailNo ?? detail.OrderNoSONo ?? detail.OrderNoWONo ?? detail.OrderNoWriteOffBalanceNo;

                        Checkpoint = "OD Order:OLT";
                        dEnt["xts_orderlookuptype"] = new Mapper<xts_entitymetadata>(Context.Service)
                            .Fetch(OrderObject, e => e.xts_entityname, e => e.xts_objecttypecode)?.FirstOrDefault()?.xts_objecttypecode;

                        Checkpoint = "OD Order: OData";
                        dEnt["xts_orderdate"] = VSOReff?.xts_transactiondate ?? SOReff?.xts_transactiondate ?? WOReff?.xts_transactiondate ?? WOBReff?.xts_transactiondate;
                            //detail.OrderDate;
                    }
                    #endregion
                    #endregion

                    #region ASIS
                    Checkpoint = "On Detail ASIS";
                    dEnt["xts_paidbacktocustomer"] = detail.PaidBackToCustomer;
                    dEnt["xts_source"] = new OptionSetValue(detail.SourceType);
                    #endregion

                    #region Amt
                    Checkpoint = "On Detail Amt";

                    dEnt["xts_receiptamount"] = detail.ReceiptAmount.ToMoney();
                    dEnt["xts_outstandingbalance"] = detail.OutstandingBalance.ToMoney();
                    dEnt["xts_remainingbalance"] = detail.RemainingBalance.ToMoney();
                    #endregion

                    #region Populated
                    var BUReff = new Mapper<BusinessUnit>(Context.Service)
                        .FetchIgnoreStateCode(DataContext.DealerCode, e => e.Name,
                            e => e.ParentBusinessUnitId, e => e.xts_onbehalfteamid)?.First();

                    dEnt["xts_businessunitid"] = BUReff?.ToEntityReference();
                    dEnt["xts_parentbusinessunitid"] = BUReff?.ParentBusinessUnitId;
                    dEnt["ownerid"] = BUReff?.xts_onbehalfteamid;

                    var cursReff = new Mapper<TransactionCurrency>(Context.Service)
                        .Fetch("IDR", e => e.ISOCurrencyCode, e => e.ExchangeRate)?.First();

                    dEnt["transactioncurrencyid"] = cursReff?.ToEntityReference();
                    dEnt["exchangerate"] = cursReff?.ExchangeRate;
                    #endregion

                    Checkpoint = "On Detail Create Attempt";
                    Context.Service.Create(dEnt);
                    ++ctr;
                }

                string.Format("Process: {0} Detail created successfully", ctr.ToString()).ConsoleWriteLine();

            }
            catch (Exception ex)
            {
                "Issue occurs. Details as follow\n".ConsoleWriteLine();
                ex.GetErrorMessage().ConsoleWriteLine();
                throw ex;
            }
        }

        private void ReleaseHeader()
        {
            "Process: Attempt to Release header".ConsoleWriteLine();
            
            try
            {
                var ent = new Entity { LogicalName = xts_accountreceivablereceipt.EntityLogicalName };
                ent.Id = Reference.Id;
                ent["xts_handling"] = new OptionSetValue((int)xts_accountreceivablereceipt.Status.Released);
                Context.Service.Update(ent);
                "Process: Released successfully".ConsoleWriteLine();
            }
            catch (Exception ex)
            {
                "Issue occurs. Details as follow\n".ConsoleWriteLine();
                ex.GetErrorMessage().ConsoleWriteLine();
                throw ex;
            }
        }
    }
}
