﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using OP.MSCRM.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Technosoft.BSI.EntityClasses;
using Technosoft.BSI.EntityClasses.Entities;
using TS.Integration.Yana.Utility;
using TS.Integration.Yana.Utility.Custom;
using model = TS.Integration.Yana.Model;

namespace TS.Integration.Yana.Modules.Integrate
{
    public class Employee : BaseOperation<model.ExternalEntities.Employee>
    {
        model.ExternalEntities.Employee EMP;
        public Employee(BaseExecute e, model.ExternalEntities.Employee ent) : base(e, ent)
        {
            EMP = ent;
        }

        public override Entity PrepareData(model.ExternalEntities.Employee Ent)
        {
            return null;
        }

        public override void Execute()
        {
            Guid? employeeId = null;
            try
            {
                Checkpoint = "On Start";
                employeeId = UpsertEmployee();
                new ExternalDealerLog(Context, new EDILog<Entity>
                {
                    DealerIdentifier = EMP.DealerCode,
                    Context = EMP.Context,
                    ExternalCode = EMP.ExternalCode,
                    ReceivedMessage = Context.Messages,
                    Operation = (int)Operation,
                    Lookup = new Entity { LogicalName = "xts_employee", Id = (Guid)employeeId }
                });
            }
            catch (Exception ex)
            {
                if (Context.DebugMode) Checkpoint.ConsoleWriteLine();
                new ExternalDealerLog(Context, new EDILog<Entity>
                {
                    DealerIdentifier = EMP.DealerCode,
                    Context = EMP.Context,
                    ExternalCode = EMP.ExternalCode,
                    ReceivedMessage = Checkpoint + "\n" + Context.Messages,
                    ExceptionMessage = ex.GetErrorMessage(),
                    Operation = (int)Operation,
                    Lookup = employeeId == null ? null : new Entity { LogicalName = "xts_employee ", Id = (Guid)employeeId }
                });
            }
        }

        private Guid? UpsertEmployee()
        {
            Console.WriteLine("Process: Attempt to Create Employee");

            try
            {
                var employee = new Entity(xts_employee.EntityLogicalName);

                #region Taken ASIS
                Checkpoint = "On Header ASIS";
                Console.WriteLine(Checkpoint);
                var BUReff = new Mapper<BusinessUnit>(Context.Service)
                    .FetchIgnoreStateCode(EMP.DealerCode, e => e.Name, e => e.Id, e => e.ParentBusinessUnitId,
                    e => e.xts_onbehalfteamid, e => e.ktb_defaultteamforserviceid, e => e.ktb_defaultteamforsparepartid, e => e.ktb_defaultteaminterfacevehicleid)
                    ?.First();
                var PBUReff = BUReff?.ParentBusinessUnitId;
                //var OwnerReff = BUReff?.xts_onbehalfteamid;
                Console.WriteLine("checkpoint 2");
                employee["ktb_externalcode"] = EMP.ExternalCode;
                if (EMP.IdentificationType != 0) employee["ktb_identificationtype"] = new OptionSetValue((int)EMP.IdentificationType);
                if (EMP.WorkingStatus != 0) employee["xts_workingstatus"] = new OptionSetValue((int)EMP.WorkingStatus);
                employee["xts_address1"] = EMP.Address1;
                if (!EMP.BirthDate.IsMinValue()) employee["xts_birthdate"] = EMP.BirthDate.ConvertFromUTC(Context.Service);
                if (!string.IsNullOrEmpty(EMP.BirthPlace))
                {
                    var birthPlace = new Mapper<xts_city>(Context.Service).Fetch(EMP.BirthPlace, e => e.xts_city1, e => e.xts_cityId, e => e.xts_provinceid).First();
                    if (birthPlace != null) employee["ktb_birthplace"] = birthPlace.ToEntityReference();
                }
                employee["xts_businessunitid"] = BUReff.ToEntityReference();
                employee["ktb_id"] = EMP.DnetId;
                employee["xts_email"] = EMP.Email;
                employee["xts_employee"] = EMP.EmployeeName;
                employee["xts_firstname"] = EMP.FirstName;
                if (EMP.Gender != 0) employee["xts_gender"] = new OptionSetValue((int)EMP.Gender);
                employee["ktb_isinterfaced"] = false;
                employee["ktb_identificationno"] = EMP.IdentificationNo;
                if (!EMP.JoinDate.IsMinValue()) employee["xts_joindate"] = EMP.JoinDate.ConvertFromUTC(Context.Service);
                employee["xts_lastname"] = EMP.LastName;
                if (EMP.MaritalStatus != 0) employee["ktb_maritalstatus"] = new OptionSetValue((int)EMP.MaritalStatus);
                employee["xts_mobilephone"] = EMP.MobilePhone;
                employee["ktb_outsource"] = EMP.Outsource;
                if (EMP.Permanent != 0) employee["xts_permanent"] = new OptionSetValue((int)EMP.Permanent);
                if (EMP.Position != 0) employee["ktb_position"] = new OptionSetValue((int)EMP.Position);
                employee["xts_postalcode"] = EMP.PostalCode;
                employee["ktb_resignreason"] = EMP.ResignReason;
                Console.WriteLine("checkpoint 3");

                if (!EMP.ResignDate.IsMinValue())
                {
                    employee["xts_resigndate"] = EMP.ResignDate.ConvertFromUTC(Context.Service);
                    if (EMP.ResignReasonType != 0) employee["bsi_resignreason"] = new OptionSetValue((int)EMP.ResignReasonType);
                    if (EMP.ResignStatus != 0) employee["bsi_resignstatusdnet"] = new OptionSetValue((int)EMP.ResignStatus);
                    if (EMP.ResignType != 0) employee["bsi_resigntype"] = new OptionSetValue((int)EMP.ResignType);
                }
                var cityReff = new Mapper<xts_city>(Context.Service).Fetch(EMP.City, e => e.xts_city1, e => e.xts_cityId, e => e.xts_provinceid).First();
                var provinceReff2 = new Mapper<xts_province>(Context.Service).Fetch(Convert.ToString(cityReff.xts_provinceid.Id), e => e.xts_provinceId, e => e.xts_countryid).First();
                var provinceReff = new Mapper<xts_province>(Context.Service).Fetch(EMP.Province, e => e.xts_province1, e => e.xts_provinceId, e => e.xts_countryid).First();

                employee["xts_cityid"] = cityReff != null ? cityReff.ToEntityReference() : null;
                employee["xts_provinceid"] = provinceReff != null ? provinceReff.ToEntityReference() : cityReff != null ? cityReff.xts_provinceid : null;
                employee["xts_countryid"] = provinceReff != null ? provinceReff.xts_countryid : provinceReff2 != null ? provinceReff2.xts_countryid : null;
                Console.WriteLine("checkpoint 4");
                switch (EMP.IdentificationType)
                {
                    case (int)xts_employee.IdentificationType.Service:
                    case (int)xts_employee.IdentificationType.BodyPaint:
                        Console.WriteLine("checkpoint service");
                        if (EMP.Category != 0) employee["ktb_category"] = new OptionSetValue((int)EMP.Category);
                        if (EMP.Education != 0) employee["ktb_education"] = new OptionSetValue((int)EMP.Education);
                        if (EMP.KategoriTeam != 0) employee["ktb_kategoriteam"] = new OptionSetValue((int)EMP.KategoriTeam);
                        if (EMP.Religion != 0) employee["ktb_religion"] = new OptionSetValue((int)EMP.Religion);
                        if (EMP.UkuranBaju != 0) employee["ktb_ukuranbaju"] = new OptionSetValue((int)EMP.UkuranBaju);
                        employee["ownerid"] = BUReff?.ktb_defaultteamforserviceid;
                        if (!string.IsNullOrEmpty(EMP.JobPosition))
                        {
                            var JobPosition = new Mapper<ktb_jobposition>(Context.Service)
                                    .Fetch(EMP.JobPosition, e => e.ktb_jobposition1, e => e.ktb_position1).First();
                            if (JobPosition != null)
                            {
                                employee["ktb_jobpositionid"] = JobPosition.ToEntityReference();
                            }
                        }
                        break;

                    case (int)xts_employee.IdentificationType.Vehicle:
                        Console.WriteLine("checkpoint sales/vehicle 1");
                        if (EMP.Category != 0) employee["ktb_category"] = new OptionSetValue((int)EMP.Category);
                        if (EMP.Education != 0) employee["ktb_education"] = new OptionSetValue((int)EMP.Education);
                        if (EMP.KategoriTeam != 0) employee["ktb_kategoriteam"] = new OptionSetValue((int)EMP.KategoriTeam);
                        if (EMP.Religion != 0) employee["ktb_religion"] = new OptionSetValue((int)EMP.Religion);
                        if (EMP.SalesmanArea != 0) employee["ktb_salesmanarea"] = new OptionSetValue((int)EMP.SalesmanArea);
                        if (EMP.SalesmanLevel != 0) employee["ktb_salesmanlevel"] = new OptionSetValue((int)EMP.SalesmanLevel);
                        employee["ktb_salesmancodereff"] = EMP.SalesmanCodeReff;
                        if (EMP.Type != 0) employee["xts_employeeclasstype"] = new OptionSetValue((int)EMP.Type);
                        if (EMP.UkuranBaju != 0) employee["ktb_ukuranbaju"] = new OptionSetValue((int)EMP.UkuranBaju);
                        Console.WriteLine("checkpoint sales/vehicle 2");
                        employee["ownerid"] = BUReff?.ktb_defaultteaminterfacevehicleid;
                        Console.WriteLine("checkpoint sales/vehicle 3");
                        if (!string.IsNullOrEmpty(EMP.KodeAtasan))
                        {
                            var ParentEmpReff = new Mapper<xts_employee>(Context.Service)
                                    .Fetch(EMP.KodeAtasan, e => e.ktb_employeecode, e => e.xts_employee1).First();
                            Console.WriteLine("Atasan: " + ParentEmpReff.xts_employee1);
                            if (ParentEmpReff != null)
                            {
                                employee["xts_parentemployeeid"] = ParentEmpReff.ToEntityReference(); //ParentEmpReff != null ? ParentEmpReff.ToEntityReference() : null;
                                employee["ktb_superiors"] = ParentEmpReff.ToEntityReference(); //ParentEmpReff != null ? ParentEmpReff.ToEntityReference() : null;
                                employee["ktb_superiorscode"] = ParentEmpReff.ktb_employeecode; //ParentEmpReff != null ? ParentEmpReff.ktb_employeecode : ""; //EMP.KodeAtasan;
                            }
                        }
                        break;

                    case (int)xts_employee.IdentificationType.Sparepart:
                        Console.WriteLine("checkpoint sparepart");
                        if (EMP.Category != 0) employee["ktb_category"] = new OptionSetValue((int)EMP.Category);
                        if (EMP.Education != 0) employee["ktb_education"] = new OptionSetValue((int)EMP.Education);
                        if (EMP.KategoriTeam != 0) employee["ktb_kategoriteam"] = new OptionSetValue((int)EMP.KategoriTeam);
                        if (EMP.Religion != 0) employee["ktb_religion"] = new OptionSetValue((int)EMP.Religion);
                        if (EMP.UkuranBaju != 0) employee["ktb_ukuranbaju"] = new OptionSetValue((int)EMP.UkuranBaju);
                        employee["ownerid"] = BUReff?.ktb_defaultteamforsparepartid;
                        break;

                        //default:
                        //    employee["ktb_category"] = new OptionSetValue((int)EMP.Category);
                        //    if (EMP.Education != 0) employee["ktb_education"] = new OptionSetValue((int)EMP.Education);
                        //    if (EMP.KategoriTeam != 0) employee["ktb_kategoriteam"] = new OptionSetValue((int)EMP.KategoriTeam);
                        //    if (EMP.Religion != 0) employee["ktb_religion"] = new OptionSetValue((int)EMP.Religion);
                        //    if (EMP.UkuranBaju != 0) employee["ktb_ukuranbaju"] = new OptionSetValue((int)EMP.UkuranBaju);
                        //    employee["ownerid"] = BUReff?.xts_onbehalfteamid;
                        //    break;
                }

                #endregion

                Checkpoint = "On Header Upsert";
                Console.WriteLine(Checkpoint);
                Guid? headerId = null;
                //Get Reference
                var Reference = new Mapper<xts_employee>(Context.Service).Fetch(EMP.ExternalCode, "ktb_externalcode")?.Id;
                if (Reference == null)
                {
                    Console.WriteLine("Create new employee");
                    Checkpoint = "On Header Sert Upsert";
                    Operation = OperationTask.Create;
                    headerId = Context.Service.Create(employee);
                }
                else if (Reference != null)
                {
                    Console.WriteLine("Data With current External Code Found:  " + Reference.ToString());
                    Console.WriteLine("Process: Attempt to Update Employee");

                    Checkpoint = "On Header Up Upsert";
                    Operation = OperationTask.Update;
                    headerId = employee.Id = (Guid)Reference;
                    Context.Service.Update(employee);
                }

                Console.WriteLine("Process: Employee success.");
                Console.WriteLine("Id: " + headerId);
                return headerId;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Issue occurs, details as follow.\n" + ex.GetErrorMessage());
                throw ex;
            }
        }
    }
}