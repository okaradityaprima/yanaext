﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using TS.Integration.Yana.Model.ExternalEntities;
using TS.Integration.Yana.Model.YanaEntities;
using TS.Integration.Yana.Utility;

namespace TS.Integration.Yana.Modules.Testing
{
    public class IntegratePartApprovalCreditLimit : BaseOperation<PartApprovalCreditLimit>
    {
        public IntegratePartApprovalCreditLimit(BaseExecute e, PartApprovalCreditLimit te) : base(e, te)
        {
        }
        
        public override Entity PrepareData(PartApprovalCreditLimit Ent)
        {
            var regarding = new Entity();
            regarding.LogicalName = Ent.Context;
            regarding["ktb_partapprovalcreditlimitno"] = Ent.DealerCode;

            return regarding;
        }
    }
}
