﻿//using System;
//using System.Net;
//using System.ServiceModel.Description;
//using Microsoft.Crm.Sdk.Messages;
//using Microsoft.ServiceBus.Messaging;
//using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Client;
//using Newtonsoft.Json;

//namespace TS.Integration.Yana.Srikandi
//{
//    using Model.YanaEntities;
//    using Utility;

//    class Sample
//    { 

//        static void Mains(string[] args)
//        {
//            return;
//            Extensions.Utility.Extensions.ConsoleWriteLine("Awaiting message..");
//            GetMessage();

//            Utility.Extensions.ConsoleWriteLine("Done.");
//            Console.ReadLine();
//        }

//        static Lead LeadReff = null;
//        static void GetMessage()
//        {
//            Utility.Extensions.ConsoleWriteLine("Receiving message process start.");
//            const string connectionString = "Endpoint=sb://srikandi.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=b0XvwxK2kcS0MzYxel7zGkoqDkzGHSr30ar30MJFM00=";
//            const string queueName = "srikandiyanamap";
//            var queueClient = QueueClient.CreateFromConnectionString(connectionString, queueName);

//            Utility.Extensions.ConsoleWriteLine("Receiving Message..");
//            BrokeredMessage message = queueClient.Receive();
//            Utility.Extensions.ConsoleWriteLine("Message received. Deserializing...");
//            Utility.Extensions.ConsoleWriteLine("Message:\n" + message);
//            LeadReff = JsonConvert.DeserializeObject<Lead>(message.GetBody<string>());
//            Utility.Extensions.ConsoleWriteLine("Deserialized.");

//            Utility.Extensions.ConsoleWriteLine("Received Packaged as follows.");
//            Utility.Extensions.ConsoleWriteLine("Topic: " + LeadReff.Subject);
//            Utility.Extensions.ConsoleWriteLine("firstname: " + LeadReff.FirstName);
//            Utility.Extensions.ConsoleWriteLine("Lastname: " + LeadReff.LastName);

//            Utility.Extensions.ConsoleWriteLine("Receiving Message process complete.");
//            if (CreateLead())
//            {
//                message.Complete();
//                message.Abandon();
//            }

//        }

//        public static int _operation;
//        public static Guid _guid;
//        public static IOrganizationService Service;

//        static bool CreateLead()
//        {
//            try
//            {
//                var envi = "ktb-uat";
//                Utility.Extensions.ConsoleWriteLine("Attempt create Lead to " + envi + ".");

//                prepareConnection(out Service, envi);
//                if (Service == null) return false;

//                var reff = new Entity
//                {
//                    LogicalName = "lead"
//                };

//                reff.Attributes["firstname"] = LeadReff.FirstName;
//                reff.Attributes["lastname"] = LeadReff.LastName;
//                reff.Attributes["subject"] = LeadReff.Subject;

//                var id = Service.Create(reff);
//                Utility.Extensions.ConsoleWriteLine("Created successfully with id: " + id.ToString());
//                return true;
//            }
//            catch (Exception ex)
//            {
//                Utility.Extensions.ConsoleWriteLine("Process Failed");
//                Utility.Extensions.ConsoleWriteLine(ex.Message);

//                return false;
//            }
//        }

//        #region Utility
//        static void prepareConnection(out IOrganizationService organizationService, string envi = null)
//        {
//            organizationService = null;
//            if (envi == null) return;
//            try
//            {
//                ClientCredentials clientCredentials = new ClientCredentials();
//                clientCredentials.UserName.UserName = "connect.user@ktbmmksidms.onmicrosoft.com";
//                clientCredentials.UserName.Password = "Mmksi@2018";

//                // For Dynamics 365 Customer Engagement V9.X, set Security Protocol as TLS12
//                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
//                // Get the URL from CRM, Navigate to Settings -> Customizations -> Developer Resources
//                // Copy and Paste Organization Service Endpoint Address URL
//                organizationService = (IOrganizationService)new OrganizationServiceProxy(
//                    //new Uri("https://<ProvideYourOrgName>.api.<CRMRegion>.dynamics.com/XRMServices/2011/Organization.svc"
//                    new Uri("https://" + envi + ".api.crm5.dynamics.com/XRMServices/2011/Organization.svc"),
//                    null,
//                    clientCredentials,
//                    null
//                );

//                if (organizationService != null)
//                {
//                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;



//                    if (userid != Guid.Empty)
//                    {
//                        Utility.Extensions.ConsoleWriteLine("Connection Established Successfully...");
//                    }
//                }
//                else
//                {
//                    Utility.Extensions.ConsoleWriteLine("Failed to Established Connection!!!");
//                }
//            }
//            catch (Exception ex)
//            {
//                Utility.Extensions.ConsoleWriteLine("Exception caught - " + ex.Message);
//            }
//        }
//        #endregion
//    }
//}