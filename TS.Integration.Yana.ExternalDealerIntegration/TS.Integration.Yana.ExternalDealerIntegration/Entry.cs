﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TS.Integration.Yana.Utility;
using TS.Integration.Yana.Utility.Custom;

namespace TS.Integration.Yana.ExternalDealerIntegration
{
    using Microsoft.ServiceBus.Messaging;
    using Newtonsoft.Json;
    using TS.Integration.Yana.Model.YanaEntities;
    using TS.Integration.Yana.Model.ExternalEntities;
    using TS.Integration.Yana.Modules;
    using Utility;

    class Entry
    {
        static void Main(string[] args)
        {
            new Listening().Execute();
        }
    }

    public class Listening : BaseExecute
    {
        private BrokeredMessage Package;
        private bool UnknownContext = false, OnRetry, PackageProcessed = true;
        private string CurrentMessage, ExceptionMsg;

        public Listening() { }

        public void Execute()
        {

            do
            {
                OnRetry = false;
                try
                {
                    #region #sfl#01
                    //var ctr = 0;
                    //while (!Queue.ClientReady)
                    //{
                    //    if (ctr != 0) { Utility.Extensions.ConsoleWriteLine("Currently queue empty, retrieving.."); }
                    //    if (ctr++ > RetryCount) {
                    //        Utility.Extensions.ConsoleWriteLine("Retry count exceeded.");
                    //        break;
                    //    }
                    //    Thread.Sleep(Retry);
                    //    Configure();
                    //}
                    #endregion

                    var ctr = 0;
                    do
                    {
                        PrepareQueue();
                        if (ctr != 0)
                        {
                            Extensions.ConsoleWriteLine("Currently queue empty, retrieving..");
                            OnRetry = true;
                        }
                        if (ctr++ > RetryCount) {
                            //Utility.Extensions.ConsoleWriteLine("");
                            break;
                        }
                        Thread.Sleep(Retry);
                    } while (!Queue.ClientReady);
                    ctr = 0;

                    if (!OnRetry) if (DebugMode) Extensions.ConsoleWriteLine("Fetching queue..");
                    Package = PackageProcessed ? Queue.GetMessage() : Package;
                    if (Package != null)
                    {
                        OnRetry = false;
                        var message = CurrentMessage =  Package?.GetBody<string>();
                        if (DebugMode) Extensions.ConsoleWriteLine("Message received. Deserializing..");
                        switch (message.GetContext())
                        {
                            case xts_accountpayablepayment.EntityLogicalName: //APPayment
                                break;

                            case xts_newvehiclewholesaleorder.EntityLogicalName:
                                //new Modules.Integrate.SPK();
                                break;

                            case xts_newvehiclesalesorder.EntityLogicalName:
                                break;


                            //Working Samples
                            case ktb_partapprovalcreditlimit.EntityLogicalName:
                                var Part = JsonConvert.DeserializeObject<PartApprovalCreditLimit>(message);
                                Extensions.ConsoleWriteLine("Processing package:");
                                Extensions.ConsoleWriteLine("Key: " + Part.DealerCode);
                                Extensions.ConsoleWriteLine("Context:" + Part.Context);
                                new Modules.Testing.IntegratePartApprovalCreditLimit(this, Part).Execute();
                                break;

                            //Unknown Or unhandled context
                            case Lead.EntityLogicalName:
                            default:
                                UnknownContext = true;
                                break;
                        }

                        if (UnknownContext)
                        {
                            Extensions.ConsoleWriteLine("Unknown Context.");
                            Extensions.ConsoleWriteLine("Sending package to deadletter.");
                            Package.DeadLetter();
                            UnknownContext = false;
                        }
                        else
                        {
                            Package.Complete();
                            Package.Abandon();
                            PackageProcessed = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionMsg = "Exception: " + ex.InnerException?.Message ?? ex.Message;
                    if (ExceptionMsg.ContainsAny("SYS", "JPN", "EXT", "KTB"))
                    {
                        PackageProcessed = false;
                        Extensions.ConsoleWriteLine(ExceptionMsg);

                        var a = Package.SequenceNumber;
                        //new ExternalDealerLog(ExceptionMsg);

                    }
                    else
                    {
                        Extensions.ConsoleWriteLine(ExceptionMsg);
                        //new ExternalDealerLog(ExceptionMsg);

                        Extensions.ConsoleWriteLine("Sending package to deadletter.");
                        Package.DeadLetter();
                        PackageProcessed = true;
                    }
                }
                finally
                {
                    Extensions.ConsoleWriteLine(
                        !PackageProcessed
                        ? String.Format("Retrying due to {0}..", ExceptionMsg?.Substring(0, ExceptionMsg.Length > 30 ? 30 : ExceptionMsg.Length - 1))
                        : OnRetry ? "Retry count exceeded. Restarting.." : "Message processed.");
                    Extensions.ConsoleWriteLine("============================================");
                    //Console.Clear();
                    Thread.Sleep(OnRetry ? 60000 : 2000);
                }
            }
            while (true);
        }
    }
}
