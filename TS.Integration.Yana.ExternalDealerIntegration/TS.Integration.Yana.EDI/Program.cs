﻿using System;
using System.Threading;

namespace TS.Integration.Yana.EDI
{
    using Microsoft.ServiceBus.Messaging;
    using Newtonsoft.Json;
    using TS.Integration.Yana.Model.YanaEntities;
    using TS.Integration.Yana.Model.ExternalEntities;
    using TS.Integration.Yana.Modules;
    using Utility;
    using TS.Integration.Yana.Utility.Custom;
    using System.Collections.Generic;

    class Program
    {
        static void Main(string[] args)
        {
            new Listening().Execute();
        }
    }

    public class Listening : BaseExecute
    {
        private BrokeredMessage Package;
        private bool UnknownContext = false, OnRetry, PackageProcessed = true;
        private string CurrentMessage, ExceptionMsg;

        public Listening() { }

        public void Execute()
        {

            do
            {
                OnRetry = false;
                try
                {
                    #region #sfl#01
                    //var ctr = 0;
                    //while (!Queue.ClientReady)
                    //{
                    //    if (ctr != 0) { Utility.Extensions.ConsoleWriteLine("Currently queue empty, retrieving.."); }
                    //    if (ctr++ > RetryCount) {
                    //        Utility.Extensions.ConsoleWriteLine("Retry count exceeded.");
                    //        break;
                    //    }
                    //    Thread.Sleep(Retry);
                    //    Configure();
                    //}
                    #endregion

                    var ctr = 0;
                    do
                    {
                        PrepareQueue();
                        if (ctr != 0) {
                            if (DebugMode) "Currently queue empty, retrieving..".ConsoleWriteLine();
                            OnRetry = true;
                        }

                        if (ctr++ > RetryCount) break;
                        Thread.Sleep(Retry);
                    }
                    while (!Queue.ClientReady);
                    
                    ctr = 0;
                    if (!OnRetry) if (DebugMode) "Fetching queue..".ConsoleWriteLine();
                    Package = PackageProcessed ? Queue.GetMessage() : Package;
                    if (Package != null)
                    {
                        OnRetry = false;
                        Messages = CurrentMessage = Package?.GetBody<string>();
                        "============================================".ConsoleWriteLine();
                        //string.Format("Message received.{0}\nDeserializing..", DebugMode ? "\n" + Messages : "").ConsoleWriteLine();
                        string.Format("Message received.{0}\nDeserializing..", "\n" + Messages.Minify()).ConsoleWriteLine();

                        var context = Messages?.GetContext();
                        Extensions.ConsoleWriteLine(context);
                        switch (context)
                        {
                            case xts_accountpayablepayment.EntityLogicalName: //APPayment
                                var APPayment = JsonConvert.DeserializeObject<APPayment>(Messages);
                                new Modules.Integrate.APPayment(this, APPayment).Execute();
                                break;

                            case xts_accountreceivablereceipt.EntityLogicalName: //APPayment
                                var receipt = JsonConvert.DeserializeObject<ARReceipt>(Messages);
                                new Modules.Integrate.ARReceipt(this, receipt).Execute();
                                break;

                            case xts_newvehiclewholesaleorder.EntityLogicalName:
                                var spk = JsonConvert.DeserializeObject<SPK>(Messages);
                                new Modules.Integrate.SPK(this, spk).Execute();
                                break;

                            case xts_newvehiclesalesorder.EntityLogicalName:
                                break;

                            case xts_employee.EntityLogicalName:
                                var errors = new List<string>();
                                var emp = JsonConvert.DeserializeObject<Employee>(Messages,
                                new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Include,
                                    Error = delegate (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs earg)
                                    {
                                        errors.Add(earg.ErrorContext.Member.ToString());
                                        earg.ErrorContext.Handled = true;
                                    }
                                });
                                Extensions.ConsoleWriteLine("Processing employee package:");
                                Extensions.ConsoleWriteLine("Dealer Code Key: " + emp.DealerCode + ". Employee Code Key: " + emp.ExternalCode);
                                Extensions.ConsoleWriteLine("Context:" + emp.Context);
                                new Modules.Integrate.Employee(this, emp).Execute();
                                break;



                            //Working Samples
                            case ktb_partapprovalcreditlimit.EntityLogicalName:
                                var Part = JsonConvert.DeserializeObject<PartApprovalCreditLimit>(Messages);
                                Extensions.ConsoleWriteLine("Processing package:");
                                Extensions.ConsoleWriteLine("Key: " + Part.DealerCode);
                                Extensions.ConsoleWriteLine("Context:" + Part.Context);
                                new Modules.Testing.IntegratePartApprovalCreditLimit(this, Part).Execute();
                                break;

                            //Unknown Or unhandled context
                            case Lead.EntityLogicalName:
                            default:
                                UnknownContext = true;
                                break;
                        }

                        if (UnknownContext)
                        {
                            "Unknown Context.\nSending package to deadletter.".ConsoleWriteLine();
                            Package.DeadLetter();
                            UnknownContext = false;
                        }
                        else
                        {
                            Package.Complete();
                            Package.Abandon();
                            PackageProcessed = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionMsg = "Exception: " + (string.IsNullOrWhiteSpace(ex.InnerException?.Message) ? ex.Message : ex.InnerException?.Message);
                    if (ExceptionMsg.ContainsAny("SYS", "JPN", "EXT", "KTB"))
                    {
                        PackageProcessed = false;
                        ExceptionMsg.ConsoleWriteLine();

                        var a = Package.SequenceNumber;
                        //new ExternalDealerLog(ExceptionMsg);

                    }
                    else
                    {
                        Extensions.ConsoleWriteLine(ExceptionMsg);
                        //new ExternalDealerLog(ExceptionMsg);

                        "Sending package to deadletter.".ConsoleWriteLine();
                        Package.DeadLetter();
                        PackageProcessed = true;
                    }
                }
                finally
                {
                    if (DebugMode)
                    {
                        Extensions.ConsoleWriteLine(
                            !PackageProcessed
                            ? String.Format("Retrying due to {0}..", ExceptionMsg?.Substring(0, ExceptionMsg.Length > 30 ? 30 : ExceptionMsg.Length - 1))
                            : OnRetry ? "Retry count exceeded. Restarting.." : "Message processed.");
                        Extensions.ConsoleWriteLine("============================================");
                    }
                    //Console.Clear();
                    Thread.Sleep(OnRetry ? 10000 : 2000);
                }
            }
            while (true);
        }
    }
}
