﻿namespace TS.Integration.Yana.SPK
{
    using System;
    using Utility;
    using Model;
    using Model.ExternalEntities;
    using Technosoft.BSI.EntityClasses.Entities;
    using Microsoft.Xrm.Sdk;

    public class IntegrateSPK : BaseOperation<SPK>
    {
        public IntegrateSPK(BaseExecute context, SPK dt) : base(context, dt) {
        }

        public override Entity PrepareData(SPK data)
        {
            var regarding = new xts_newvehiclewholesaleorder();
            regarding.xts_newvehiclewholesaleordernumber = data.ExternalCode;

            return regarding.ToEntity<Entity>();
        }
    }
}
