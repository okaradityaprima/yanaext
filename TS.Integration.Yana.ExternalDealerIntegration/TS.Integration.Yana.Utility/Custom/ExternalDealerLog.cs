﻿using Microsoft.Xrm.Sdk;
using OP.MSCRM.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Technosoft.BSI.EntityClasses.Entities;

namespace TS.Integration.Yana.Utility.Custom
{
    public class ExternalDealerLog
    {
        public ExternalDealerLog(BaseExecute Context, EDILog<Entity> regarding)
        {
            Console.WriteLine("Creating EDILog.");

            var Ent = new Entity { LogicalName = "ktb_externaldealerinterfacelog" };
            Ent["ktb_pf"] = 
                DateTime.Now.Year.ToString().Substring(DateTime.Now.Year.ToString().Length - 2) 
                + DateTime.Now.Month.ToString() 
                + DateTime.Now.Day.ToString()
                + "-" 
                + DateTime.Now.Millisecond.ToString()
                + ": " +  (string.IsNullOrWhiteSpace(regarding.ExternalCode) ? "NA" : regarding.ExternalCode);

            Ent["ktb_context"] = regarding.Context;
            Ent["ktb_operation"] = regarding.Operation != null ? new OptionSetValue((int)regarding.Operation) : new OptionSetValue((int)Operation.Configuration);
            Ent["ktb_receivedmessage"] = regarding.ReceivedMessage;
            Ent["ktb_exceptionmessage"] = regarding.ExceptionMessage;
            Ent["ktb_dealeridentifier"] = regarding.DealerIdentifier;
            Ent["ktb_externalcode"] = regarding.ExternalCode;
            Ent["ktb_businessunitid"] =
                new Mapper<BusinessUnit>(Context.Service).FetchIgnoreStateCode(regarding.DealerIdentifier, e => e.Name)?.First()?.ToEntityReference();

            var Lookup = regarding.GetLookupTarget();
            if (regarding.Lookup != null) if(Lookup != null) Ent[Lookup] = new EntityReference(regarding.Context, regarding.Lookup.Id);

            Context.Service.Create(Ent);
        }

        public ExternalDealerLog(BaseExecute Context, string Regarding, string ExceptionMessage)
        {
            var Ent = new Entity { LogicalName = "ktb_externaldealerinterfacelog" };
            Ent["ktb_context"] = Regarding;
            Ent["ktb_exceptionmessage"] = ExceptionMessage;
            Context.Service.Create(Ent);
        }
    }
    
    public enum Operation
    {
        Create = 1,
        Update,
        Read,
        Delete,
        Configuration
    }

    public class EDILog<T>
        where T : Entity
    {
        public string DealerIdentifier { get; set; }

        public string Context { get; set; }

        public string ExternalCode { get; set; }

        public T Lookup { get; set; }

        public string ReceivedMessage { get; set; }
        public string ExceptionMessage { get; set; }

        public int? Operation { get; set; }

        public String GetLookupTarget()
        {
            switch (Context.ToLower())
            {
                case "xts_accountpayablepayment": return "ktb_appaymentid";
                case "xts_accountreceivableinvoice": return "ktb_arinvoiceid";
                case "xts_accountreceivablereceipt": return "ktb_arreceiptid";
                case "xts_employee": return "ktb_employeeid";
                case "xts_newvehiclewholesaleorder": return "ktb_spkid";
            }

            return null;
        }
    }

    
}
