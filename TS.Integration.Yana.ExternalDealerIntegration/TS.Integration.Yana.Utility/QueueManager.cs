﻿using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;
using System;

namespace TS.Integration.Yana.Utility
{
    using TS.Integration.Yana.Model;
    using TS.Integration.Yana.Utility.Interfaces;

    public class QueueManager
    {
        public bool ClientReady = false;
        public QueueClient Client;

        private ConfigSetup Config;

        public QueueManager(ConfigSetup config)
        {
            Config = config;
            Prepare();
        }

        public void Prepare()
        {
            try
            {
                Client = QueueClient.CreateFromConnectionString(Config.BusConnectionString, Config.QueueName);
                ClientReady = Client.Peek() != null;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.InnerException?.Message ?? ex.Message);
            }
        }

        public BrokeredMessage Peek() => ClientReady ? Client.Peek() : null;
        public BrokeredMessage GetMessage() => ClientReady ? Client.Receive() : null;
    }
}
