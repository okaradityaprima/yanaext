﻿namespace TS.Integration.Yana.Utility
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using Model;
    using Newtonsoft.Json;
    using OP.MSCRM.Wrapper;
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text.RegularExpressions;

    public static class Extensions
    {
        #region String
        public static string GetContext(this string messages)
            => JsonConvert.DeserializeObject<BaseEntity>(messages).Context;
        //{
        //    var obj = JsonConvert.DeserializeObject<BaseEntity>(messages);
        //    return obj?.Context;
        //}

        public static string GetErrorMessage(this Exception ex)
        {
            return (string.IsNullOrWhiteSpace(ex.InnerException?.Message) ? ex.Message : ex.InnerException?.Message);
        }

        public static string Minify(this string m)
        {
            return Regex.Replace(m, "(\"(?:[^\"\\\\]|\\\\.)*\")|\\s+", "$1");
        }
        #endregion

        #region Bool
        public static bool ContainsAny(this string line, params string[] regarding)
        {
            foreach (var str in regarding)
            {
                if (line.Contains(str)) return true;
            }

            return false;
        }

        public static bool IsMinValue(this DateTime dt)
        {
            if (dt <= DateTime.MinValue) return true; //new DateTime(1753, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);
            else return false; //return dt;
        }
        #endregion

        #region Xrm.Sdk
        public static Guid Id(this string id) { return new Guid(id); }
        
        public static EntityReference ToEntityReference(this Guid id, string LogicalName) { return new EntityReference(LogicalName, id); }

        public static Money ToMoney(this decimal val) { return new Money(val); }
        public static Money ToMoney(this decimal? val) { return new Money(val ?? 0); }
        public static OptionSetValue ToOptionsetValue(this int? val) { if (val == null) return null; else return new OptionSetValue((int)val); }
        
        #endregion
        
        #region Date
        private static double RetrieveCurrentUsersSettings(IOrganizationService service)
        {
            var currentUserSettings = service.RetrieveMultiple(
                new QueryExpression("usersettings")
                {
                    ColumnSet = new ColumnSet("timezonebias"),
                    Criteria = new FilterExpression
                    {
                        Conditions =
                        {
                            new ConditionExpression("systemuserid", ConditionOperator.EqualUserId)
                        }
                    }
                }).Entities[0].ToEntity<Entity>();

            if (currentUserSettings == null) return 0;
            if (!currentUserSettings.Contains("timezonebias")) return 0;

            return -1 * ((int?)currentUserSettings.Attributes["timezonebias"]) / 60 ?? 0;
        }

        public static DateTime ConvertFromUTC(this DateTime dt, IOrganizationService service)
        {
            return dt.AddHours(RetrieveCurrentUsersSettings(service));
        }
        #endregion

        #region Etc
        public static string ConsoleWriteLine(this string line) {
            Console.WriteLine(
                //DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " >> " + 
                line);

            return line;
        }
        #endregion
    }
}
