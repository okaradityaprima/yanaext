﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace TS.Integration.Yana.Utility
{
    public class ConfigSetup
    {
        #region Const
        protected const string _busConnectionString = "Endpoint=sb://srikandi.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=b0XvwxK2kcS0MzYxel7zGkoqDkzGHSr30ar30MJFM00=";
        protected const string _queueName = "srikandiyanamap";
        protected const string _testingTargetEnvironment = "ktb-qa";
        protected const string _storageContainerName = "ts-integration-setting";
        protected const string _storageFolder = "";
        protected const string _configFileName = "TSIntegrationConfig.json";
        #endregion

        #region DataMember
        public string TargetEnvironment { get; internal set; }
        public string QueueName { get; internal set; }
        public string BusConnectionString { get; internal set; }
        public DMSConfig DMSConnectionConfig { get; set; }
        internal string DMSConnectionString { get; set; }
        #endregion

        public ConfigSetup()
        {
            //Get From Adapter ?? File?? const
            var Config = GetFromAdapter() ?? GetFromFile();
            TargetEnvironment = Config?.TargetEnvironment ?? _testingTargetEnvironment;
            QueueName = Config?.QueueName ?? _queueName;
            BusConnectionString = Config?.BusConnectionString ?? _busConnectionString;
            DMSConnectionString = Config?.DMSConnectionString;

            GetDMSConfigFromConnectionString(DMSConnectionString.Split(';').ToList());
        }

        private void GetDMSConfigFromConnectionString(List<string> cs)
        {
            string 
                url = string.Empty, 
                uname = string.Empty, 
                pw = string.Empty, 
                atp = string.Empty;
            
            foreach (string data in cs)
            {
                if (data.ToLower().Contains("url"))
                    url = GetConnectionData(data);

                if (data.ToLower().Contains("username"))
                    uname = GetConnectionData(data);

                if (data.ToLower().Contains("password"))
                    pw = GetConnectionData(data);

                if (data.ToLower().Contains("authtype"))
                    atp = GetConnectionData(data);
            }

            DMSConnectionConfig = new DMSConfig {
                Url = url,
                Username = uname,
                Password = pw,
                AuthType = atp,
            };
        }
        private string GetConnectionData(string st)
        {
            return st.Substring(st.IndexOf("=")).Replace("=", "").Replace(";", "");
        }

        #region Helper
        internal ConnectionConfig GetFromFile() => SetConfigFromFile();
        internal ConnectionConfig GetFromAdapter() => SetConfigFromAdapter();

        internal class ConnectionConfig
        {
            internal string TargetEnvironment { get; set; }
            internal string QueueName { get; set; }
            internal string BusConnectionString { get; set; }
            internal string DMSConnectionString { get; set; }
        }
        
        public class DMSConfig
        {
            public string Url;
            public string Username;
            public string Password;
            public string AuthType;
        }
        #endregion

        #region GetConfig
        private ConnectionConfig SetConfigFromAdapter()
        {
            //return null;

            var setting = ConfigurationManager.AppSettings;
            var TargetEnvironment = setting["TS.Integration.EDI.TargetEnvironment"];
            if (TargetEnvironment == null) { throw new ArgumentException("TargetEnvironment not configured."); }

            var QueueName = setting["TS.Integration.EDI.QueueName"];
            if (QueueName == null) { throw new ArgumentException("QueueName not configured."); }

            var BCS = setting["TS.Integration.EDI.BusConnectionString"];
            if (BCS == null) { throw new ArgumentException("Bus config not configured."); }

            var DCS = setting["TS.Integration.EDI.DMSConnectionString"];
            if (DCS == null) { throw new ArgumentException("DMS config not configured."); }

            return new ConnectionConfig
            {
                TargetEnvironment = TargetEnvironment,
                QueueName = QueueName,
                BusConnectionString = BCS,
                DMSConnectionString = DCS
            };
        }

        private ConnectionConfig SetConfigFromFile()
        {
            var setting = ConfigurationManager.AppSettings;
            var connection = setting["TS.Integration.EDI.ConfigPath"];
            if (connection == null) { throw new ArgumentException("File Config not configured."); }
            
            var configPath = GetFilesFromBlob(connection);
            var config = File.ReadAllText(configPath.AbsolutePath);

            //throw new ArgumentException("TTs: " + config ?? "NaN");

            string TargetEnvironment = "",
                QueueName = "",
                BCS = "", 
                DCS = "";
            
            return new ConnectionConfig
            {
                TargetEnvironment = TargetEnvironment,
                QueueName = QueueName,
                BusConnectionString = BCS,
                DMSConnectionString = DCS
            };
        }
        
        //private async Task<string> GetDataCoreAsync(string connectionString)
        //{
        //    var _cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
        //    var blobContainer = _cloudStorageAccount.CreateCloudBlobClient().GetContainerReference("");// BlobContainerName);
        //    var blockBlob = blobContainer.GetBlockBlobReference("");// _mapDataName);
        //    var json = await blockBlob.OpenReadAsync();
            
        //    return json;
        //}

        public static Uri GetFilesFromBlob(string storageConnectionString)
        {
            var storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(_storageContainerName);
            var dira = container.GetDirectoryReference(_storageFolder);
            
            return dira.ListBlobs().OfType<CloudBlockBlob>().Where(x => x.Name == _configFileName)?.First().Uri;
        }

        #endregion
    }
}
