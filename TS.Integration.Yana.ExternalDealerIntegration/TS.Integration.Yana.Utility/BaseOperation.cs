﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Utility
{
    using Microsoft.Xrm.Sdk;
    using Model;
    using Newtonsoft.Json;
    using OP.MSCRM.Wrapper;
    using TS.Integration.Yana.Utility.Custom;

    public abstract class BaseOperation<TE>
        where TE : IBaseEntity
    {
        #region Cons
        public const string ExternalCodeScheme = "ktb_externalcode";
        #endregion

        public BaseExecute Context { get; }
        public Entity Reference { get; set; }
        public TE DataContext { get; internal set; }

        private Entity Ent;
        public OperationTask Operation { get; set; }
        public string Checkpoint { get; set; }
        public abstract Entity PrepareData(TE Ent);
        public virtual OperationTask OperationIdentifier() => GetOperationIdentifier();
        public bool PrintExecuteLog = true;

        public BaseOperation(BaseExecute e, TE te) {
            Context = e;
            DataContext = te;
        }

        public virtual void Execute()
        {
            try
            {
                "Prepare data".ConsoleWriteLine();
                Reference = Ent = PrepareData(DataContext);

                if (Context.DebugMode) "Operate Identifier Check".ConsoleWriteLine();
                Operation = OperationIdentifier();
                if (Context.DebugMode) ("Task : " + Operation.ToString()).ConsoleWriteLine();

                if (Operation == OperationTask.Create)
                {
                    ("Attempt to create " + DataContext.Context).ConsoleWriteLine();
                    Reference.Id = Context.Service.Create(Ent);
                    ("Created with Id: " + Reference.Id.ToString()).ConsoleWriteLine();
                }
                else if (Operation == OperationTask.Update)
                {
                    ("Attempt to update " + DataContext.Context).ConsoleWriteLine();
                    Context.Service.Update(Ent);
                    ("Updated Successfully." + DataContext.Context).ConsoleWriteLine();
                }

                if (PrintExecuteLog)
                {
                    new ExternalDealerLog(Context, new EDILog<Entity>
                    {
                        DealerIdentifier = DataContext.DealerCode,
                        Context = DataContext.Context,
                        ExternalCode = DataContext.ExternalCode,
                        ReceivedMessage = Context.Messages,
                        Operation = (int)Operation,
                        Lookup = Ent
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetErrorMessage());
                if (PrintExecuteLog)
                {
                    new ExternalDealerLog(Context, new EDILog<Entity>
                    {
                        Context = DataContext.Context,
                        DealerIdentifier = DataContext.DealerCode,
                        ExternalCode = DataContext.ExternalCode,
                        ReceivedMessage = Checkpoint + "\n" + Context.Messages,
                        ExceptionMessage = ex.GetErrorMessage(),
                        Operation = (int)Operation,
                        Lookup = Ent ?? null
                    });
                }
            }
        }

        private OperationTask GetOperationIdentifier()
            //=> Operation.Create;
            => new Mapper<Entity>(Context.Service).Fetch(DataContext.ExternalCode, e => ExternalCodeScheme, e => e.Id)
                .Any() ? OperationTask.Update : OperationTask.Create;


        public enum OperationTask
        {
            Create = 1,
            Update,
            Delete,
            Read,
            Configuration
        }
    }
}
