﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TS.Integration.Yana.Utility.Interfaces
{
    public interface IConfigSetup
    {
        string ConnectionString { get; }
        string QueueName { get; }
    }
}
