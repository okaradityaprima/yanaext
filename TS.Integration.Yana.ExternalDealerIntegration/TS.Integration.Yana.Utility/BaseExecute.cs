﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Net;
using System.ServiceModel.Description;
using TS.Integration.Yana.Utility.Custom;

namespace TS.Integration.Yana.Utility
{
    public abstract class BaseExecute
    {
        #region Const
        public bool DebugMode = false;

        public const int Retry = 4000;
        public const int RetryCount = 3;
        private const string ConnectionString = "asdf";
        private const string QueueName = "queue";
        public const string ExternalCodeScheme = "ktb_externalcode";
        #endregion

        #region DataMember
        public IOrganizationService Service;
        public QueueManager Queue;
        public ConfigSetup Config;

        public string EntityContext;
        public string Messages { get; set; }
        #endregion

        protected BaseExecute() => Configure();
        
        protected void PrepareQueue() => Queue = new QueueManager(Config);

        protected void Configure()
        {
            try
            {
                //GetConfig {Adapter | File}
                if (Config == null) {
                    Extensions.ConsoleWriteLine("Fetching configuration..");
                    Config = new ConfigSetup();
                }

                //Prepare Connection - Validate
                if (Service == null) {
                    Extensions.ConsoleWriteLine("Connecting to environment..");
                    Service = PrepareService() ?? PrepareServiceByToken();
                }

                Config.TargetEnvironment.ConsoleWriteLine();
                Extensions.ConsoleWriteLine("============================================");
                Extensions.ConsoleWriteLine("Standing by..");

                /* Remarking this, triggered on Program instead.
                    //Prepare Queue Listener & Fetch
                    PrepareQueue(); 
                */
            }
            catch(Exception ex)
            {
                var ErrMsg = (string.IsNullOrWhiteSpace(ex.InnerException?.Message) ? ex.Message : ex.InnerException?.Message);
                Extensions.ConsoleWriteLine("Issue Occurs, details as follow.\n"
                    + ErrMsg);

                if (Service != null) new ExternalDealerLog(this, "Setup Configuration", ErrMsg);
            }
        }

        //public abstract void SetContext();

        #region Connection
        public IOrganizationService PrepareService()
        {
            IOrganizationService organizationService = null;
            var envi = Config.TargetEnvironment;
            if (envi == null) return null;
            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = Config.DMSConnectionConfig?.Username ?? (DebugMode ? "connect.user@ktbmmksidms.onmicrosoft.com" : null);
                clientCredentials.UserName.Password = Config.DMSConnectionConfig?.Password ?? (DebugMode ? "Mmksi@2018" : null);

                if (clientCredentials.UserName.UserName == null) return null;

                // For Dynamics 365 Customer Engagement V9.X, set Security Protocol as TLS12
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                
                organizationService = (IOrganizationService)new OrganizationServiceProxy(
                    new Uri("https://" + envi + ".api.crm5.dynamics.com/XRMServices/2011/Organization.svc"),
                    null,
                    clientCredentials,
                    null
                );

                if (organizationService != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                    
                    if (userid != Guid.Empty)
                    {
                        Utility.Extensions.ConsoleWriteLine("Connection Established Successfully...");
                        return organizationService;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Utility.Extensions.ConsoleWriteLine("Exception caught - " + ex.Message);
                return null;
            }
        }

        public IOrganizationService PrepareServiceByToken()
        {
            return null;
        }
        #endregion
    }
}
