﻿namespace Technosoft.YanaExt.BSI.Plugin.CRM
{
    using System;
    using Microsoft.Xrm.Sdk;
    using Technosoft.DMS.XRM.Common.Business.ApprovalMap;
    using Technosoft.DMS.XRM.Common.Business.PluginExecution;
    using Technosoft.DMS.XRM.EntityClasses.Entities;

    public class PreApprovalMapDelete : Plugin
    {
        public PreApprovalMapDelete()
            : base(typeof(PreApprovalMapDelete))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Delete",
                "xts_approvalmap", new Action<LocalPluginContext>(ExecutePreApprovalMapDelete)));

        }

        protected void ExecutePreApprovalMapDelete(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            var dataContext = new DataContext<xts_approvalmap>(localContext.ServiceProvider);
            var service = dataContext.OrganizationService;
            var context = dataContext.Prepare();
            ExecuteOperations(service, context);
        }

        private void ExecuteOperations(IOrganizationService service, ITransactionContext<xts_approvalmap> context)
        {
            new Business.CRM.ApprovalMap.DeletePluginStep(context).Execute();
        }
    }
}
