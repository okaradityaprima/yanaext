﻿namespace Technosoft.YanaExt.BSI.Plugin.CRM
{
    using System;
    using Microsoft.Xrm.Sdk;
    using Technosoft.DMS.XRM.Common.Business.PluginExecution;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    
    /// <summary>
    /// 
    /// </summary>
    public class PreApprovalMapCreate : Plugin
    {
        public PreApprovalMapCreate()
            : base(typeof(PreApprovalMapCreate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Create",
                "xts_approvalmap", new Action<LocalPluginContext>(ExecutePreApprovalMapCreate)));
        }

        protected void ExecutePreApprovalMapCreate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            var dataContext = new DataContext<xts_approvalmap>(localContext.ServiceProvider);
            var service = dataContext.OrganizationService;
            var context = dataContext.Prepare();
            ExecuteOperations(service, context);
        }

        private void ExecuteOperations(IOrganizationService service, ITransactionContext<xts_approvalmap> context)
        {
            new Business.CRM.ApprovalMap.CreatePluginStep(context).Execute();
        }
    }
}