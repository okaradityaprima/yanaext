﻿namespace Technosoft.YanaExt.BSI.Plugin.CRM
{
    using System;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using Technosoft.DMS.XRM.Global.Config.DmsPlugin;

    public class PreApprovalUpdate : Plugin
    {
        public PreApprovalUpdate(string unsecureConfig, string secureConfig)
            : base(typeof(PreApprovalUpdate))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(20, "Update", null, new Action<LocalPluginContext>(ExecutePreApprovalUpdate)));
        }

        private IPluginExecutionContext _context;
        private IOrganizationService _service;

        protected void ExecutePreApprovalUpdate(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new InvalidPluginExecutionException(new ArgumentNullException("localContext").Message);
            }
            var tracingService = (ITracingService)localContext.ServiceProvider.GetService(typeof(ITracingService));
            _context = localContext.PluginExecutionContext;
            _service = localContext.OrganizationService;
            var dmsContext = localContext.ServiceProvider.GetDmsPluginContext();

            var entity = (Entity)_context.InputParameters["Target"];
            var originalEntity = GetOriginalEntity(entity);
            var attrributeTiggering = new Business.CRM.ApprovalOperation(entity, originalEntity, _context, dmsContext, _service, tracingService).Execute();

            tracingService.Trace("Attribute Trigering : " + attrributeTiggering);
            if (attrributeTiggering != string.Empty)
            {
                if (entity.Attributes.Contains(attrributeTiggering))
                {
                    entity.Attributes.Remove(attrributeTiggering);
                }
                var originalValue = originalEntity.GetAttributeValue<OptionSetValue>(attrributeTiggering);

                tracingService.Trace("Attribute Trigering Value : " + originalValue.Value);

                entity.Attributes.Add(attrributeTiggering, new OptionSetValue(originalValue.Value));
            }
        }

        private Entity GetOriginalEntity(Entity entityTarget)
        {
            if (_context.MessageName == "Create")
            {
                return entityTarget;
            }
            else
            {
                var columnSet = new ColumnSet(true);
                return _service.Retrieve(entityTarget.LogicalName, entityTarget.Id, columnSet);
            }
        }
    }
}
