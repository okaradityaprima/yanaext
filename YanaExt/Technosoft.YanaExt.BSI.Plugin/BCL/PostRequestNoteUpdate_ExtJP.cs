﻿namespace Technosoft.YanaExt.BSI.Plugin.BCL
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Collections.Generic;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.EntityClasses.Data.Wrappers;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using Technosoft.DMS.XRM.Service.Business;
    using Technosoft.YanaExt.BSI.Business.BCL.Execution.RequestNote;

    /// <summary>
    /// PostRequestNoteUpdate_ExtJP Plugin.
    /// Fires when the following attributes are updated:
    /// xts_handling
    /// </summary>    
    public class PostRequestNoteUpdate_ExtJP : Plugin
    {
        private ITracingService _tracingservice;

        public PostRequestNoteUpdate_ExtJP()
            : base(typeof(PostRequestNoteUpdate_ExtJP))
        {
            RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "Update", "xts_requestnote",
                new Action<LocalPluginContext>(ExecutePostRequestNoteUpdate_ExtJP)));
        }

        protected void ExecutePostRequestNoteUpdate_ExtJP(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new InvalidPluginExecutionException(new ArgumentNullException("localContext").Message);
            }

            _tracingservice = localContext.TracingService;

            ColumnSet columnSet = new ColumnSet(new string[] {
                EntityExt.GetAttributeLogicalName<xts_requestnote>(x => x.xts_handling),
                EntityExt.GetAttributeLogicalName<xts_requestnote>(x => x.xts_status),
                EntityExt.GetAttributeLogicalName<xts_requestnote>(x => x.xts_businessunitid),
                EntityExt.GetAttributeLogicalName<xts_requestnote>(x => x.xts_workorderid),
                EntityExt.GetAttributeLogicalName<xts_requestnote>(x => x.xts_type),
                EntityExt.GetAttributeLogicalName<xts_requestnote>(x => x.xts_salesorderid)
            });

            IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> serviceContext = localContext.ServiceProvider.GetServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>>(columnSet);

            var sharedVariable = new SharedVariableHelper().GetSharedVariable(serviceContext.PluginExecutionContext, "RNSTATUS");

            int statusPre = sharedVariable != null ? Convert.ToInt32(sharedVariable) : 0;
            int statusPost = serviceContext.PostImageEntity.ToEntity<xts_requestnote>().xts_status != null ? serviceContext.PostImageEntity.ToEntity<xts_requestnote>().xts_status.Value : 0;

            Dictionary<StateTransitionKey, object> workOrderTransitions = new Dictionary<StateTransitionKey, object>
            {
                { new StateTransitionKey((int)xts_requestnote.Status.Open,           (int)xts_requestnote.Status.Released),  new OpenToReleaseCombineBCL(serviceContext, _tracingservice) },
                { new StateTransitionKey((int)xts_requestnote.Status.Released,       (int)xts_requestnote.Status.Cancelled), new ReleaseToCancelBCL(serviceContext, _tracingservice) },
                { new StateTransitionKey((int)xts_requestnote.Status.PartialReceipt, (int)xts_requestnote.Status.Cancelled), new ReleaseToCancelBCL(serviceContext, _tracingservice) }
            };

            var state = StateTransitionHelper.StateNextExecute(statusPre, statusPost, workOrderTransitions);
            if (state != null)
            {
                ((ServiceBaseOperation)state).Execute();
            }
        }
    }
}