﻿namespace Technosoft.YanaExt.BSI.Business.CRM
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Technosoft.DMS.XRM.Base.Data.Query;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using Technosoft.DMS.XRM.Messages;

    public class TransactionApprovalHistory
    {
        private readonly string TriggerOnApproved = "Approved";
        private readonly string TriggerOnRejected = "Rejected";
        private IOrganizationService _service;
        private Entity _entity;
        private IPluginExecutionContext _context;
        private IMessages _messages;
        private Guid _userId;
        private readonly ITracingService _tracingService;
        private EntityReference _employee;

        public TransactionApprovalHistory(Entity entity, IPluginExecutionContext context, IOrganizationService service, IMessages messages, ITracingService iTracingService)
        {
            _entity = entity;
            _service = service;
            _context = context;
            _tracingService = iTracingService;
            _userId = context.InitiatingUserId;
            _messages = messages;
            _employee = GetEmployee();
        }

        public bool Create(xts_approvalmap approvalMap)
        {
            var approvalCreated = false;
            var approvalMapId = approvalMap.Id;
            var approvalParties = GetApprovalParties(approvalMapId);

            var sequence = 1;
            foreach (var parties in approvalParties)
            {
                var approvalConditions = GetApprovalCondition(parties.Id);

                var conditionFlag = false;

                foreach (var approvalCondition in approvalConditions)
                {
                    var condition = approvalCondition.xts_condition;
                    var attributeTypeData = approvalCondition.xts_fieldnametypedata;
                    var attributeConditions = approvalCondition.xts_fieldnamevalue;
                    var conditionOptionSetValue = approvalCondition.xts_optionsetvalue;
                    var conditionMoneyValue = approvalCondition.xts_amount;
                    var conditionStringValue = approvalCondition.xts_value;

                    if (!_entity.Contains(attributeConditions) || condition == null)
                    {
                        conditionFlag = false;
                        break;
                    }

                    var optionSetCondition = (xts_approvalcondition.Condition)condition.Value;
                    if (attributeTypeData == "Picklist")
                    {
                        var attributeOptionSet = _entity.GetAttributeValue<OptionSetValue>(attributeConditions);
                        var valueOptionSet = attributeOptionSet != null ? attributeOptionSet.Value.ToString() : string.Empty;
                        conditionFlag = CheckConditionString(optionSetCondition, valueOptionSet, conditionOptionSetValue);
                    }
                    else if (attributeTypeData == "Boolean")
                    {
                        var attributeBoolean = _entity.GetAttributeValue<Boolean>(attributeConditions);
                        var conditionBooleanValue = !string.IsNullOrEmpty(conditionOptionSetValue) ? (conditionOptionSetValue == "1" ? true : false) : false;
                        conditionFlag = CheckConditionBoolean(optionSetCondition, attributeBoolean, conditionBooleanValue);
                    }
                    else if (attributeTypeData == "String")
                    {
                        var attributeString = _entity.GetAttributeValue<String>(attributeConditions);
                        var valueString = attributeString != null ? attributeString : string.Empty;
                        conditionFlag = CheckConditionString(optionSetCondition, valueString, conditionStringValue);
                    }
                    else if (attributeTypeData == "Decimal")
                    {
                        var attributeDecimal = _entity.GetAttributeValue<Decimal>(attributeConditions);
                        conditionFlag = CheckConditionDecimalDoubleInteger(optionSetCondition, attributeDecimal, conditionStringValue);
                    }
                    else if (attributeTypeData == "Double")
                    {
                        var attributeDouble = (Decimal)_entity.GetAttributeValue<Double>(attributeConditions);
                        conditionFlag = CheckConditionDecimalDoubleInteger(optionSetCondition, attributeDouble, conditionStringValue);
                    }
                    else if (attributeTypeData == "Integer")
                    {
                        var attributeInteger = _entity.GetAttributeValue<Int64>(attributeConditions);
                        conditionFlag = CheckConditionDecimalDoubleInteger(optionSetCondition, attributeInteger, conditionStringValue);
                    }
                    else if (attributeTypeData == "Money")
                    {
                        var attributeMoney = _entity.GetAttributeValue<Money>(attributeConditions);
                        var valueMoney = attributeMoney != null ? attributeMoney.Value : 0m;
                        conditionFlag = CheckConditionMoney(optionSetCondition, valueMoney, conditionMoneyValue);
                    }
                    else if (attributeTypeData == "Lookup")
                    {
                        var attributeLookup = _entity.GetAttributeValue<EntityReference>(attributeConditions);
                        var valueId = attributeLookup != null ? attributeLookup.Id : Guid.Empty;
                        var conditionLookup = approvalCondition.GetAttributeValue<EntityReference>(attributeConditions);
                        var conditionValueId = conditionLookup != null ? conditionLookup.Id : Guid.Empty;
                        conditionFlag = CheckConditionLookup(optionSetCondition, valueId, conditionValueId);
                    }

                    if (!conditionFlag) break;
                }

                if (!conditionFlag) continue;

                var state = sequence == 1 ?
                    (int)xts_newtransactionapprovalhistory.State.WaitingForApproval :
                    (int)xts_newtransactionapprovalhistory.State.Open;

                var pfAttributeValue = string.Empty;
                var attributePrimaryField = approvalMap.xts_primaryattribute;
                if (_entity.Contains(attributePrimaryField))
                {
                    pfAttributeValue = _entity.GetAttributeValue<String>(attributePrimaryField);
                }

                var createTransactionApprovalHistory = new xts_newtransactionapprovalhistory
                {
                    xts_status = new OptionSetValue(state),
                    xts_approvalteamid = parties.xts_approverteamid,
                    xts_approveruserid = parties.xts_approverteamid == null ? parties.xts_approveruserid : null,
                    xts_approveremployeeid = parties.xts_approveremployeeid,
                    xts_requestedbyuserid = new EntityReference(SystemUser.EntityLogicalName, _userId),
                    RegardingObjectId = new EntityReference(_entity.LogicalName, _entity.Id),
                    xts_sequence = sequence,
                    Subject = "Asking approval for " + pfAttributeValue
                };
                if (parties.xts_approverteamid != null || parties.xts_approveruserid != null)
                {
                    createTransactionApprovalHistory["ownerid"] = parties.xts_approveruserid ?? parties.xts_approverteamid;
                }


                var bu = GetBusinessUnit(parties.xts_approverteamid, parties.xts_approveruserid);
                if (bu != null)
                {
                    createTransactionApprovalHistory.xts_businessunitid = new EntityReference(BusinessUnit.EntityLogicalName, bu.Id);

                    if (bu.ParentBusinessUnitId != null)
                    {
                        createTransactionApprovalHistory.xts_parentbusinessunitid = new EntityReference(BusinessUnit.EntityLogicalName, bu.ParentBusinessUnitId.Id);
                    }
                }

                if (_employee != null)
                {
                    createTransactionApprovalHistory.xts_requestedbyemployeeid = new EntityReference(xts_employee.EntityLogicalName, _employee.Id);
                }

                _service.Create(createTransactionApprovalHistory.ToEntity<Entity>());
                approvalCreated = true;

                sequence++;

            }
            return approvalCreated;
        }

        private BusinessUnit GetBusinessUnit(EntityReference teamApprover, EntityReference userApprover)
        {
            if (teamApprover != null)
            {
                var columnSetTeam = new ColumnSet<Team>(e => e.BusinessUnitId);

                var teamQuery = _service.Retrieve(Team.EntityLogicalName, teamApprover.Id, columnSetTeam);

                if (teamQuery == null) return null;

                var team = teamQuery.ToEntity<Team>();

                if (team.BusinessUnitId == null) return null;

                var columnSetBusinessUnit = new ColumnSet<BusinessUnit>(e => e.ParentBusinessUnitId);
                var businessUnit = _service.Retrieve(BusinessUnit.EntityLogicalName, team.BusinessUnitId.Id, columnSetBusinessUnit);

                return businessUnit?.ToEntity<BusinessUnit>();
            }
            else if (userApprover != null)
            {
                var columnSetSystemUser = new ColumnSet<SystemUser>(e => e.BusinessUnitId);

                var systemUserQuery = _service.Retrieve(SystemUser.EntityLogicalName, userApprover.Id, columnSetSystemUser);

                if (systemUserQuery == null) return null;

                var user = systemUserQuery.ToEntity<SystemUser>();

                if (user.BusinessUnitId == null) return null;

                var columnSetBusinessUnit = new ColumnSet<BusinessUnit>(e => e.ParentBusinessUnitId);
                var businessUnit = _service.Retrieve(BusinessUnit.EntityLogicalName, user.BusinessUnitId.Id, columnSetBusinessUnit);

                return businessUnit?.ToEntity<BusinessUnit>();
            }

            return null;
        }

        private EntityReference GetEmployee()
        {
            var query = new QueryExpression
            {
                EntityName = xts_employee.EntityLogicalName,
                ColumnSet = new ColumnSet<xts_employee>(e => e.Id)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_employee>(e => e.xts_regardinguserid), ConditionOperator.Equal, _userId);

            var result = _service.RetrieveMultiple(query);

            return result.Entities.Any() ? result.Entities[0].ToEntityReference() : null;
        }

        public IEnumerable<xts_newtransactionapprovalhistory> Get()
        {
            var query = new QueryExpression
            {
                EntityName = xts_newtransactionapprovalhistory.EntityLogicalName,
                ColumnSet = new ColumnSet(true)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_newtransactionapprovalhistory>(e => e.RegardingObjectId),
                ConditionOperator.Equal, _entity.Id);
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_newtransactionapprovalhistory>(e => e.xts_status),
                ConditionOperator.NotNull);
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_newtransactionapprovalhistory>(e => e.xts_sequence),
                ConditionOperator.NotNull);
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_newtransactionapprovalhistory>(e => e.xts_status),
                ConditionOperator.NotEqual, (int)xts_newtransactionapprovalhistory.State.Approved);
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_newtransactionapprovalhistory>(e => e.xts_status),
                ConditionOperator.NotEqual, (int)xts_newtransactionapprovalhistory.State.Rejected);

            var queryResult = _service.RetrieveMultiple(query);

            return queryResult.Entities.Any() ?
                queryResult.Entities.Select(e => e.ToEntity<xts_newtransactionapprovalhistory>()).OrderBy(e => e.xts_sequence) :
                Enumerable.Empty<xts_newtransactionapprovalhistory>();
        }

        public bool Aprroved(IEnumerable<xts_newtransactionapprovalhistory> transactionApprovalHistories)
        {
            var historyWaitingForApproval = transactionApprovalHistories.Where(
                e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.WaitingForApproval);

            if (historyWaitingForApproval.Any())
            {
                var TAH = historyWaitingForApproval.FirstOrDefault();

                ApprovalValidate(TAH, TriggerOnApproved);

                var updateTransactionApprovalHistroy = new xts_newtransactionapprovalhistory
                {
                    Id = TAH.Id,
                    xts_date = DateTime.UtcNow,
                    xts_status = new OptionSetValue((int)xts_newtransactionapprovalhistory.State.Approved),
                    xts_approveruserid = new EntityReference(SystemUser.EntityLogicalName, _context.UserId)
                };

                if (_employee != null)
                {
                    updateTransactionApprovalHistroy.xts_approveremployeeid = new EntityReference(xts_employee.EntityLogicalName, _employee.Id);
                }
                else
                {
                    updateTransactionApprovalHistroy.xts_approveremployeeid = null;
                }

                _service.Update(updateTransactionApprovalHistroy.ToEntity<Entity>());
            }

            return UpdateOtherTransactionApprovalHistory(transactionApprovalHistories);
        }

        public void Rejected(IEnumerable<xts_newtransactionapprovalhistory> transactionApprovalHistories)
        {
            var historyWaitingForApproval = transactionApprovalHistories.Where(
                e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.WaitingForApproval);

            if (historyWaitingForApproval.Any())
            {
                var TAH = historyWaitingForApproval.FirstOrDefault();

                ApprovalValidate(TAH, TriggerOnRejected);
            }

            var historyOpenOrWaitingForApproval = transactionApprovalHistories.Where(
                e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.WaitingForApproval ||
                    e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.Open);

            foreach (var history in historyOpenOrWaitingForApproval)
            {
                var updateTransactionApprovalHistroy = new xts_newtransactionapprovalhistory
                {
                    Id = history.Id,
                    xts_date = DateTime.UtcNow,
                    xts_status = new OptionSetValue((int)xts_newtransactionapprovalhistory.State.Rejected),
                    xts_approveruserid = new EntityReference(SystemUser.EntityLogicalName, _context.UserId)
                };

                if (_employee != null)
                {
                    updateTransactionApprovalHistroy.xts_approveremployeeid = new EntityReference(xts_employee.EntityLogicalName, _employee.Id);
                }
                else
                {
                    updateTransactionApprovalHistroy.xts_approveremployeeid = null;
                }

                _service.Update(updateTransactionApprovalHistroy.ToEntity<Entity>());
            }
        }

        private void ApprovalValidate(xts_newtransactionapprovalhistory approvalHistory, string triggerOn)
        {
            var approverEmployee = approvalHistory.xts_approveremployeeid;
            var approverTeam = approvalHistory.xts_approvalteamid;
            var approverUser = approvalHistory.xts_approveruserid;

            var validateValid = false;

            if (approverEmployee != null && _employee != null)
            {
                validateValid = approverEmployee.Id == _employee.Id;
                _tracingService.Trace("Tracing: Valid Employee : " + validateValid);
            }
            if (approverUser != null && _context.UserId != null && !validateValid)
            {
                validateValid = approverUser.Id == _context.UserId;
                _tracingService.Trace("Tracing: Valid User : " + validateValid);
            }
            if (approverTeam != null && _context.UserId != null && !validateValid)
            {//Get USER Bu
                validateValid = GetTeamUserExisting(approverTeam.Id);
                _tracingService.Trace("Tracing: Valid Team : " + validateValid);
            }
            _tracingService.Trace("Tracing: Valid : " + validateValid);
            if (triggerOn == TriggerOnApproved && !validateValid)
            {
                throw new InvalidPluginExecutionException(_messages.GetMessageString("SYS001256"));
            }
            else if (triggerOn == TriggerOnRejected && !validateValid)
            {
                throw new InvalidPluginExecutionException(_messages.GetMessageString("SYS001257"));
            }
        }

        private bool GetTeamUserExisting(Guid teamId)
        {
            var columnSetTeam = new ColumnSet<TeamMembership>(e => e.TeamMembershipId);

            var queryTeam = new QueryExpression
            {
                EntityName = TeamMembership.EntityLogicalName,
                ColumnSet = columnSetTeam
            };
            queryTeam.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<TeamMembership>(e => e.SystemUserId), ConditionOperator.Equal, _context.UserId);
            queryTeam.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<TeamMembership>(e => e.TeamId), ConditionOperator.Equal, teamId);

            var result = _service.RetrieveMultiple(queryTeam);

            return result.Entities.Any();
        }

        private bool UpdateOtherTransactionApprovalHistory(IEnumerable<xts_newtransactionapprovalhistory> transactionApprovalHistories)
        {
            var historyOtherStateOpen = transactionApprovalHistories.Where(
                e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.Open);

            if (historyOtherStateOpen.Any())
            {
                var updateTransactionApprovalHistory = new xts_newtransactionapprovalhistory
                {
                    Id = historyOtherStateOpen.FirstOrDefault().Id,
                    xts_status = new OptionSetValue((int)xts_newtransactionapprovalhistory.State.WaitingForApproval),
                };
                _service.Update(updateTransactionApprovalHistory.ToEntity<Entity>());

                return true;
            }

            return false;
        }

        private IEnumerable<xts_approvalparties> GetApprovalParties(Guid approvalMapId)
        {
            var query = new QueryExpression
            {
                EntityName = xts_approvalparties.EntityLogicalName,
                ColumnSet = new ColumnSet(true)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalparties>(e => e.xts_approvalmapId),
                ConditionOperator.Equal, approvalMapId);

            var queryResult = _service.RetrieveMultiple(query);

            return queryResult.Entities.Any() ?
                queryResult.Entities.Select(e => e.ToEntity<xts_approvalparties>()).OrderBy(e => e.xts_sequence) : Enumerable.Empty<xts_approvalparties>();
        }

        private IEnumerable<xts_approvalcondition> GetApprovalCondition(Guid approvalPartiesId)
        {
            var query = new QueryExpression
            {
                EntityName = xts_approvalcondition.EntityLogicalName,
                ColumnSet = new ColumnSet(true)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalcondition>(e => e.xts_appvoralpartiesid),
                ConditionOperator.Equal, approvalPartiesId);

            var queryResult = _service.RetrieveMultiple(query);

            return queryResult.Entities.Any() ?
                queryResult.Entities.Select(e => e.ToEntity<xts_approvalcondition>()) : Enumerable.Empty<xts_approvalcondition>();
        }

        private bool CheckConditionString(xts_approvalcondition.Condition condition, string attributeValue, string conditionValue)
        {
            if (condition == xts_approvalcondition.Condition.ContainsData)
            {
                return attributeValue != null;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotContainData)
            {
                return attributeValue == null;
            }
            else if (condition == xts_approvalcondition.Condition.Equals)
            {
                if (string.IsNullOrEmpty(conditionValue)) return false;

                return attributeValue == conditionValue;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotEqual)
            {
                if (string.IsNullOrEmpty(conditionValue)) return false;

                return attributeValue != conditionValue;
            }

            return false;
        }

        private bool CheckConditionLookup(xts_approvalcondition.Condition condition, Guid attributeValue, Guid conditionValue)
        {
            if (condition == xts_approvalcondition.Condition.Equals)
            {
                return attributeValue == conditionValue;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotEqual)
            {
                return attributeValue != conditionValue;
            }

            return false;
        }

        private bool CheckConditionBoolean(xts_approvalcondition.Condition condition, bool attributeValue, bool conditionValue)
        {
            if (condition == xts_approvalcondition.Condition.ContainsData)
            {
                return attributeValue != null;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotContainData)
            {
                return attributeValue == null;
            }
            else if (condition == xts_approvalcondition.Condition.Equals)
            {
                return attributeValue == conditionValue;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotEqual)
            {
                return attributeValue != conditionValue;
            }

            return false;
        }

        private bool CheckConditionMoney(xts_approvalcondition.Condition condition, decimal attributeValue, Money conditionValue)
        {
            if (condition == xts_approvalcondition.Condition.ContainsData)
            {
                return attributeValue > 0m;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotContainData)
            {
                return attributeValue == 0m;
            }
            else if (conditionValue == null)
            {
                return false;
            }
            else if (condition == xts_approvalcondition.Condition.Equals)
            {
                return attributeValue == conditionValue.Value;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotEqual)
            {
                return attributeValue != conditionValue.Value;
            }
            else if (condition == xts_approvalcondition.Condition.IsGreaterThan)
            {
                return attributeValue > conditionValue.Value;
            }
            else if (condition == xts_approvalcondition.Condition.IsGreaterThanorEqualTo)
            {
                return attributeValue >= conditionValue.Value;
            }
            else if (condition == xts_approvalcondition.Condition.IsLessThan)
            {
                return attributeValue < conditionValue.Value;
            }
            else if (condition == xts_approvalcondition.Condition.IsLessThanorEqualTo)
            {
                return attributeValue <= conditionValue.Value;
            }

            return false;
        }


        private bool CheckConditionDecimalDoubleInteger(xts_approvalcondition.Condition condition, decimal attributeValue, string conditionValue)
        {
            var valueConverToDecimal = 0m;

            try
            {
                valueConverToDecimal = Convert.ToDecimal(conditionValue);
            }
            catch { };


            if (condition == xts_approvalcondition.Condition.ContainsData)
            {
                return attributeValue > 0m;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotContainData)
            {
                return attributeValue == 0m;
            }
            else if (conditionValue == null)
            {
                return false;
            }
            else if (condition == xts_approvalcondition.Condition.Equals)
            {
                return attributeValue == valueConverToDecimal;
            }
            else if (condition == xts_approvalcondition.Condition.DoesNotEqual)
            {
                return attributeValue != valueConverToDecimal;
            }
            else if (condition == xts_approvalcondition.Condition.IsGreaterThan)
            {
                return attributeValue > valueConverToDecimal;
            }
            else if (condition == xts_approvalcondition.Condition.IsGreaterThanorEqualTo)
            {
                return attributeValue >= valueConverToDecimal;
            }
            else if (condition == xts_approvalcondition.Condition.IsLessThan)
            {
                return attributeValue < valueConverToDecimal;
            }
            else if (condition == xts_approvalcondition.Condition.IsLessThanorEqualTo)
            {
                return attributeValue <= valueConverToDecimal;
            }

            return false;
        }
    }
}