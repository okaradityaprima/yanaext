﻿namespace Technosoft.YanaExt.BSI.Business.CRM.ApprovalMap
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Linq;
    using Technosoft.DMS.XRM.Base.Data.Query;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.Common.Business.PluginExecution;
    using Technosoft.DMS.XRM.EntityClasses.Data.Wrappers;
    using Technosoft.DMS.XRM.EntityClasses.Entities;

    public class DeletePluginStep : BaseOperation<xts_approvalmap, Xts_ApprovalMapWrapper<xts_approvalmap>>
    {
        private readonly ITracingService _tracingService;
        public DeletePluginStep(ITransactionContext<xts_approvalmap> context) : base(context)
        {
            _tracingService = context.TracingService;
        }

        private const string UPDATE_MESSAGE_NAME = "Update";
        private const string PROJECT_NAME = "Technosoft.DMS.XRM.Global.Plugin";
        private const string CLASS_NAME_UPDATE = "Technosoft.DMS.XRM.Global.Plugin.PreApprovalUpdate";

        protected override void HandleExecute()
        {
            if (!SiblingExist()) 
                DeleteStep();
        }

        private bool SiblingExist()
        {
            var query = new QueryExpression
            {
                EntityName = xts_approvalmap.EntityLogicalName,
                ColumnSet = new ColumnSet(true)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalmap>(e => e.xts_entitynamevalue),
                ConditionOperator.Equal, Wrapper.XrmEntity.xts_entitynamevalue);

            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalmap>(e => e.Id),
                            ConditionOperator.NotEqual, Wrapper.Id);

            var queryResult = Context.OnBehalfService.RetrieveMultiple(query);

            return queryResult.Entities.Any() ? true : false;
        }

        private void DeleteStep()
        {
            var wrapper = Wrapper.XrmEntity;
            var entityLogicalName = wrapper.xts_entitynamevalue;
            var objectTypeCode = 0;

            var entityDisplayName = getEntity(entityLogicalName, out objectTypeCode).Replace(" ", string.Empty);
            _tracingService.Trace("Entity Display Name :" + entityDisplayName);
            _tracingService.Trace("Entity Object Type Code :" + objectTypeCode);

            string stepName = string.Format("Pre{0}ApprovalUpdate", entityDisplayName);
            var query = new QueryExpression
            {
                EntityName = SdkMessageProcessingStep.EntityLogicalName,
                ColumnSet = new ColumnSet<SdkMessageProcessingStep>(e => e.StateCode, e => e.Name)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<SdkMessageProcessingStep>(e => e.Name), ConditionOperator.Equal, stepName);

            var step = Context.OnBehalfService.RetrieveMultiple(query);

            if (step != null && step.Entities.Count > 0)
            {
                var sdkStep = step.Entities[0].ToEntity<SdkMessageProcessingStep>();
                Context.OnBehalfService.Delete(SdkMessageProcessingStep.EntityLogicalName, sdkStep.Id);
            }
        }

        private string getEntity(string entityName, out int objectTypeCode)
        {
            var displayName = string.Empty;
            objectTypeCode = 0;

            // Define entity request
            var entityRequest = new RetrieveAllEntitiesRequest
            {
                EntityFilters = EntityFilters.Entity
            };

            // Call execute to retrieve entities
            var entityResult = (RetrieveAllEntitiesResponse)Service.Execute(entityRequest);
            int isFound = entityResult.EntityMetadata.Where(x => x.LogicalName == entityName).Count();

            // Check entity is exist or not
            if (isFound > 0)
            {
                displayName = entityResult.EntityMetadata.Where(x => x.LogicalName == entityName).Single().DisplayName.UserLocalizedLabel.Label;
                objectTypeCode = entityResult.EntityMetadata.Where(x => x.LogicalName == entityName).Single().ObjectTypeCode.Value;
            }

            return displayName;
        }
    }
}
