﻿namespace Technosoft.YanaExt.BSI.Business.CRM
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using Technosoft.DMS.XRM.Global.Config.DmsPlugin;

    public class ApprovalOperation
    {
        private IOrganizationService _service;
        private Entity _entity;
        private Entity _entityInput;
        private Entity _originalEntity;
        private readonly ITracingService _tracingService;
        private IPluginExecutionContext _context;
        private IDmsPluginContext _dmscontext;

        public ApprovalOperation(Entity entityInput, Entity originalEntity, IPluginExecutionContext context, IDmsPluginContext iDmsContext, IOrganizationService service, ITracingService iTracingService)
        {
            _entityInput = entityInput;
            _originalEntity = originalEntity;
            _service = iDmsContext.OrganizationServiceAdmin;
            _tracingService = iTracingService;
            _context = context;
            _dmscontext = iDmsContext;
        }

        public string Execute()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            _tracingService.Trace("Tracing: Start Approval");

            if (_entityInput == null) return string.Empty;

            var approvalMaps = GetApprovalMapRecords();

            stopwatch.Stop();
            _tracingService.Trace(
                string.Format("Tracing: Get Approval Map, duration: {0} ms, Aprroval Map Records: {1}", 
                stopwatch.Elapsed.TotalMilliseconds, 
                approvalMaps.Count().ToString())
            );
            stopwatch.Restart();

            if (approvalMaps == null) return string.Empty;

            var _trigeringAttribute = string.Empty;
            foreach (var approvalMap in approvalMaps)
            {
                _tracingService.Trace("Processing: " + approvalMap.xts_approvalmap1);

                var trigeringAttribute = approvalMap.xts_triggeringattributevalue;
                var askApprovalValue = approvalMap.xts_askforapprovalvalue;
                var approvalValue = approvalMap.xts_approvedvalue;
                var rejectedValue = approvalMap.xts_rejectedvalue;
                var askApprovalStateValue = approvalMap.xts_askforapprovalstatevalue;
                var stateAttribute = approvalMap.xts_stateattributevalue;

                _tracingService.Trace(
                    string.Format(
                        "Triggering attribute:{0}\nAsk for Approval Value:{1}",
                        trigeringAttribute, askApprovalValue
                ));

                GetEntityTarget(trigeringAttribute);
                if (!_entityInput.Contains(trigeringAttribute)) continue; //return string.Empty;
                if (_entityInput.Attributes[trigeringAttribute] == null) continue; //return string.Empty;

                if(!_context.SharedVariables.Contains("PreviousTrigeringAttributeValue"))
                    _context.SharedVariables.Add("PreviousTrigeringAttributeValue", _entityInput.Attributes[trigeringAttribute]);

                if(_context.SharedVariables.Contains("PreviousTrigeringAttributeName"))
                    _context.SharedVariables.Add("PreviousTrigeringAttributeName", trigeringAttribute);

                var optionSetAskApproval = _entityInput.GetAttributeValue<OptionSetValue>(trigeringAttribute);
                if (optionSetAskApproval == null) continue;//return string.Empty;
                //if (int.Parse(askApprovalValue) != optionSetAskApproval.Value) continue;

                _tracingService.Trace("Tracing: Start Get Transaction Approval History");
                var TAH = new TransactionApprovalHistory(_entity, _context, _service, _dmscontext.Messages, _tracingService);

                var transactionApprovalHistory = TAH.Get();

                stopwatch.Stop();
                _tracingService.Trace(string.Format("Tracing: END Get Transaction Approval History, duration: {0} ms, Record Count:{1}", stopwatch.Elapsed.TotalMilliseconds, transactionApprovalHistory.Count()));
                stopwatch.Restart();

                var existingRecordTAH = false;

                if (optionSetAskApproval.Value.ToString() == askApprovalValue)
                {//Ask For Approval Value
                    var historyStateOpenOrWaiting = transactionApprovalHistory.Where(
                        e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.WaitingForApproval ||
                        e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.Open);

                    if (historyStateOpenOrWaiting.Any())
                    {
                        _tracingService.Trace(string.Format("Tracing: Start Update Approved Transaction Approval History. Count Record Existing TAH : {0}", historyStateOpenOrWaiting.Count()));
                        existingRecordTAH = TAH.Aprroved(transactionApprovalHistory);
                    }
                    else
                    {
                        _tracingService.Trace("Tracing: Start Create Transaction Approval History");
                        var approvalCreated = TAH.Create(approvalMap);
                        if (!approvalCreated)
                        {
                            //_tracingService.Trace("Tracing: Transaction Approval History Created");
                            //_tracingService.Trace("Tracing: triggering attribute: " + trigeringAttribute);
                            //_tracingService.Trace("Tracing: Approval Value: " + approvalValue);
                            //_entityInput.Attributes[trigeringAttribute] = new OptionSetValue(Int32.Parse(approvalValue));
                        }
                        else
                        {
                            //_tracingService.Trace("Tracing: Transaction Approval History Not Created. Assumed already there?");
                            //_tracingService.Trace("Tracing: State attribute: " + stateAttribute);
                            //_tracingService.Trace("Tracing: Approval Value: " + askApprovalStateValue);

                            _entityInput.Attributes[stateAttribute] = new OptionSetValue(Int32.Parse(askApprovalStateValue));
                            existingRecordTAH = true;
                        }
                    }

                    stopwatch.Stop();
                    _tracingService.Trace(string.Format("Tracing: Ask For Approval Process, duration: {0} ms", stopwatch.Elapsed.TotalMilliseconds));
                    stopwatch.Restart();
                }
                else if (optionSetAskApproval.Value.ToString() == approvalValue &&
                    transactionApprovalHistory.Where(e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.WaitingForApproval).Any())
                {//Approve Value
                    _tracingService.Trace("Tracing: Start Update Approved Transaction Approval History");
                    existingRecordTAH = TAH.Aprroved(transactionApprovalHistory);

                    stopwatch.Stop();
                    _tracingService.Trace(string.Format("Tracing: Approve Process, duration: {0} ms", stopwatch.Elapsed.TotalMilliseconds));
                    stopwatch.Restart();
                }
                else if (optionSetAskApproval.Value.ToString() == rejectedValue &&
                    transactionApprovalHistory.Where(e => e.xts_status.Value == (int)xts_newtransactionapprovalhistory.State.WaitingForApproval).Any())
                {//Rejected Value
                    _tracingService.Trace("Tracing: Start Update Rejected Transaction Approval History");
                    TAH.Rejected(transactionApprovalHistory);

                    stopwatch.Stop();
                    _tracingService.Trace(string.Format("Tracing: Rejected Process, duration: {0} ms", stopwatch.Elapsed.TotalMilliseconds));
                    stopwatch.Restart();
                }

                if (existingRecordTAH)
                {
                    _trigeringAttribute =  trigeringAttribute;
                }
            }

            return _trigeringAttribute;
        }

        private void GetEntityTarget(string attributeTriggering)
        {
            var columnSet = new ColumnSet(true);
            var result = _originalEntity;
            var entityId = _entityInput.Id;

            if (_context.MessageName != "Create")
            {
                foreach (var attr in _entityInput.Attributes)
                {
                    if (result.Contains(attr.Key) && attr.Key != attributeTriggering)
                    {
                        result[attr.Key] = attr.Value;
                    }
                }
            }
            _entity = result;
        }

        private IEnumerable<xts_approvalmap> GetApprovalMapRecords()
        {
            var businessUnitAttributeName = GetBusinessUnitAttributeName();
            EntityReference businessUnitId = null;
            _tracingService.Trace("Business Unit Attribute : " + businessUnitAttributeName);
            if (businessUnitAttributeName != string.Empty)
            {
                businessUnitId = _entityInput.Contains(businessUnitAttributeName) ? (EntityReference)_entityInput[businessUnitAttributeName] :
                                (_originalEntity.Contains(businessUnitAttributeName) ? (EntityReference)_originalEntity[businessUnitAttributeName] : null);
                _tracingService.Trace("Entity Input Contains Business Unit : " + _entityInput.Contains(businessUnitAttributeName));
                _tracingService.Trace("original Entity Contains Business Unit : " + _originalEntity.Contains(businessUnitAttributeName));

                if (businessUnitId != null)
                {
                    var result = GetApprovalMapRecordsByBusinessUnit(businessUnitId.Id);
                    if (result == null)
                    {
                        var parentBusinessUnit = _dmscontext.BusinessUnitSettingsManager.GetParentBusinessUnit(businessUnitId.Id);
                        if (parentBusinessUnit != null)
                        {
                            return GetApprovalMapRecordsByBusinessUnit(parentBusinessUnit.Id);
                        }
                    }
                    else
                    {
                        return result;
                    }
                }
            }

            return null;
        }

        private IEnumerable<xts_approvalmap> GetApprovalMapRecordsByBusinessUnit(Guid businessUnitId)
        {
            var query = new QueryExpression
            {
                EntityName = xts_approvalmap.EntityLogicalName,
                ColumnSet = new ColumnSet(true)
            };

            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalmap>(e => e.statecode),
                ConditionOperator.Equal, 0);

            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalmap>(e => e.xts_entitynamevalue),
                ConditionOperator.Equal, _entityInput.LogicalName);

            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<xts_approvalmap>(e => e.xts_businessunitid),
                         ConditionOperator.Equal, businessUnitId);
            var queryResult = _service.RetrieveMultiple(query);
            return queryResult.Entities.Any() 
                ? queryResult.Entities.Select(e => e.ToEntity<xts_approvalmap>()) 
                : Enumerable.Empty<xts_approvalmap>();
        }

        private string GetBusinessUnitAttributeName()
        {
            var prefixSchema = _dmscontext.EntityLibrary.GetPrefix(_entityInput.LogicalName);
            var businessUnitAttribute = prefixSchema + "businessunitid";
            _tracingService.Trace("Get Business Unit Attribute Name :" + businessUnitAttribute);
            _tracingService.Trace("Entity Input Contains BU : " + _entityInput.Contains(businessUnitAttribute));
            _tracingService.Trace("Original Entity Contains BU : " + _originalEntity.Contains(businessUnitAttribute));
            if (!_entityInput.Contains(businessUnitAttribute) && !_originalEntity.Contains(businessUnitAttribute))
            {
                businessUnitAttribute = prefixSchema + "frombusinessunitid";
                if (!_entityInput.Contains(businessUnitAttribute) && !_originalEntity.Contains(businessUnitAttribute)) businessUnitAttribute = string.Empty;
            }

            return businessUnitAttribute;
        }
        private string GetTransactionDateAttribute()
        {
            var prefix = _originalEntity.LogicalName.Split('_');

            var transactionDate = string.Format("{0}_{1}", prefix[0], "transactiondate");

            return _originalEntity.Contains(transactionDate) ? transactionDate : "createdon";
        }
    }
}
