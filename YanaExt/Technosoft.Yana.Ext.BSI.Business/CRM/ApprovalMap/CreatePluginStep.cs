﻿namespace Technosoft.YanaExt.BSI.Business.CRM.ApprovalMap
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Linq;
    using Technosoft.DMS.XRM.Base.Data.Query;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.Common.Business.PluginExecution;
    using Technosoft.DMS.XRM.EntityClasses.Data.Wrappers;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    
    /// <summary>
    /// 
    /// </summary>
    public class CreatePluginStep : BaseOperation<xts_approvalmap, Xts_ApprovalMapWrapper<xts_approvalmap>>
    {
        private readonly ITracingService _tracingService;
        public CreatePluginStep(ITransactionContext<xts_approvalmap> context) : base(context)
        {
            _tracingService = context.TracingService;
        }

        private const string UPDATE_MESSAGE_NAME = "Update";
        private const string PROJECT_NAME = "Technosoft.YanaExt.BSI.Plugin";
        private const string CLASS_NAME_UPDATE = "Technosoft.YanaExt.BSI.Plugin.CRM.PreApprovalUpdate";

        protected override void HandleExecute()
        {
            GenerateCreateStep();
        }

        private void GenerateCreateStep()
        {
            var wrapper = Wrapper.XrmEntity;
            var entityLogicalName = wrapper.xts_entitynamevalue;
            var objectTypeCode = 0;

            var entityDisplayName = getEntity(entityLogicalName, out objectTypeCode).Replace(" ", string.Empty);
            _tracingService.Trace("Entity Display Name :" + entityDisplayName);
            _tracingService.Trace("Entity Object Type Code :" + objectTypeCode);

            string stepName = string.Format("Pre{0}ApprovalUpdate", entityDisplayName);
            if (!isStepExist(stepName))
            {
                var sdkMsgId = QuerySDKMessage(UPDATE_MESSAGE_NAME);
                _tracingService.Trace("SDK Message ID :" + sdkMsgId);

                if (sdkMsgId != Guid.Empty)
                {
                    var sdkMsgFilterID = QuerySDKMessageFilter(objectTypeCode, sdkMsgId);
                    _tracingService.Trace("SDK Message Filter ID :" + sdkMsgFilterID);

                    if (sdkMsgFilterID != Guid.Empty)
                    {
                        var pluginTypeId = QueryPluginType(CLASS_NAME_UPDATE);
                        _tracingService.Trace("Plugin Type ID :" + pluginTypeId);

                        if (pluginTypeId != Guid.Empty)
                        {
                            var step = new SdkMessageProcessingStep
                            {
                                Mode = new OptionSetValue(0), //Synchronous
                                Name = stepName,
                                Rank = 1,
                                SupportedDeployment = new OptionSetValue(0),//Server Only Deployment
                                SdkMessageId = new EntityReference(SdkMessage.EntityLogicalName, sdkMsgId),
                                SdkMessageFilterId = new EntityReference(SdkMessageFilter.EntityLogicalName, sdkMsgFilterID),
                                EventHandler = new EntityReference(PluginType.EntityLogicalName, pluginTypeId),
                                Stage = new OptionSetValue(20), //Pre
                                FilteringAttributes = wrapper.xts_triggeringattributevalue
                            };
                            Context.OnBehalfService.Create(step);
                        }
                    }
                }
            }
        }

        private string getEntity(string entityName, out int objectTypeCode)
        {
            var displayName = string.Empty;
            objectTypeCode = 0;

            // Define entity request
            var entityRequest = new RetrieveAllEntitiesRequest
            {
                EntityFilters = EntityFilters.Entity
            };

            // Call execute to retrieve entities
            var entityResult = (RetrieveAllEntitiesResponse)Context.OnBehalfService.Execute(entityRequest);
            int isFound = entityResult.EntityMetadata.Where(x => x.LogicalName == entityName).Count();

            // Check entity is exist or not
            if (isFound > 0)
            {
                displayName = entityResult.EntityMetadata.Where(x => x.LogicalName == entityName).Single().DisplayName.UserLocalizedLabel.Label;
                objectTypeCode = entityResult.EntityMetadata.Where(x => x.LogicalName == entityName).Single().ObjectTypeCode.Value;
            }

            return displayName;
        }

        private bool isStepExist(string stepName)
        {
            bool result = false;
            var query = new QueryExpression
            {
                EntityName = SdkMessageProcessingStep.EntityLogicalName,
                ColumnSet = new ColumnSet<SdkMessageProcessingStep>(e => e.StateCode, e => e.Name)
            };
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<SdkMessageProcessingStep>(e => e.Name), ConditionOperator.Equal, stepName);

            var step = Context.OnBehalfService.RetrieveMultiple(query);

            if (step != null && step.Entities.Count > 0)
            {
                var sdkStep = step.Entities[0].ToEntity<SdkMessageProcessingStep>();
                var state = sdkStep.StateCode.ToOptionSetValue();

                //if (state.IsEqualTo(SdkMessageProcessingStepState.Disabled))
                // {
                UpdateStepStage(sdkStep.Id, state.IsEqualTo(SdkMessageProcessingStepState.Disabled));
                //}
                result = true;
            }

            return result;
        }

        private Guid QuerySDKMessageFilter(int objTypeCode, Guid sdkMessageId)
        {
            Guid resultId = Guid.Empty;

            try
            {
                // Define query attribute for sdk message filter
                var query = new QueryExpression
                {
                    EntityName = SdkMessageFilter.EntityLogicalName,
                    ColumnSet = new ColumnSet(true)
                };

                query.Criteria.AddCondition("primaryobjecttypecode", ConditionOperator.Equal, objTypeCode);
                query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<SdkMessageFilter>(e => e.SdkMessageId), ConditionOperator.Equal, sdkMessageId);

                var result = Context.OnBehalfService.RetrieveMultiple(query);
                if (result.Entities.Any())
                {
                    resultId = result.Entities[0].Id;
                }
            }
            catch (Exception ex)
            {
                _tracingService.Trace("ERROR : " + ex.Message);

                var query = new QueryExpression
                {
                    EntityName = SdkMessageFilter.EntityLogicalName,
                    ColumnSet = new ColumnSet(true)
                };

                query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<SdkMessageFilter>(e => e.SdkMessageId), ConditionOperator.Equal, sdkMessageId);

                var result = Context.OnBehalfService.RetrieveMultiple(query);
                if (result.Entities.Any())
                {
                    foreach (var sdkMessageFilter in result.Entities.Select(e => e.ToEntity<SdkMessageFilter>()))
                    {
                        _tracingService.Trace("SDK Message Filter ID :" + sdkMessageFilter.Id);

                        var a = sdkMessageFilter.RelatedEntities;
                        foreach (var item in a)
                        {
                            _tracingService.Trace("Releated Entity Value :" + item.Value + ", Key :" + item.Key);
                        }
                    }
                }
            }

            return resultId;
        }

        private void UpdateStepStage(Guid stepId, bool enableStep = false)
        {
            var updateStep = new SdkMessageProcessingStep
            {
                Id = stepId,
                FilteringAttributes = Context.Input.xts_triggeringattributevalue
            };
            if (enableStep)
            {
                updateStep["StateCode"] = SdkMessageProcessingStepState.Enabled.ToOptionSetValue();
            }

            Context.OnBehalfService.Update(updateStep);
        }

        private Guid QueryPluginType(string className)
        {
            Guid resultId = Guid.Empty;

            var query = new QueryExpression
            {
                EntityName = PluginType.EntityLogicalName,
                ColumnSet = new ColumnSet<PluginType>(e => e.AssemblyName, e => e.TypeName)
            };

            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<PluginType>(e => e.AssemblyName), ConditionOperator.Equal, PROJECT_NAME);
            query.Criteria.AddCondition(EntityExt.GetAttributeLogicalName<PluginType>(e => e.TypeName), ConditionOperator.Equal, className);

            var result = Context.OnBehalfService.RetrieveMultiple(query);
            if (result.Entities.Count > 0)
            {
                resultId = result.Entities[0].Id;
            }

            return resultId;
        }

        private Guid QuerySDKMessage(string message)
        {
            Guid resultId = Guid.Empty;

            // Define query attribute for sdk message
            var queryByAttribute = new QueryByAttribute()
            {
                EntityName = SdkMessage.EntityLogicalName,
                ColumnSet = new ColumnSet<SdkMessage>(e => e.Name)
            };

            // Add message name value to name attributes
            var qba = queryByAttribute;
            qba.AddAttributeValue(EntityExt.GetAttributeLogicalName<SdkMessage>(e => e.Name), message);

            var result = Context.OnBehalfService.RetrieveMultiple(qba);
            if (result.Entities.Count > 0)
            {
                resultId = result.Entities[0].Id;
            }

            return resultId;
        }
    }
}
