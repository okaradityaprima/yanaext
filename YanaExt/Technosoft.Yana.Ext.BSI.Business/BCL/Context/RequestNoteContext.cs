﻿namespace Technosoft.YanaExt.BSI.Business.BCL.Context
{
    using System.Collections.Generic;
    using Microsoft.Xrm.Sdk;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using coreDto = Technosoft.DMS.BCL.DataTransfer;

    /// <summary>
    /// Context of ax request note operation 
    /// </summary>
    public class RequestNoteContext
    {
        /// <summary>
        /// CRM context
        /// </summary>
        public IPluginExecutionContext Context { get; set; }

        /// <summary>
        /// CRM service
        /// </summary>
        public IOrganizationService Service { get; set; }
        
        /// <summary>
        /// Service dealer settings
        /// </summary>
        public xts_servicedealersettings ServiceDealerSettings { get; set; }

        /// <summary>
        /// TS Document Combine
        /// </summary>
        public List<coreDto.TSDocumentCombine> TSDocumentCombine { get; set; }
    }
}
