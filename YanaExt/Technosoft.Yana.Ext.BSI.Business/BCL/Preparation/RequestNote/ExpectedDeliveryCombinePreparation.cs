﻿namespace Technosoft.YanaExt.BSI.Business.BCL.Preparation.RequestNote
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Technosoft.DMS.BCL.DataTransfer;
    using Technosoft.DMS.BCL.Global.Business.Helper;
    using Technosoft.DMS.XRM.Base.Data.Query;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.EntityClasses.Data.Wrappers;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using Technosoft.DMS.XRM.Service.Business;
    using jpDto = DMS.BCL.DataTransfer;
    using Technosoft.DMS.XRM.Base.Interfaces;
    using Technosoft.DMSJP.XRM.Service.Business;
    using Technosoft.YanaExt.BSI.Business.BCL.Context;

    /// <summary>
    /// Preparation of Request Note to AX preparation
    /// </summary>
    public class ExpectedDeliveryCombinePreparation : IPreparation
    {
        private IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> _serviceContext;
        private string _axLanguageCode;
        private readonly IOrganizationService _service;
        private readonly IOrganizationService _adminService;
        private readonly double _diffTimeUser;
        private readonly ITracingService _tracingservice;
        private IJapanServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> _japanContext;
        private xts_servicedealersettings _serviceDealerSettings;

        public ExpectedDeliveryCombinePreparation(IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> iServiceContext, ITracingService iTracingservice)
        {
            _serviceContext = iServiceContext;
            _axLanguageCode = iServiceContext.DMSPluginContext.UserSettings.AXLanguageCode;
            _service = iServiceContext.DMSPluginContext.OrganizationService;
            _adminService = iServiceContext.DMSPluginContext.OrganizationServiceAdmin;
            _diffTimeUser = iServiceContext.DiffTimeUser;
            _tracingservice = iTracingservice;
            _japanContext = new JapanServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>>
            {
                ServiceContext = _serviceContext
            };
            _serviceDealerSettings = _japanContext.JapanServiceDealerSettings;
        }

        public object Prepare()
        {
            Xts_RequestNoteWrapper<xts_requestnote> wrapper = (Xts_RequestNoteWrapper<xts_requestnote>)_serviceContext.Wrapper;
            var requestNoteType = wrapper.XrmEntity.xts_type;
            if (requestNoteType == null) return new RequestNoteContext();
            if (wrapper.XrmEntity.xts_workorderid == null && wrapper.XrmEntity.xts_salesorderid == null) return new RequestNoteContext();

            TSDocumentCombine tsCombineHeader = null;
            switch (requestNoteType.Value)
            {
                case (int)xts_requestnote.Type.WorkOrder:
                    {
                        tsCombineHeader = GenerateEDBasedOnWorkOrder(wrapper);
                    }
                    break;

                case (int)xts_requestnote.Type.SalesOrder:
                    {
                        tsCombineHeader = GenerateEDBasedOnSalesOrder(wrapper);
                    }
                    break;
            }

            RequestNoteContext requestNoteContext = new RequestNoteContext
            {
                TSDocumentCombine = new List<TSDocumentCombine> { tsCombineHeader }
            };

            return requestNoteContext;
        }

        private TSDocumentCombine GenerateEDBasedOnWorkOrder(Xts_RequestNoteWrapper<xts_requestnote> wrapper)
        {
            var workOrderWrapper = wrapper.WorkOrder(new ColumnSet<xts_workorder>(
                x => x.xts_workorderstatus, x => x.xts_workorderId,
                x => x.xts_workorder1, x => x.xts_transactiondate,
                x => x.xts_servicepersoninchargeid)
            );

            TSDocumentCombine tsCombineHeader = new TSDocumentCombine
            {
                Company = _serviceContext.BusinessUnit.msdyn_companycode,
                DataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                LanguageCode = _axLanguageCode,
                MessageId = _serviceContext.Wrapper.XrmEntity.Id,
                ProcessCode = "RequestNote",

                EXPDLVRYDmsDocumentState = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSDocumentState.Committed,
                EXPDLVRYDmsTransType = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSTransType.WorkOrder,
                EXPDLVRYExpectedDeliveryNumber = workOrderWrapper.XrmEntity.xts_workorder1,
                EXPDLVRYSourceData = "TS.3.150"
            };

            var workOrderPartMaterialAndServices = workOrderWrapper.WorkPartMaterialAndServiceNotClosedLine;
            if (workOrderPartMaterialAndServices != null && workOrderPartMaterialAndServices.Count() > 0)
            {
                var requestNoteDetails = wrapper.GetRequestNoteDetail(new ColumnSet<xts_requestnotedetail>(
                    x => x.xts_servicepartsandmaterialid, x => x.xts_basequantityrequestallocate, x => x.xts_warehouseid)
                );

                workOrderPartMaterialAndServices = workOrderPartMaterialAndServices.Where(x =>
                    x.XrmEntity.xts_productid != null &&
                    x.XrmEntity.xts_producttype != null &&
                    x.XrmEntity.xts_producttype.Value != (int)xts_workorderpartmaterialandservice.ProductType.Services &&
                    x.XrmEntity.xts_producttype.Value != (int)xts_workorderpartmaterialandservice.ProductType.NonStockDummy &&
                    x.XrmEntity.xts_outsource.Value == (int)xts_workorderpartmaterialandservice.Outsource.No &&
                    x.XrmEntity.xts_outsource != null).ToList();

                if (workOrderPartMaterialAndServices.Count() > 0)
                {
                    List<TSDocumentCombineDetail> tsCombineDetails = new List<TSDocumentCombineDetail>();

                    foreach (var list in workOrderPartMaterialAndServices)
                    {
                        TSDocumentCombineDetail tsCombineDetail = new TSDocumentCombineDetail
                        {
                            EXPDLVRYSiteDataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                            EXPDLVRYOriginalDetailReferenceNumber = list.XrmEntity.xts_workorderdetail,
                            EXPDLVRYExpectedDeliveryQuantity = Convert.ToDecimal(list.XrmEntity.xts_baseqtymanhourrequired != null ? list.XrmEntity.xts_baseqtymanhourrequired.Value : 0),
                            EXPDLVRYSourceData = "TS.3.160",
                            EXPDLVRYDmsDocumentState = 2,
                            EXPDLVRYExpectedDeliveryDate = workOrderWrapper.XrmEntity.xts_transactiondate.GetValueOrDefault(DateTime.UtcNow).ToUniversalTime().AddHours(_diffTimeUser),
                        };

                        //set Base Qty Request from Request Note
                        if (requestNoteDetails != null && requestNoteDetails.Count() > 0)
                        {
                            var selectedDetail = requestNoteDetails.Where(x => (x.XrmEntity.xts_servicepartsandmaterialid != null ? x.XrmEntity.xts_servicepartsandmaterialid.Id : Guid.Empty) == list.Id).FirstOrDefault();
                            if (selectedDetail != null)
                            {
                                tsCombineDetail.EXPDLVRYReservedRequestQuantity = Convert.ToDecimal(selectedDetail.XrmEntity.xts_basequantityrequestallocate != null ? selectedDetail.XrmEntity.xts_basequantityrequestallocate.Value : 0);
                            }
                        }

                        if (list.XrmEntity.xts_productid != null)
                        {
                            tsCombineDetail.EXPDLVRYItemId = new ProductHelper(_adminService).GetProductName(list.XrmEntity.xts_productid);
                        }

                        if (list.XrmEntity.xts_inventoryunitid != null)
                        {
                            tsCombineDetail.EXPDLVRYUoMId = list.XrmEntity.xts_inventoryunitid.Name != null ? list.XrmEntity.xts_inventoryunitid.Name : string.Empty;
                        }
                        if (workOrderWrapper.XrmEntity.xts_servicepersoninchargeid != null)
                        {
                            tsCombineDetail.EXPDLVRYPIC = workOrderWrapper.XrmEntity.xts_servicepersoninchargeid.Name ?? string.Empty;
                        }

                        // Invent DIM
                        var expectedDelivertInventDimDto = new InventDim
                        {
                            InventSiteId = list.XrmEntity.xts_siteid?.Name,
                            //WMSLocationId = list.XrmEntity.xts_locationid?.Name,
                            //InventLocationId = list.XrmEntity.xts_warehouseid?.Name
                        };

                        if (_serviceDealerSettings.xts_wopartsandmaterialmethod != null)
                        {
                            switch (_serviceDealerSettings.xts_wopartsandmaterialmethod.Value)
                            {
                                case (int)xts_servicedealersettings.WOPartsAndMaterialMethod.DirectUsed:
                                    {
                                        expectedDelivertInventDimDto.InventLocationId = list.XrmEntity.xts_warehouseid?.Name;
                                    }
                                    break;

                                case (int)xts_servicedealersettings.WOPartsAndMaterialMethod.InventoryTransfer:
                                    {
                                        if (requestNoteDetails != null && requestNoteDetails.Count() > 0)
                                        {
                                            var selectedDetail = requestNoteDetails.Where(x => (x.XrmEntity.xts_servicepartsandmaterialid != null ? x.XrmEntity.xts_servicepartsandmaterialid.Id : Guid.Empty) == list.Id).FirstOrDefault();
                                            if (selectedDetail != null)
                                            {
                                                expectedDelivertInventDimDto.InventLocationId = selectedDetail.XrmEntity.xts_warehouseid != null ? selectedDetail.XrmEntity.xts_warehouseid?.Name : null;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }

                        tsCombineDetail.InventDim_ExpDlvry = expectedDelivertInventDimDto;
                        tsCombineDetails.Add(tsCombineDetail);
                    }

                    tsCombineHeader.TSDocumentCombineDetail = tsCombineDetails;
                }
            }

            return tsCombineHeader;
        }

        private TSDocumentCombine GenerateEDBasedOnSalesOrder(Xts_RequestNoteWrapper<xts_requestnote> wrapper)
        {
            Xts_SalesOrderWrapper<xts_salesorder> salesOrderWrapper = new Xts_SalesOrderWrapper<xts_salesorder>(_service);
            salesOrderWrapper.SetXrmEntity(_service.Retrieve(xts_salesorder.EntityLogicalName,
                wrapper.XrmEntity.xts_salesorderid.Id, new ColumnSet(new string[] {
                    EntityExt.GetAttributeLogicalName<xts_salesorder>(c => c.xts_salesorderId),
                    EntityExt.GetAttributeLogicalName<xts_salesorder>(c => c.xts_transactiondate),
                    EntityExt.GetAttributeLogicalName<xts_salesorder>(c => c.xts_status),
                    EntityExt.GetAttributeLogicalName<xts_salesorder>(c => c.xts_salesordernumber)
            })).ToEntity<xts_salesorder>());

            TSDocumentCombine tsCombineHeader = new TSDocumentCombine
            {
                Company = _serviceContext.BusinessUnit.msdyn_companycode,
                DataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                LanguageCode = _axLanguageCode,
                MessageId = _serviceContext.Wrapper.XrmEntity.Id,
                ProcessCode = "RequestNote",

                EXPDLVRYDmsDocumentState = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSDocumentState.Committed,
                EXPDLVRYDmsTransType = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSTransType.SalesOrder,
                EXPDLVRYExpectedDeliveryNumber = salesOrderWrapper.XrmEntity.xts_salesordernumber,
                EXPDLVRYSourceData = "TS.0.B0010"
            };

            var salesOrderDetails = salesOrderWrapper.SalesOrderDetailNotCloseLine;
            if (salesOrderDetails != null && salesOrderDetails.Count() > 0)
            {
                var requestNoteDetails = wrapper.GetRequestNoteDetail(new ColumnSet<xts_requestnotedetail>(x => x.xts_salesorderdetailid, x => x.xts_basequantityrequestallocate));

                List<TSDocumentCombineDetail> tsCombineDetails = new List<TSDocumentCombineDetail>();

                foreach (var list in salesOrderDetails)
                {
                    TSDocumentCombineDetail tsCombineDetail = new TSDocumentCombineDetail
                    {
                        EXPDLVRYSiteDataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                        EXPDLVRYOriginalDetailReferenceNumber = list.XrmEntity.xts_salesorderdetailnumber,
                        EXPDLVRYExpectedDeliveryDate = salesOrderWrapper.XrmEntity.xts_transactiondate.GetValueOrDefault(DateTime.UtcNow).ToUniversalTime().AddHours(_diffTimeUser),
                        EXPDLVRYExpectedDeliveryQuantity = Convert.ToDecimal(list.XrmEntity.xts_quantityorderbaseunit != null ? list.XrmEntity.xts_quantityorderbaseunit.Value : 0),
                        EXPDLVRYSourceData = "TS.0.B0020",
                        EXPDLVRYDmsDocumentState = 2
                    };

                    // set Base Qty Request from Request Note
                    if (requestNoteDetails != null && requestNoteDetails.Count() > 0)
                    {
                        var selectedDetail = requestNoteDetails.Where(x => (x.XrmEntity.xts_salesorderdetailid != null ? x.XrmEntity.xts_salesorderdetailid.Id : Guid.Empty) == list.Id).FirstOrDefault();
                        if (selectedDetail != null)
                        {
                            tsCombineDetail.EXPDLVRYReservedRequestQuantity = Convert.ToDecimal(selectedDetail.XrmEntity.xts_basequantityrequestallocate != null ? selectedDetail.XrmEntity.xts_basequantityrequestallocate.Value : 0);
                        }
                    }

                    if (list.XrmEntity.xts_productid != null)
                    {
                        tsCombineDetail.EXPDLVRYItemId = new ProductHelper(_serviceContext.DMSPluginContext.OrganizationService).GetProductName(list.XrmEntity.xts_productid);
                    }

                    if (list.XrmEntity.xts_inventoryunitid != null)
                    {
                        tsCombineDetail.EXPDLVRYUoMId = list.XrmEntity.xts_inventoryunitid.Name ?? string.Empty;
                    }

                    // Invent DIM
                    var expectedDelivertInventDimDto = new InventDim
                    {
                        InventSiteId = list.XrmEntity.xts_siteid?.Name,
                        //WMSLocationId = list.XrmEntity.xts_locationid?.Name,
                        InventLocationId = list.XrmEntity.xts_warehouseid?.Name
                    };

                    tsCombineDetail.InventDim_ExpDlvry = expectedDelivertInventDimDto;
                    tsCombineDetails.Add(tsCombineDetail);
                }

                tsCombineHeader.TSDocumentCombineDetail = tsCombineDetails;
            }

            return tsCombineHeader;
        }
    }
}