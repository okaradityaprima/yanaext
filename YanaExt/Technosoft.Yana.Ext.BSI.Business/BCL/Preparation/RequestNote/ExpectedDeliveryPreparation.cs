﻿namespace Technosoft.YanaExt.BSI.Business.BCL.Preparation.RequestNote
{
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Technosoft.DMS.BCL.DataTransfer;
    using Technosoft.DMS.XRM.Base.Data.Query;
    using Technosoft.DMS.XRM.Base.Extensions;
    using Technosoft.DMS.XRM.Base.Interfaces;
    using Technosoft.DMS.XRM.EntityClasses.Data.Wrappers;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using Technosoft.DMS.XRM.Service.Business;
    using Technosoft.DMSJP.XRM.Service.Business;
    using jpDto = DMS.BCL.DataTransfer;
    using Technosoft.YanaExt.BSI.Business.BCL.Context;

    /// <summary>
    /// Preparation of Request Note to AX preparation
    /// </summary>
    public class ExpectedDeliveryPreparation : IPreparation
    {
        private IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> _serviceContext;
        private string _axLanguageCode;
        private readonly IOrganizationService _service;
        private readonly IOrganizationService _adminService;
        private readonly double _diffTimeUser;
        private readonly ITracingService _tracingservice;
        private string _currencyCode = string.Empty;
        private EntityReference _currencyCodeRef;
        private IJapanServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> _japanContext;
        private xts_servicedealersettings _serviceDealerSettings;

        public ExpectedDeliveryPreparation(IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> iServiceContext, ITracingService iTracingservice)
        {
            _serviceContext = iServiceContext;
            _axLanguageCode = iServiceContext.DMSPluginContext.UserSettings.AXLanguageCode;
            _service = iServiceContext.DMSPluginContext.OrganizationService;
            _adminService = iServiceContext.DMSPluginContext.OrganizationServiceAdmin;
            _diffTimeUser = iServiceContext.DiffTimeUser;
            _tracingservice = iTracingservice;
            _japanContext = new JapanServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>>
            {
                ServiceContext = _serviceContext
            };
            _serviceDealerSettings = _japanContext.JapanServiceDealerSettings;
        }

        public object Prepare()
        {
            Xts_RequestNoteWrapper<xts_requestnote> wrapper = (Xts_RequestNoteWrapper<xts_requestnote>)_serviceContext.Wrapper;
            var requestNoteType = wrapper.XrmEntity.xts_type;
            if (requestNoteType == null) return new RequestNoteContext();
            if (wrapper.XrmEntity.xts_workorderid == null && wrapper.XrmEntity.xts_salesorderid == null) return new RequestNoteContext();

            var columnSet = new ColumnSet<xts_requestnotedetail>();
            columnSet.AddColumn(x => x.xts_basequantityremaining);
            columnSet.AddColumn(x => x.xts_servicepartsandmaterialid);
            columnSet.AddColumn(x => x.xts_salesorderdetailid);

            var requestNoteDetails = wrapper.GetRequestNoteDetail(columnSet);
            if (requestNoteDetails.Count() == 0) { return new RequestNoteContext(); }

            if (_serviceContext.DMSPluginContext.GeneralSetup<xts_generalsetup>().Instance.xts_basecurrencyid != null)
            {
                var currencyInfo = _serviceContext.DMSPluginContext.OrganizationService.Retrieve(TransactionCurrency.EntityLogicalName,
                    _serviceContext.DMSPluginContext.GeneralSetup<xts_generalsetup>().Instance.xts_basecurrencyid.Id, new ColumnSet(new string[] {
                        EntityExt.GetAttributeLogicalName<TransactionCurrency>(x => x.ISOCurrencyCode),
                        EntityExt.GetAttributeLogicalName<TransactionCurrency>(x => x.CurrencyPrecision)
                })).ToEntity<TransactionCurrency>();

                _currencyCode = currencyInfo.ISOCurrencyCode;
                _currencyCodeRef = currencyInfo?.ToEntityReference();
            }

            TSDocumentCombine tsCombineHeader = null;
            switch (requestNoteType.Value)
            {
                case (int)xts_requestnote.Type.WorkOrder:
                    {
                        tsCombineHeader = GenerateEDBasedOnWorkOrder(wrapper, requestNoteDetails);
                    }
                    break;

                case (int)xts_requestnote.Type.SalesOrder:
                    {
                        tsCombineHeader = GenerateEDBasedOnSalesOrder(wrapper, requestNoteDetails);
                    }
                    break;
            }

            RequestNoteContext requestNoteContext = new RequestNoteContext
            {
                TSDocumentCombine = new List<TSDocumentCombine> { tsCombineHeader }
            };

            _tracingservice.Trace("_currencyCode : " + _currencyCode);
            _tracingservice.Trace("_currencyCodeRef : " + _currencyCodeRef?.Id);

            return requestNoteContext;
        }

        private TSDocumentCombine GenerateEDBasedOnWorkOrder(Xts_RequestNoteWrapper<xts_requestnote> wrapper, IEnumerable<Xts_RequestNoteDetailWrapper<xts_requestnotedetail>> requestNoteDetails)
        {
            Xts_WorkOrderWrapper<xts_workorder> workOrderWrapper = new Xts_WorkOrderWrapper<xts_workorder>(_service);
            workOrderWrapper.SetXrmEntity(_service.Retrieve(xts_workorder.EntityLogicalName,
                wrapper.XrmEntity.xts_workorderid.Id, new ColumnSet(new string[] {
                    EntityExt.GetAttributeLogicalName<xts_workorder>(c => c.xts_workorder1),
                    EntityExt.GetAttributeLogicalName<xts_workorder>(c => c.xts_workorderId)
            })).ToEntity<xts_workorder>());

            TSDocumentCombine tsCombineHeader = new TSDocumentCombine
            {
                Company = _serviceContext.BusinessUnit.msdyn_companycode,
                DataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                LanguageCode = _axLanguageCode,
                MessageId = Guid.NewGuid(),
                ProcessCode = "CancelRequestNote",

                EXPDLVRYDmsTransType = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSTransType.WorkOrder,
                EXPDLVRYDmsDocumentState = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSDocumentState.Committed,
                EXPDLVRYExpectedDeliveryNumber = workOrderWrapper.XrmEntity.xts_workorder1,
                EXPDLVRYSourceData = "TS.3.150",
                JourTblCurrencyCode = _currencyCode,
                JourTblCurrencyCodeRef = _currencyCodeRef?.Id
            };

            var WOParts = workOrderWrapper.WorkPartMaterialAndServiceNotClosedLine.ToList();

            List<TSDocumentCombineDetail> tsCombineDetails = new List<TSDocumentCombineDetail>();
            foreach (var requestNoteDetail in requestNoteDetails)
            {
                if (requestNoteDetail.XrmEntity.xts_servicepartsandmaterialid == null) { continue; }

                var selectedWOPart = WOParts.Where(x => x.XrmEntity.Id == requestNoteDetail.XrmEntity.xts_servicepartsandmaterialid.Id).FirstOrDefault();
                if (selectedWOPart != null)
                {
                    /*
                    var remainingQuantity = requestNoteDetail.XrmEntity.xts_basequantityremaining != null ? requestNoteDetail.XrmEntity.xts_basequantityremaining.Value : 0;
                    var transferQuantity = requestNoteDetail.XrmEntity.xts_basequantitytransferred != null ? requestNoteDetail.XrmEntity.xts_basequantitytransferred.Value : 0;
                    Double EXPDLVRYReservedRequestQuantity = 0;

                    if (transferQuantity > 0)
                    {
                        EXPDLVRYReservedRequestQuantity = remainingQuantity;
                    }
                    */

                    TSDocumentCombineDetail tsCombineDetail = new TSDocumentCombineDetail
                    {
                        EXPDLVRYSiteDataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                        EXPDLVRYOriginalDetailReferenceNumber = selectedWOPart.XrmEntity.xts_workorderdetail,
                        EXPDLVRYExpectedDeliveryDate = workOrderWrapper.XrmEntity.xts_transactiondate.GetValueOrDefault(DateTime.UtcNow).ToUniversalTime().AddHours(_diffTimeUser),
                        EXPDLVRYExpectedDeliveryQuantity = Convert.ToDecimal(selectedWOPart.XrmEntity.xts_baseqtymanhourrequired != null ? selectedWOPart.XrmEntity.xts_baseqtymanhourrequired.Value : 0),
                        EXPDLVRYReservedRequestQuantity = Convert.ToDecimal(selectedWOPart.XrmEntity.xts_baseqtyrequest != null ? selectedWOPart.XrmEntity.xts_baseqtyrequest.Value : 0),
                        EXPDLVRYSourceData = "TS.3.160",
                        EXPDLVRYDmsDocumentState = 2
                    };

                    // Invent DIM
                    var expectedDelivertInventDimDto = new InventDim
                    {
                        InventSiteId = selectedWOPart.XrmEntity.xts_siteid?.Name,
                        //WMSLocationId = list.XrmEntity.xts_locationid?.Name,
                        //InventLocationId = list.XrmEntity.xts_warehouseid?.Name
                    };

                    if (_serviceDealerSettings.xts_wopartsandmaterialmethod != null)
                    {
                        switch (_serviceDealerSettings.xts_wopartsandmaterialmethod.Value)
                        {
                            case (int)xts_servicedealersettings.WOPartsAndMaterialMethod.DirectUsed:
                                {
                                    expectedDelivertInventDimDto.InventLocationId = selectedWOPart.XrmEntity.xts_warehouseid?.Name;
                                }
                                break;

                            case (int)xts_servicedealersettings.WOPartsAndMaterialMethod.InventoryTransfer:
                                {
                                    if (requestNoteDetails != null && requestNoteDetails.Count() > 0)
                                    {
                                        var selectedDetail = requestNoteDetails.Where(x => (x.XrmEntity.xts_servicepartsandmaterialid != null ? x.XrmEntity.xts_servicepartsandmaterialid.Id : Guid.Empty) == selectedWOPart.Id).FirstOrDefault();
                                        if (selectedDetail != null)
                                        {
                                            expectedDelivertInventDimDto.InventLocationId = selectedDetail.XrmEntity.xts_warehouseid != null ? selectedDetail.XrmEntity.xts_warehouseid?.Name : null;
                                        }
                                    }
                                }
                                break;
                        }
                    }

                    tsCombineDetail.InventDim_ExpDlvry = expectedDelivertInventDimDto;

                    tsCombineDetails.Add(tsCombineDetail);
                }
            }

            tsCombineHeader.TSDocumentCombineDetail = tsCombineDetails;
            return tsCombineHeader;
        }

        private TSDocumentCombine GenerateEDBasedOnSalesOrder(Xts_RequestNoteWrapper<xts_requestnote> wrapper, IEnumerable<Xts_RequestNoteDetailWrapper<xts_requestnotedetail>> requestNoteDetails)
        {
            Xts_SalesOrderWrapper<xts_salesorder> salesOrderWrapper = new Xts_SalesOrderWrapper<xts_salesorder>(_service);
            salesOrderWrapper.SetXrmEntity(_service.Retrieve(xts_salesorder.EntityLogicalName,
                wrapper.XrmEntity.xts_salesorderid.Id, new ColumnSet(new string[] {
                    EntityExt.GetAttributeLogicalName<xts_salesorder>(c => c.xts_salesordernumber),
                    EntityExt.GetAttributeLogicalName<xts_salesorder>(c => c.xts_salesorderId)
            })).ToEntity<xts_salesorder>());

            TSDocumentCombine tsCombineHeader = new TSDocumentCombine
            {
                Company = _serviceContext.BusinessUnit.msdyn_companycode,
                DataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                LanguageCode = _axLanguageCode,
                MessageId = Guid.NewGuid(),
                ProcessCode = "CancelRequestNote",

                EXPDLVRYDmsTransType = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSTransType.SalesOrder,
                EXPDLVRYDmsDocumentState = (int)jpDto.DMSExpectedDelivery.AxdEnum_DMSDocumentState.Committed,
                EXPDLVRYExpectedDeliveryNumber = salesOrderWrapper.XrmEntity.xts_salesordernumber,
                EXPDLVRYSourceData = "TS.0.B0010",
                JourTblCurrencyCode = _currencyCode,
                JourTblCurrencyCodeRef = _currencyCodeRef?.Id
            };

            var SODetails = salesOrderWrapper.SalesOrderDetailNotCloseLine.ToList();

            List<TSDocumentCombineDetail> tsCombineDetails = new List<TSDocumentCombineDetail>();
            foreach (var requestNoteDetail in requestNoteDetails)
            {
                if (requestNoteDetail.XrmEntity.xts_salesorderdetailid == null) { continue; }

                var selectedSODetail = SODetails.Where(x => x.XrmEntity.Id == requestNoteDetail.XrmEntity.xts_salesorderdetailid.Id).FirstOrDefault();
                if (selectedSODetail != null)
                {
                    /*
                    var remainingQuantity = requestNoteDetail.XrmEntity.xts_basequantityremaining != null ? requestNoteDetail.XrmEntity.xts_basequantityremaining.Value : 0;
                    var transferQuantity = requestNoteDetail.XrmEntity.xts_basequantitytransferred != null ? requestNoteDetail.XrmEntity.xts_basequantitytransferred.Value : 0;
                    Double EXPDLVRYReservedRequestQuantity = 0;

                    if (transferQuantity > 0)
                    {
                        EXPDLVRYReservedRequestQuantity = remainingQuantity;
                    }
                    */

                    TSDocumentCombineDetail tsCombineDetail = new TSDocumentCombineDetail
                    {
                        EXPDLVRYSiteDataAreaId = _serviceContext.BusinessUnit.msdyn_companycode,
                        EXPDLVRYOriginalDetailReferenceNumber = selectedSODetail.XrmEntity.xts_salesorderdetailnumber,
                        EXPDLVRYExpectedDeliveryDate = salesOrderWrapper.XrmEntity.xts_transactiondate.GetValueOrDefault(DateTime.UtcNow).ToUniversalTime().AddHours(_diffTimeUser),
                        //EXPDLVRYExpectedDeliveryQuantity = Convert.ToDecimal(selectedSODetail.XrmEntity.xts_quantityorderbaseunit != null ? selectedSODetail.XrmEntity.xts_quantityorderbaseunit.Value : 0),
                        EXPDLVRYReservedRequestQuantity = Convert.ToDecimal(selectedSODetail.XrmEntity.xts_baseqtyrequest != null ? selectedSODetail.XrmEntity.xts_baseqtyrequest.Value : 0),
                        EXPDLVRYSourceData = "TS.0.B0020",
                        EXPDLVRYDmsDocumentState = 2
                    };

                    // Invent DIM
                    var expectedDelivertInventDimDto = new InventDim
                    {
                        InventSiteId = selectedSODetail.XrmEntity.xts_siteid?.Name,
                        //WMSLocationId = list.XrmEntity.xts_locationid?.Name,
                        InventLocationId = selectedSODetail.XrmEntity.xts_warehouseid?.Name
                    };

                    tsCombineDetail.InventDim_ExpDlvry = expectedDelivertInventDimDto;

                    tsCombineDetails.Add(tsCombineDetail);
                }
            }

            tsCombineHeader.TSDocumentCombineDetail = tsCombineDetails;
            return tsCombineHeader;
        }
    }
}