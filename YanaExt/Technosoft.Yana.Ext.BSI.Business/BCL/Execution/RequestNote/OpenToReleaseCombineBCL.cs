﻿namespace Technosoft.YanaExt.BSI.Business.BCL.Execution.RequestNote
{
    using Microsoft.Xrm.Sdk;
    using System.Linq;
    using Technosoft.DMS.XRM.EntityClasses.Data.Wrappers;
    using Technosoft.DMS.XRM.EntityClasses.Entities;
    using Technosoft.DMS.XRM.Service.Business;
    using Technosoft.DMSJP.CRM2AX.Adapter;

    using Technosoft.YanaExt.BSI.Business.BCL.Context;
    using Technosoft.YanaExt.BSI.Business.BCL.Preparation.RequestNote;

    /// <summary>
    /// Request Note from Open to Released
    /// </summary>
    public class OpenToReleaseCombineBCL : ServiceBaseOperation
    {
        private IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> _serviceContext;
        private readonly ITracingService _tracingservice;

        public OpenToReleaseCombineBCL(IServiceContext<xts_requestnote, Xts_RequestNoteWrapper<xts_requestnote>> iServiceContext, ITracingService iTracingservice)
        {
            _serviceContext = iServiceContext;
            _tracingservice = iTracingservice;
        }

        public override void Execute()
        {
            ExpectedDeliveryCombinePreparation requestNoteToAxPreparation = new ExpectedDeliveryCombinePreparation(_serviceContext, _tracingservice);
            RequestNoteContext requestNoteToAxContext = (RequestNoteContext)requestNoteToAxPreparation.Prepare();

            if (requestNoteToAxContext.TSDocumentCombine != null)
            {
                if (requestNoteToAxContext.TSDocumentCombine.Count() > 0)
                {
                    var combineCreate = CreateDocumentCombineAdapter.Construct(requestNoteToAxContext.TSDocumentCombine, _serviceContext.DMSPluginContext);
                    combineCreate.Execute();
                }
            }
        }
    }
}